<?php
/**
* Стартовый скрипт (с использованием mod_rewrites)
* @package SWC-6
* @subpackage core
* @author Eugeny Leonov <eleonov@leolab.info>
* @version 1.0.rc <30/10/2010>
* @todo: Проверка что файл не вызван прямой ссылкой
* 	Данный скрипт должен вызваться ТОЛЬКО по правилу в /.htaccess
*/
define('htaccess',true);
define('base_url',rtrim(str_replace(array(basename(__FILE__),'\\'),array('','/'),$_SERVER['SCRIPT_NAME']),'/'));
$bp=str_replace('\\','/',__FILE__);define('base_path',rtrim(substr($bp,0,strrpos($bp,'/'))),'/');unset($bp);
if(file_exists(base_path.'/_swc/core.php')){
	include(base_path.'/_swc/core.php');
}elseif(file_exists(base_path.'/core.php')){
	include(base_path.'/core.php');
}else{
	die('SWC: Core module not exists.');
}
die();
?>
