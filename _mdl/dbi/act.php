<?php
/**
@title: Интерфейс действия модуля интерфейса БД MySQL
@package: dbi
@version: 1.0.a <05/02/2011>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
if(!defined('htaccess')){die('Direct access disabled');}
if(!isset($blk)||!is_array($blk)){return(setResult(false,'Отсутствуют данные интерфейса.'));}
if(!isset($blk['act'])||!is_string($blk['act'])||(trim($blk['act'])=='')){return(setResult(false,'Не указан метод интерфейса.'));}

switch($blk['act']){

/**
@title: Сохранить (создать/изменить) конфигурацию модуля.
@todo: Проверка прав пользователя
@todo: Проверка возможности подключения к БД с указанными параметрами.
*/
	case 'cfg_save':
		return(setResult(false,'Не реализовано (dbi:'.$blk['act'].')'));
	break;
	default:
		return(setResult('Неизвестный метод интерфейса (dbi:'.$blk['act'].').'));
	break;
}

_die('Метод интерфейса не вернул данных (dbi:'.$blk['act'].').');
?>
