<?php
/**
@title: Модуль интерфейса БД
@package: dbi
@author: Eugeny Leonov <eleonov@leolab.info>
@version: 4.0.rc <10/11/2010>
 
#rev.1 <04/02/2011> Eugeny Leonov
 	[*] Ревизия модуля.
*/
if(!defined('htaccess')){die('SWC: Direct access disabled.');}
if(!function_exists('mysql_connect')&&!function_exists('mysqli_connect')){return(setResult(false,'В системе отсутствует поддержка БД MySQL.'));}

class dbi{

	private $cfg=false;
	private $i=false;
	private $conn=false;
	private $rep=array();

/* Массив типов индексов полей таблиц */
	static private $itypes=array(
		'pri'=>array('sql'=>'PRIMARY KEY',),
		'mul'=>array('sql'=>'INDEX',),
		'idx'=>array('sql'=>'INDEX',),
		'uni'=>array('sql'=>'UNIQUE INDEX',),
	);

/* Массив описаний типов полей */
	static $ftypes=array(
		'dict'=>array('is_num'=>true,'sized'=>true,'ext'=>array('type'=>'bigint','size'=>20,'idx'=>'mul',),),
		'link'=>array('is_num'=>true,'sized'=>true,'ext'=>array('type'=>'bigint','size'=>20,'idx'=>'mul',),),
		'key'=>array('is_num'=>true,'sized'=>true,'ext'=>array('type'=>'bigint','size'=>20,'idx'=>'pri','auto'=>1,),),
		'bool'=>array('is_num'=>true,'sized'=>false,'ext'=>array('type'=>'tinyint','size'=>1,),),
		'string'=>array('is_num'=>false,'sized'=>true,'ext'=>array('type'=>'varchar','size'=>250,),),
		'tinyint'=>array('is_num'=>true,'sized'=>true,),
		'smallint'=>array('is_num'=>true,'sized'=>true,),
		'int'=>array('is_num'=>true,'sized'=>true,),
		'mediumint'=>array('is_num'=>true,'sized'=>true,),
		'bigint'=>array('is_num'=>true,'sized'=>true,),
		'float'=>array('is_num'=>true,'sized'=>true,),
		'double'=>array('is_num'=>true,'sized'=>true,),
		'decimal'=>array('is_num'=>true,'sized'=>true,),
		'timestamp'=>array('is_num'=>true,'sized'=>false,),
		'year'=>array('is_num'=>true,'sized'=>false,),
		'char'=>array('is_num'=>false,'sized'=>false,),
		'varchar'=>array('is_num'=>false,'sized'=>true,),
		'date'=>array('is_num'=>false,'sized'=>false,),
		'datetime'=>array('is_num'=>false,'sized'=>false,),
		'time'=>array('is_num'=>false,'sized'=>false,),
		'tinyblob'=>array('is_num'=>false,'sized'=>false,),
		'tinytext'=>array('is_num'=>false,'sized'=>false,),
		'blob'=>array('is_num'=>false,'sized'=>false,),
		'text'=>array('is_num'=>false,'sized'=>false,),
		'mediumblob'=>array('is_num'=>false,'sized'=>false,),
		'mediumtext'=>array('is_num'=>false,'sized'=>false,),
		'longblob'=>array('is_num'=>false,'sized'=>false,),
		'longtext'=>array('is_num'=>false,'sized'=>false,),
		'enum'=>array('is_num'=>false,'sized'=>false,),
		'binary'=>array('is_num'=>false,'sized'=>true,),
		'varbinary'=>array('is_num'=>false,'sized'=>true,),
	);
	
/**
@title: Конструктор класса.
@version: 1.0.rc <11/11/2010>
@param: array(host,user,passwd,name,pref)|false
*/
	function __construct($cfg=false){
		$this->cfg=dbi::cfg_load($cfg);

		if(!is_array($this->cfg)){return(setResult(false,'Ошибка в параметрах подключения: '.getResult('reason')));}
		$this->i=function_exists('mysqli_connect');
		if($this->i){
			$this->conn=mysqli_connect($this->cfg['host'],$this->cfg['user'],$this->cfg['passwd'],$this->cfg['name']);
		}else{
			$this->conn=mysql_connect($this->cfg['host'],$this->cfg['user'],$this->cfg['passwd']);
		}
		if(!$this->conn){return(setResult(false,'Ошибка подключения к серверу БД: '.getResult('reason')));}
		if(isset($this->cfg['name'])&&(trim($this->cfg['name'])!='')){
			if($this->i){if(mysqli_select_db($this->conn,$this->cfg['name'])){if(!$this->query_ok()){return(setResult(false,'Ошибка выбора БД: '.getResult('reason')));}}}
			else{if(mysql_select_db($this->cfg['name'],$this->conn)){if(!$this->query_ok()){return(setResult(false,'Ошибка выбора БД: '.getResult('reason')));}}}
		}
		$this->rep=array('%pref%'=>$this->cfg['pref'],'SELECT'=>'SELECT SQL_CALC_FOUND_ROWS',' SELECT SQL_CALC_FOUND_ROWS'=>' SELECT',);
		if(!$this->sql_exec("SET names utf8")){return(setResult(false,'Ошибка установки кодировки БД: '.getResult('reason')));}
		return(setResult(true));
	}

//== Вспомогательные методы настройки и управления >>>>>>>>>>>>>>>>>>>>>>>>>>>>>

/**
@title: Получить наличие и сообщение последней ошибки
@version: 1.0.rc <11/11/2010>
*/
	private function query_ok(){
		if($this->i){
			if(is_object($this->conn)){return(setResult(($this->conn->errno==0),$this->conn->error));
			}else{return(setResult((mysqli_errno($this->conn)==0),mysql_error($this->conn)));}
		}else{
			return(setResult((mysql_errno($this->conn)==0),mysql_error($this->conn)));
		}
	}

/**
@title: Найти и загрузить файл конфигурации
@version: 1.0.a <05/02/2011>
@param: array|string|false
*/
	static function cfg_load($cfg=false){
		if(!is_array($cfg)){
			if(is_string($cfg)&&(trim($cfg)!='')){$cfg=cfg_load(cfg_find($cfg));}
			else{$cfg=cfg_load(cfg_find('dbi'));}
			if(!$cfg){return(setResult(false,'Не найдена конфигурация модуля (dbi).'));}
		}
		return(dbi::cfg_check($cfg));
	}

/**
@title: Сохранить файл конфигурации.
@version: 1.0.a <05/02/2011>
@param: array
@todo: Определение файла для сохранения (создания) конфигурации.
*/
	static function cfg_save($cfg){
		if(!is_array($cfg)){return(setResult(false,'Неверный формат параметра.'));}
		if(!isset($cfg['cfg_name'])||!is_string($cfg['cfg_name'])||(trim($cfg['cfg_name'])=='')){return(setResult(false,'Не указан файл конфигурации.'));}
		if(!dbi::cfg_check($cfg)){return(setResult(false,'Ошибка проверки параметров: '.getResult('reason')));}
		if(!cfg_save($cfg['cfg_name'],$cfg)){return(setResult(false,'Ошибка сохранения параметров: '.getResult('reason')));}else{return(setResult(true));}
	}

/**
@title: Проверить наличие и корректность параметров конфигурации
@version: 1.0.rc <11/11/2010>
@param: array
@return: array|false
*/
	static function cfg_check($cfg){
		if(!is_array($cfg)){return(setResult(false,'Неверный тип параметров.'));}
		if(!isset($cfg['host'])||(trim($cfg['host'])=='')){return(setResult(false,'Не указан сервер БД.'));}
		if(!isset($cfg['user'])||(trim($cfg['user']==''))){return(setResult(false,'Не указан пользователь БД.'));}
		if(!isset($cfg['passwd'])){$cfg['passwd']='';}
		if(!isset($cfg['name'])){$cfg['name']='';}
		if(!isset($cfg['pref'])){$cfg['pref']='';}
		setResult(true);
		return($cfg);
	}

//<< Вспомогательные методы настройки и управления =============================
//== Основные методы >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/**
@title: Выполнить запрос без возвращения результата
@version: 1.0.rc <11/11/2010>
@param: string
@return: bool
*/
	function sql_exec($sql){
		if(trim($sql)==''){return(setResult(false,'Пустой запрос.'));}
		$sql=str_replace(array_keys($this->rep),array_values($this->rep),$sql);
		if(defined('dbgEnabled')&&dbgEnabled){_msg('Query (exec): '.$sql,'D','SWC.dbi');}
		if($this->i){
			mysqli_query($this->conn,$sql);
		}else{
			mysql_query($sql,$this->conn);
		}
		return($this->query_ok());
	}

/**
@title: Получить класс результата запроса
@version: 1.0.rc <11/11/2010>
@param: string
@return: object|false
*/
	function sql_open($sql){
		if(trim($sql)==''){return(setResult(false,'Пустой запрос'));}
		$sql=str_replace(array_keys($this->rep),array_values($this->rep),$sql);
		if(defined('dbgEnabled')&&dbgEnabled){_msg('Query (open): '.$sql,'D','SWC.dbi');}
		if($this->i){
			$res=mysqli_query($this->conn,$sql);
		}else{
			$res=mysql_query($sql,$this->conn);
		}
		if($this->query_ok()){return new dbi_rs($res,$this->i);}else{return(false);}
	}

/**
@title: Получить массив результата запроса
@version: 1.0.rc <11/11/2010>
@param: string
@param: bool
@param: boll
@return: array|false
 
#rev.1 <01/12/2010>
 	[*] Исправлена ошибка: При пустом результате множественной выборки производилась попытка возврата неопределенной переменной.
*/
	function sql_get($sql,$single=false,$named=false){
		$rs=$this->sql_open($sql);
		if(!$rs){return(false);}
		$ret=false;
		if($single){$ret=$rs->get($named);}
		else{while($td=$rs->get($named)){$ret[]=$td;}}
		$rs->free();
		return($ret);
	}

/**
@title: Получить кол-во строк найденных последним запросом
@version: 1.0.rc <11/11/2010>
@return: int
*/
	function foundRows(){
		$this->rep['SELECT']='SELECT';
		list($ret)=$this->sql_get("SELECT FOUND_ROWS()",true,false);
		$this->rep['SELECT']='SELECT SQL_CALC_FOUND_ROWS';
		return($ret);
	}

/**
@title: Получить авто-id с последнего инсерта
@version: 1.0.rc <11/11/2010>
@return: int
*/
	function insertId(){
		if($this->i){return(mysqli_insert_id($this->conn));}else{return(mysql_insert_id($this->i));}
	}
//<< Основные методы ==========================================================
	
//== Вспомогательные методы >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/**
@title: Получить список баз данных
@version: 1.0 <13/03/2011>
*/
	function db_list(){
		return($this->sql_get("SHOW DATABASES",false,true));
	}

/**
@title: Создать базу данных
@version: 1.0 <13/03/2011>
@param: string
@return: bool
*/
	function db_create($name){
		return($this->sql_exec("CREATE DATABASE ".$name));
	}

//<< Вспомогательные методы ===================================================
}

//== Класс результата запроса >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
class dbi_rs{

	private $res;
	private $i;

/**
@title: Конструктор класса
@version: 1.0.rc <11/11/2010>
@param: mysql_resource
@param: bool
*/
	function __construct($res,$i){
		$this->i=$i;
		$this->res=$res;
	}

/**
@title: Получить кол-во строк результата
@version: 1.0.rc <11/11/2010>
@return: int
*/
	function count_rows(){
		if($this->i){
			if(is_object($this->res)){return($this->res->num_rows);}
			else{return(mysqli_count_rows($this->res));}
		}else{return(mysql_count_rows($this->res));}
	}

/**
@title: Результат пустой?
@version: 1.0.rc <11/11/2010>
@return: bool
*/
	function is_empty(){
		return($this->count_rows()==0);
	}
	
/**
@title: Получить строку результата
@version: 1.0.rc <11/11/2010>
@param: bool
@return: array|false
*/
	function get($named=false){
		if($this->i){
			if($named){
				return(mysqli_fetch_assoc($this->res));
			}else{
				return(mysqli_fetch_row($this->res));
			}
		}else{
			if($named){
				return(mysql_fetch_assoc($this->res));
			}else{
				return(mysql_fetch_row($this->res));
			}
		}
	}

/**
@title: Освободить результат
*/
	function free(){
		if($this->i){
			mysqli_free_result($this->res);
		}else{
			mysql_free_result($this->res);
		}
	}
}
//<< Класс результата запроса =================================================
?>
