<?php
/**
@title: Интерфейс данных модуля интерфейса БД (MySQL)
@package: dbi
@version: 4.0.rc <08/01/2011>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
if(!defined('htaccess')){die('SWC: Direct access disabled');}
if(!isset($blk)||!is_array($blk)){return(setResult(false,'Отсутствуют параметры интерфейса.'));}
if(!isset($blk['get'])||!is_string($blk['get'])||(trim($blk['get'])=='')){return(setResult(false,'Не указан метод интерфейса.'));}
$data['blk']=$blk;

switch($blk['get']){
/**
@title: Вывести форму конфигурации
@version: 1.0.a <05/02/2011>
@param: &conf[name]
 
@todo: Проверка прав текущего пользователя.
@todo: Определение файла конфигурации.
*/
	case 'config':
		//= Проверка прав текущего пользователя >
		//< Проверка прав текущего пользователя =
		if(!isset($data['blk']['pars']['conf']['cfg_name'])||!is_strin($data['blk']['pars']['conf']['cfg_name'])||(trim($data['blk']['pars']['conf']['cfg_name'])=='')){setResult(false,'Не указан файл конфигурации.');return(_e500($data));}
		$data['conf']=cfg_load(cfg_find('dbi'));
		echo(tpl_parse('dbi:conf',$data));
		return(getResult('result'));
	break;

/**
@title: Форма управления БД
@version: 1.0 <13/03/2011>
*/
	case 'manage':
		//= Определение списка доступных конфигураций >
		$data['cList']=array();
		
		echo(tpl_parse('dbi:manage.cfg_list',$data));
		return(getResult('result'));
		//< Определение списка доступных конфигураций =
	break;
	default:
		return(setResult(false,'Неизвестный метод интерфейса [dbi]:'.$blk['get']));
	break;
}

_die('Unclosed interface method: dbi:'.$blk['get']);
?>
