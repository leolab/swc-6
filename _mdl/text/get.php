<?php
/**
@title: Интерфейс данных простых текстовых страниц
@package: mdl.text
@version: 1.0.a <23/02/2011>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
if(!defined('htaccess')){die('Direct access disabled');}
if(!isset($blk)||!is_array($blk)){return(setResult(false,'Отсутствуют параметры интерфейса.'));}
if(!isset($blk['get'])||!is_string($blk['get'])||(trim($blk['get'])=='')){return(setResult(false,'Не указан метод интерфейса.'));}

$data['blk']=&$blk;
$text=mdl_load('text');
if(!is_object($text)){return(setResult(false,'Ошибка загрузки класса модуля. ('.getResult('reason').')'));}

switch($blk['get']){
/**
@title: Получить список файлов
@version: 1.0.a <23/02/2011>
*/
	case 'list':
		if(!isset($blk['pars']['fPath'])||!is_string($blk['pars']['fPath'])){$blk['pars']['fPath']=$GLOBALS['swc.req']['path'];}
		$data['items']=$text->lst($blk['pars']['fPath']);
		echo(tpl_parse('text:list',$data));
		return(getResult('result'));
	break;

/**
@title: Получить блок с текстом
@version: 1.0.a <23/02/2011>
@todo: Реализовать проверку прав пользователя (редактирование блока)
*/
	case 'get':
	case 'blk':
		if(!isset($blk['pars']['name'])){$blk['pars']['name']=trim($GLOBALS['swc.req']['path'].'/'.$GLOBALS['swc.req']['file'],'/');}
		//= Поиск файла >
		$data['content']=$text->get($blk['pars']['name']);
		//< Поиск файла =
		//= Проверка прав пользователя >
		//< Проверка прав пользователя =
		echo(tpl_parse('text:blk',$data));
		return(getResult('result'));
	break;

/**
@title: Получить форму редактирования текста
@version: 1.0.a <12/03/2011>
*/
	case 'edt':
		if(!isset($blk['pars']['name'])){$blk['pars']['name']=trim($GLOBALS['swc.req']['path'].'/'.$GLOBALS['swc.req']['file'],'/');}
		$data['content']=$text->get($blk['pars']['name']);
		//= Проверка прав пользователя >
		//< Проверка прав пользователя =
		echo(tpl_parse('text:edt',$data));
		return(getResult('result'));
	break;

/**
@title: Получить форму (блок) конфигурации
@version: 1.0.a <18/03/2011>
*/
	case 'config':
		$data['conf']=$text->cfg;
		return(setResult(true));
	break;

	default:
		return(_e500('Неизвестный метод: [text]:'.$blk['get']));
	break;
}

_die('Метод [text]:'.$blk['get'].' не вернул результат.');
?>
