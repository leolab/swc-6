<?php
/**
@title: Класс модуля текстовых блоков
@package mdl.text
@version: 1.0.a <24/02/2011>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
if(!defined('htaccess')){die('Direct access disabled');}

class text{

	private $cfg=false;
	private $cName=false;

	const version='mdl.text#1.0.a#2011-02-24#Eugeny Leonov';

/**
@title: Конструктор класса
@version: 1.0 <24/02/2011>
@param: string|false
*/
	function __construct($cName=false){
		if(!is_string($cName)||(trim($cName)=='')){$cName='text';}
		$this->cName=$cName;
		$this->cfg=$this->cfg_load($cName);
		if(!$this->cfg){return(setResult(false,'Ошибка загрузки конфигурации модуля.'));}
		return(setResult(true));
	}

/**
@title: Получить список доступных документов в указанной папке.
@version: 1.0.a <25/02/2011>
@param: string
@return: array
*/
	function lst($sp=''){
		$sp=trim($sp,'/');
/*		if(!isset($this->cfg['stores'])||!is_array($this->cfg['stores'])){
			$this->cfg['stores']=array('@S/%site%/_data/text','@D/text/%site%/','@D/text/');
			if(!cfg_save('@M/text/mdl')){_die('Ошибка обновления конфигурации.');}
		}
*/
		$ret=array();
		if(file_exists(fname($this->cfg['path'].'/'.$sp))&&is_dir(fname($this->cfg['path'].'/'.$sp))){
			$dh=opendir(fname($$this->cfg['path']));
			if(!$dh){}else{
				while($fn=readdir($dh)){
					$fn=trim($fn,'.');
					if($fn!==''){
						if(is_dir($fn)){
							$ret[$fn]=array('folder'=>true,'name'=>$fn);
						}else{
							$ret[$fn]=array('folder'=>false,'name'=>$fn);
						}
					}
				}
				closedir($dh);
			}
		}
		return($ret);
	}

/**
@title: Получить содержание файла
@version: 1.0.a <25/02/2011>
@param: string
@return: string|false
 
@todo: Разбор файлов-шаблонов текстового блока (.htt)
	[] обработка разметки
	[] определение ссылок
*/
	function get($fName){
		setResult(true);
		if(file_exists(fname('@S/%site%/_data/text/'.$fName.'.htm'))){
			return(file_get_contents(fname('@S/%site%/_data/text/'.$fName.'.htm')));
		}elseif(file_exists(fname('@D/text/'.$fName.'.htm'))){
			return(file_get_contents(fname('@D/text/'.$fName.'.htm')));
		}else{
			return('<div class="error">Request data not found ('.$fName.').</div>');
		}
	}

/**
@title: Обработать текстовый файл
@version: 1.0.a <08/03/2011>
@param: string
@return: string|false
*/
	function parse($fName){
		setResult(true);
		
	}

//== Служебные методы >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

/**
@title: Проверить конфигурацию
@version: 1.0.a <25/02/2011>
@param: array
@return: array
*/
	static function cfg_check($cfg){
		if(!is_array($cfg)){return(setResult(false,'Неверный формат параметра.'));}
		$ret=array('conf'=>$cfg);
		if(!isset($cfg['stores'])||!is_array($cfg['stores'])||empty($cfg['stores'])){$ret['err'][]=array('field'=>'stores','message'=>'Не указаны пути хранения файлов данных.',);}

		return($ret);
	}

/**
@title: Загрузить конфигурацию
@version: 1.0.a <18/03/2011>
@param: string|false
@return: array|false
*/
	function cfg_load($cName=false){
		if(!is_string($cName)){$cName='text';}
		$ret=cfg_load('@S/%site%/_conf/'.$cName);
		if(!$ret){$ret=cfg_load('@D/conf/'.$cName);}
		if(!$ret){$ret=cfg_load('@M/'.$cName.'/mdl');}
		return($ret);
	}

/**
@title: Сохранить конфигурацию
@version: 1.0.a <18/03/2011>
@param: array
@return: boolean
*/
	static function cfg_save($cfg){
		return(cfg_save('@S/%site%/_conf/text',$cfg));
	}
//<< Служебные методы ==========================================================
}
?>
