<?php
/**
@title: Интерфейс действия простых текстовых блоков.
@package: mdl.text
@version: 1.0.a <23/02/2011>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
if(!defined('htaccess')){die('Direct access disabled');}
if(!isset($blk)||!is_array($blk)){return(setResult(false,'Отсутствуют данные интерфейса.'));}
if(!isset($blk['act'])||!is_string($blk['act'])||(trim($blk['act'])=='')){return(setResult(false,'Не указан метод интерфейса.'));}

$text=mdl_load('text');
if(!is_object($text)){return(setResult(false,'Ошибка загрузки класса модуля ('.getResult('reason').')'));}

switch($blk['act']){
/**
* Создать текстовый блока
* @version 1.0.a <23/02/2011>
*/
	case 'add':
		return(setResult(false,'Не реализовано.'));
	break;
/**
* Обновить текстовый блок.
* @version 1.0.a <23/02/2011>
*/
	case 'upd':
		return(setResult(false,'Не реализовано.'));
	break;
/**
* Удалить текстовый блок
* @version 1.0.a <23/02/2011>
*/
	case 'del':
		return(setResult(false,'Не реализовано.'));
	break;
	default:
		return(setResult(false,'Неизвестный метод интерфейса text['.$blk['act'].']'));
	break;
}

_die('Метод не вернул результат. text['.$blk['act'].']');
?>
