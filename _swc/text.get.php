<?php
/**
@title: Интерфейс данных модуля текстовых блоков
@package: SWC-6
@subpackage: core.text
@author Eugeny Leonov <eleonov@leolab.info>
@version 1.0.a <14/02/2011>
*/
if(!defined('htaccess')){die('SWC: Direct access disabled');}
if(!isset($blk)||!is_array($blk)){return(setResult(false,'Отсутствуют данные модуля.'));}
if(!isset($blk['get'])||!is_string($blk['get'])||(trim($blk['get'])=='')){return(setResult(false,'Не указан метод интерфейса.'));}
$data['blk']=$blk;

switch($blk['get']){

/**
@title: Вернуть данные блоком в html-формате.
@version: 1.0.a <14/02/2011>
@param: &name string
*/
	case 'html':
		if(!isset($blk['pars']['name'])||!is_string($blk['pars']['name'])||($blk['pars']['name']=='')){return(setResult(false,'Не указано имя элемента данных.'));}
		//= Поиск файла данных >
		$sa[]='@S/_data/text/'.$blk['pars']['name'].'.htm';
		$sa[]='@D/text/%site%/'.$blk['pars']['name'].'.htm';
		$sa[]='@D/text/_all_/'.$blk['pars']['name'].'.htm';
		$fName=where_is($sa);
		if(!$fName){$data['content']='';}else{$data['content']=file_get_contents(fname($fName));}
		echo(tpl_parse('text:html',$data));
		return(getResult('result'));
		//< Поиск файла данных =
	break;

	default:
		return(setResult(false,'Неизвестный метод [text]:'.$blk['get']));
	break;
}

_die('Метод [text]:'.$blk['get'].' не вернул результата.');
?>
