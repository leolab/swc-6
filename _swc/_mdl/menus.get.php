<?php
/**
@title: Интерфейс данных меню
@package: SWC-6
@subpackage: core.menus
@version: 1.0.b <09/01/2011>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
if(!defined('htaccess')){die('SWC: Direct access disabled.');}
if(!isset($blk)||!is_array($blk)){return(setResult(false,'Отсутствуют параметры интерфейса.'));}
if(!isset($blk['get'])||!is_string($blk['get'])||(trim($blk['get'])=='')){return(setResult(false,'Не указан метод интерфейса.'));}
$data['blk']=$blk;

switch($blk['get']){

/**
@title: Вывести меню-список
	Описания элементов меню могут быть заданы следующими путями (в порядке приоритета):
		- массивом элементов в описании блока ($blk['items'])
		- именем файла описания элементов в описании блока ($blk['mnu_name'])
		- именем файла описания элементов в запросе ($blk['pars']['mnu_name'])

	Для проверки возможности вывода элемента меню используются параметры [grp.allow][] и [grp.deny][] соответствующего элемента.
		Если ни один из параметров не определен - элемент выводится.
		Если пользователь не входит ни в одну из групп указанных в параметре [grp.allow][] - элемент не выводится
		Если пользователь входит в одну из групп указанных в параметре [grp.deny][] - элемент не выводится
	Если элемент содержит параметр visible и этот параметр содержит значение эквивалентное {false} - элемент не выводится

	Шаблон для обработки выводимых элементов меню задается параметром ($blk[tpl])
	Поиск шаблона производится для модуля menus.

@version: 1.0.b <09/01/2011>
@param: [&mnu_name] string

#rev.1 <20/01/2011> Eugeny Leonov
	[f] ошибка при попытке определения (поиска) элементов меню из внешнего файла.
	[*] Исправлено имя индекса.
*/
	case 'list':
		if(!isset($blk['items'])||!is_array($blk['items'])){
			if(isset($blk['mnu_name'])){$blk['items']=cfg_load(cfg_find($blk['mnu_name']));}
			elseif(isset($blk['pars']['mnu_name'])){$blk['items']=cfg_load(cfg_find($blk['mnu_name']));}
			else{$blk['items']=false;}
		}
		if(!$blk['items']){return(setResult(false,'Элементы меню не определены.'));}
		foreach($blk['items']as$k=>$v){
			$a=true;
			if(isset($v['grp.allow'])&&is_array($v['grp.allow'])){if(!user_isMember($v['grp.allow'])){$a=false;}}
			if(isset($v['grp.deny'])&&is_array($v['grp.deny'])){if(user_isMember($v['grp.deny'])){$a=false;}}
			if(isset($v['visible'])&&is_false($v['visible'])){$a=false;}
			if($a){$data['items'][$k]=$v;}
		}
		if(!isset($blk['tpl'])||!is_string($blk['tpl'])||(trim($blk['tpl'])=='')){$blk['tpl']='list';}
//		echo(tpl_parse('menus:'.$blk['tpl'],$data));
		echo(tpl_parse('menus:'.$blk['tpl'],$data));
		return(getResult('result'));
	break;
	default:
		return(setResult(false,'Неизвестный метод ('.$blk['get'].') интерфейса [core]:menus'));
	break;
}

_die('Метод '.$blk['get'].' интерфейса [core]:menus не вернул управление.');
?>
