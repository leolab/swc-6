<?php
/**
@title: Интерфейс действия меню
@package: SWC-6
@subpackage: core.menus
@version: 1.0.b <09/01/2011>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
if(!defined('htaccess')){die('SWC: Direct access disabled');}
if(!isset($blk)||!is_array($blk)){return(setResult(false,'Отсутствуют параметры интерфейса.'));}
if(!isset($blk['act'])||!is_string($blk['act'])||(trim($blk['act'])=='')){return(setResult(false,'Не указан метод интерфейса.'));}

switch($blk['act']){

	default:
		return(setResult(false,'Неизвестный метод ('.$blk['act'].') интерфейса [core]:menus'));
	break;
}

_die('Метод '.$blk['act'].' интерфейса [core]:menus не вернул управление.');
?>
