<?php
/**
@title: Интерфейс действия модуля сообщений системы
@package: SWC-6
@subpackage: core
@author: Eugeny Leonov <eleonov@leolab.info>
@version: 1.0.rc <04/11/2010>
*/
if(!defined('htaccess')){die('SWC: Direct access disabled');}
if(!isset($blk)||!is_array($blk)){return(setResult(false,'Интерфейс вызван без параметров'));}
if(!isset($blk['get'])){return(setResult(false,'Не указан метод интерфейса'));}
switch($blk['get']){
/**
@title: Получить сообщения из стека
*/
	case 'messages':
		if(isset($_SESSION['swc.msg'])&&is_array($_SESSION['swc.msg'])){
			echo(tpl_parse('messages:list',array('items'=>$_SESSION['swc.msg'],'blk'=>$blk)));
			if(!getResult('result')){
				echo('<div class="swc_error">Ошибка обработки шаблона: '.getResult('reason').'</div>');
				return(setResult(false,'Ошибка обработки шаблона: '.getResult('reason')));
			}
//			return(setResult(true));
			$_SESSION['swc.msg']=false;
			unset($_SESSION['swc.msg']);
		}
		return(setResult(true));
	break;
	default:
		echo('<div class="swc_error">Неизвестный метод: '.$blk['get'].'</div>');
		return(setResult(false,'Неизвестный метод: '.$blk['get']));
	break;
}
?>
