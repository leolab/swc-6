<?php
/**
@title: Интерфейс действия модуля сообщений системы
@package: SWC
@subpackage: core
@author: Eugeny Leonov <eleonov@leolab.info>
@version: 1.0.rc <04/11/2010>
*/
if(!defined('htaccess')){die('SWC: Direct access disabled.');}
if(!isset($blk)||!is_array($blk)){return(setResult(false,'Интерфейс вызван без параметров'));}
if(!isset($blk['act'])){return(setResult(false,'Не указан метод интерфейса.'));}

switch($blk['act']){
/**
@title: Очистить стек сообщений
*/
	case 'clear':
		if(isset($_SESSION['swc.msg'])){$_SESSION['swc.msg']=false;unset($_SESSION['swc.msg']);}
		return(true);
	break;
	default:
		return(setResult(false,'Неизвестный метод интерфейса: '.$blk['act']));
	break;
}
?>
