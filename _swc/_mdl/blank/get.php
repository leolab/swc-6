<?php
/**
@title: Интерфейс данных "пустого" модуля
@package: SWC
@subpackage: core
@version: 1.0.rc <29/10/2010>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
if(!defined('htaccess')){die('SWC: Direct access disabled');}
if(!isset($blk)||!is_array($blk)){return(setResult(false,'Отсутствуют параметры блока.'));}
if(!isset($blk['get'])||!is_string($blk['get'])){
	if(isset($data['get'])){$blk['get']=$data['get'];}
	elseif(isset($data['pars']['get'])){$blk['get']=$data['pars']['get'];}
	else{$blk['get']='_undefined_';}
}
if(!isset($data)||!is_array($data)){$data=array();}
switch($blk['get']){
	default:
		$dt=array('src'=>'SWC.core:blank','method'=>$blk['get'],'mdl'=>'blank','data'=>$data,'blk'=>$blk);
		echo(tpl_parse('blank:blank',$dt));
		if(!getResult('result')){echo('<div class="swc_error">Ошибка обработки шаблона: '.getResult('reason').' ['.print_r(getResult('from'),true).']</div>');}
		return(setResult(true));
	break;
}
?>
