<?php
/**
@title: SWC.JavaScript handlers
*/
set_contentType('text/javascript');
//$GLOBALS['out']['headers']['Content-type']='text/javascript';
?>
/**
* SWC: JavaScript handlers
* @package SWC
* @subpackage core
* @version 1.0.rc <17/12/2010>
* @author Eugeny Leonov <eleonov@leolab.info>
* $Rev: 153 $
* $Date: 2011-02-22 18:07:42 +0500 (Втр, 22 Фев 2011) $
* $Author: eleonov $
*
* #rev.2 <02/02/2011> Eugeny Leonov
*	[+] fn:substr()
*	[+] fn:strpos()
*
* #rev.1 <21/01/2011> Eugeny Leonov
*	[+] fn:swc_setSite()
*	[r] fn:swc_hlOn()
*	[r] fn:swc_olOff()
*	[r] fn:swc_blk_toggle()
*	[r] fn:swc_blk_show()
*	[r] fn:swc_blk_hide()
*/

/**
* Return part of a string
* @original by: Martijn Wieringa
*/
function substr( f_string, f_start, f_length ) {
	if(f_start<0){f_start+=f_string.length;}
	if(f_length==undefined){f_length=f_string.length;}else{if(f_length<0){f_length+=f_string.length;}else{f_length+=f_start;}}
	if(f_length<f_start){f_length=f_start;}
	return f_string.substring(f_start, f_length);
}

/**
* Find position of first occurrence of a string
* @original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net) 
*/
function strpos( haystack, needle, offset){ 
	var i=haystack.indexOf(needle,offset); // returns -1
	return i >= 0 ? i : false;
}

/**
* Подсветить элемент
* @version 1.0.rc <13/12/2010>
* @param id
*
* #Relise:1.0.r <21/01/2011> Eugeny Leonov
*/
function swc_hlOn(id){
	$('#'+id).addClass('highlight');
}

/**
* Снять подсветку с элемента
* @version 1.0.rc <13/12/2010>
* @param id
*
* #Relise:1.0.r <21/01/2011> Eugeny Leonov
*/
function swc_hlOff(id){
	$('#'+id).removeClass('highlight');
}

/**
* Изменить (переключить) отображение блока.
* @version 1.0.rc <05/12/2010>
* @param id
*	Идентификатор элемента-переключателя
*	Текст элемента меняется (+/-).
*	Скрывается-открывает блок с ID=id+'_blk'
*
* #Relise:1.0.r <21/01/2011> Eugeny Leonov
*/
function swc_blk_toggle(id){
	$("#"+id+'_blk').slideToggle(0,function(){if($("#"+id+'_blk').css('display')=='none'){$("#"+id).text('+');}else{$("#"+id).text('-');}});
}

/**
* Скрыть или отобразить элементы
* @version 1.0.rc <05/12/2010>
* @param id
*	Идентификатор переключаемого элемента
* @param bool show
*	Скрыть (false) или отобразить элемент
*
* #Relise:1.0.r <21/01/2011> Eugeny Leonov
*/
function swc_blk_show(id,show){
	if(show){ //Показать
		$("#"+id).slideDown("slow");
	}else{ //Скрыть
		$("#"+id).slideUp("slow");
	}
}

/**
* Строка - положительный результат выполнения запроса?
* @version 1.0.a <05/12/2010>
* @param str string
*/
function is_ok(str){
	if(str.indexOf('@Ok')>=0){return(true);}else{return(false);}
	str=lines_pop(str);
	if(str.indexOf(':')){ts=str.substring(0,str.indexOf(':'));}else{ts=str;}
	if(ts.substr(1,2).toLowerCase()=='ok'){return(true);}else{return(false);}
}

/**
* Вывод сообщения ядра
* @version 1.0.a <17/12/2010>
* @param string
* @param string('notify'|'error')
*
* @todo: Разбор второго параметра, по умолчанию ='notify'
* @todo: Проверка строку на SWC_Result - тип. Корректировка типа сообщения (если не указано)
* @todo: Вменяемые окна сообщений.
*/
function swc_alert(str,type){
	alert(str);
}

/**
*Сменить тему (для вызова из селектора)
*@version 1.0.a <12/12/2010>
*@param theme string
*
* #rev.1 <21/01/2011> Eugeny Leonov
*	[*] Откорректирован формат вызываемого метода
*/
function swc_setTheme(theme){
	window.location='<?=href('/',array('act'=>'theme.select'));?>'+'&theme[name]='+theme;
}

/**
* Сменить сайт (для вызова из селектора)
* @version 1.0.b <21/01/2011>
* @param site string
*/
function swc_setSite(site){
	$.post('/',{
		'do':'site:select',
		'site[name]':site
	},function(data){
		if(!is_ok(data)){swc_alert(data);}else{window.location='<?=href('/');?>';}
	});
//	window.location='<?=href('/',array('act'=>'site:select'));?>';
}


/**
* Открыть блок в модальном окне
* @version 1.0.a <22/02/2011>
* @param href string
* @param pars object
*/
/*
function swc_showModal(url,pars){
	if($('#modalbox').length==0){$(document).append('<div class="modalbox" id="modalbox">&nbsp;</div>');}
	$.post(url,pars,function(data){
		alert(data);
		$('#modalbox').html(data);
		$('#modalbox').modal();
		$('#modalbox').css('display','block');
	});
}
*/
