<?php
/**
@title: Файл функций обработки текста
@package: SWC-6
@subpackage: core
@version: 1.0.rc <15/12/2010>
*/
if(!defined('htaccess')){die('SWC: Direct access disabled.');}

/**
@title: Обработать текст с Ext-разметкой
@version: 1.0.rc <15/12/2010>
@param: string
@return: string
@fixme: "Жрет" много памяти.
*/
function text_parse($txt){
	setResult(true);
	if(!is_string($txt)){return(setResult(false,'Неверный формат параметра.'));}
	if(trim($txt)==''){return('');}
	$txt=explode("\n",$txt);
	$ret='';
	$tC=array('div'=>0,);
	foreach($txt as $k=>$v){
		$v=trim($v);
		if(strlen($v)){
			switch($v[0]){
				case '#': $ret.='<p><b>'.trim(stripslashes($v),'#').'</b></p>'; break;
				case '>': if($v[1]=='>'){$ret.='<div style="margin-left:20px;">'.trim(stripslashes($v),'>');$tC['div']++;}else{$ret.='<p style="margin-left:20px;">'.trim(stripslashes($v),'>').'</p>';} break;
				case '<': if($v[1]=='<'){if($tC['div']>0){$ret.='</div>'.trim(stripslashes($v),'<');$tC['div']--;}}else{$ret.='<p style="margin-left:-20px;">'.trim(stripslashes($v),'<').'</p>';} break;
				case '-': $ret.='<p style="margin-left:15px;">'.stripslashes($v).'</p>'; break;
				case '=': if($v[1]=='='){if($tC['div']>0){$ret.='</div>';$tC['div']--;}$ret.='<h3>'.trim(stripslashes($v),'= ').'</h3><div style="margin-left:10px;">';$tC['div']++;} break;
				case '[': if(strlen(trim($v)>3)){if(($v[1]!='')&&($v[2]==']')){switch($v[1]){
					case '+': $ret.='<p> + '.substr($v,3).'</p>'; break;
					case '-': $ret.='<p> - '.substr($v,3).'</p>'; break;
					case '*': $ret.='<p> * '.substr($v,3).'</p>'; break;
					case '#': $ret.='<p> # '.substr($v,3).'</p>'; break;
					case '!': $ret.='<p> ! '.substr($v,3).'</p>'; break;
					default: $ret.='<p>'.stripslashes($v).'</p>'; break;
				}}}break;

				default: $ret.='<p>'.stripslashes($v).'</p>';
			}
		}
	}
	while($tC['div']>0){$ret.='</div>';$tC--;}
	return($ret);
}
?>
