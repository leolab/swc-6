<?php
/**
@title: Функции работы со страницами и блоками
@package: SWC-6
@subpackage: core.page
@version: 1.0.b <10/01/2011>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
if(!defined('htaccess')){die('SWC: Direct access disabled');}


/**
@title: Найти и загрузить параметры страницы
	Поиск параметров страницы производится в порядке:
		- папка текущего сайта
		- папка страниц ядра

@version: 1.0.rc <26/10/2010>
@param: array|string|false
@param: bool - true - не пытаться использовать класс страницы.
@return: array|false

#rev.3 <19/02/2011> Eugeny Leonov
	[-] Попытка обработки классом

#rev.2 <29/01/2011> Eugeny Leonov
	[*] Параметры страниц сайта загружаются из папки ./_pages/ текущего сайта.

#rev.1 <10/01/2011> Eugeny Leonov
	[*] При возможности обработки классом - результат выполнения метода класса возвращается без проверки.
*/
function pg_load($pgs=false,$self=false){
	$ret=false;
//	if(!$self){$pg=mdl_load('page');if(is_object($pg)&&method_exists($pg,'load')){return($pg->load($pgs));}}
	if(!$pgs){$pg=reqGet();}
	if(is_string($pgs)){
		$pgs=str_replace('\\','/',$pgs);
		$pg['url']=$pgs;
		if(strpos($pg['url'],'/')!=false){
			$pg['path']=trim(substr($pg['url'],0,strrpos($pg['url'],'/')),'/');
			$pg['file']=trim(substr($pg['url'],strrpos($pg['url'])),'/');
			if(!$pg['file']){$pg['file']='index';}
		}else{
			$pg['file']=$pg['url'];$pg['path']='';
		}
	}elseif(is_array($pgs)){$pg=$pgs;}
	if(!$pg){_die('Unrecognized error');}
_msg('Page: [url]='.$pg['url'].' [path]='.$pg['path'].' [file]='.$pg['file'],'DN','SWC.core');
	//= Поиск описания страницы >
	$ret=false;
	if(cfg_exists('@S/%site%/_pages/'.$pg['path'].'/'.$pg['file'])){$ret=cfg_load('@S/%site%/_pages/'.$pg['path'].'/'.$pg['file']);}
	elseif(cfg_exists('@S/%site%/_pages/'.$pg['path'].'/_index')){$ret=cfg_load('@S/%site%/_pages/'.$pg['path'].'/_index');}
	elseif(cfg_exists(swc_base.'/_pages/'.$pg['path'].'/'.$pg['file'])){$ret=cfg_load(swc_base.'/_pages/'.$pg['path'].'/'.$pg['file']);}
	elseif(cfg_exists(swc_base.'/_pages/'.$pg['path'].'/_index')){$ret=cfg_load(swc_base.'/_pages/'.$pg['path'].'/_index');}
	//< Поиск описания страницы =
	if(!$ret){return(setResult(false,'Страница не найдена: ['.$pg['path'].'/'.$pg['file'].'].'));}
	else{setResult(true);return($ret);}
}

/**
@title: Обработать страницу и вернуть данные
@version: 1.0.rc <09/11/2010>
@param: array
@param: bool
@return: array|false

#rev.2 <19/02/2011> Eugeny Leonov
	[-] Попытка обработки классом.
	[*] Изменена логика проверки прав доступа к странице.
	[+] Поиск страницы e403 производится: из конфигурации сайта; из конфигурации ядра; страница {site|core}/e403

#rev.1 <10/01/2011> Eugeny Leonov
	[*] При возможности обработки классом - результат выполнения метода класса возвращается без проверки.
	[!] Параметр $page - массив полученный из функции pg_load()
*/
function pg_parse($page,$self=false){
//	if(!$self){$pg=mdl_load('page');$ret=false;if(is_object($pg)&&method_exists($pg,'parse')){return($pg->parse($pgs));}}
	if(!is_array($page)){$page=pg_load($page);}
	if(!$page||!is_array($page)){
		return(setResult(false,'Неверный параметр.'));
	}
	//= Проверка прав доступа >
	$a=false;
	if(user_isAllow($page)){$a=true;}
//	if(!a){if(user_isAllow($page,'grp.deny')){$a=false;}}
//	if(isset($page['grp.allow'])&&is_array($page['grp.allow'])){if(!user_isMember($page['grp.allow'])){$a=false;}}
//	if(isset($page['grp.deny'])&&is_array($page['grp.deny'])){if(user_isMember($page['grp.deny'])){$a=false;}}
	if(!$a){
		$page=false;
		if(isset($_SESSION['swc.site']['e403'])){$page=pg_load($_SESSION['swc.site']['e403']);}
		if(!$page){if(isset($_SESSION['swc.cfg']['default']['e403'])){$page=pg_load($_SESSION['swc.cfg']['default']['e403']);}}
		if(!$page){$page=pg_load('/e403');}
		if(!$page||!is_array($page)){return(setResult(false,'Ошибка загрузки параметров страницы: '.getResult('reason')));}
	}
	//< Проверка прав доступа =
	//= Определение шаблона страницы >
//	if(!isset($page['tpl'])||!is_string($page['tpl'])){$page['tpl']=false;}
	if(!isset($page['tpl'])){$page['tpl']=false;}
	if(!$page['tpl']){if(isset($GLOBALS['swc.page']['tpl'])){$page['tpl']=$GLOBALS['swc.page']['tpl'];}}
	if(!$page['tpl']){if(isset($GLOBALS['swc.tpl'])){$page['tpl']=$GLOBALS['swc.tpl'];}}
	if(!$page['tpl']){if(isset($_SESSION['swc.theme'])){if(isset($_SESSION['swc.theme']['default']['tpl'])){$page['tpl']=$_SESSION['swc.theme']['default']['tpl'];}}}
	if(!$page['tpl']){if(isset($_SESSION['swc.site'])){if(isset($_SESSION['swc.site']['default']['tpl'])){$page['tpl']=$_SESSION['swc.site']['default']['tpl'];}}}
// {-}
	if(!$page['tpl']){if(isset($_SESSION['swc.cfg']['default']['tpl'])){$page['tpl']=$_SESSION['swc.cfg']['default']['tpl'];}}
	// Шаблон не найден.
	if(!$page['tpl']){_die('Для запрошенной страницы не указан шаблон.');}
	if(!is_array($page['tpl'])){$page['tpl']=tpl_load($page['tpl'].'.pg');}
	if(!$page['tpl']){if(!getResult('result')){return(setResult(false,getResult('reason')));}else{return(setResult(false,'Ошибка загрузки параметров шаблона страницы.'));}}
	//< Определение шаблона страницы =
	//= Обрабатываем главный блок страницы >
	if(isset($page['blk'])){
		$ret['main']=blk_get($page['blk'],array('pos'=>'main','num'=>0,'page'=>$page));
		if(!isset($ret['main']['content'])||trim($ret['main']['content']=='')){
			if(!getResult('result')){
				$ret['main']['content']=tpl_parse('e500',array('result'=>getResult(),'pos'=>'main','num'=>0,'blk'=>$page['blk'],'page'=>$page));
				if(!$ret['main']['content']){$ret['main']['content']='<div class="swc_error">Ошибка обработки главного блока страницы</div>';}
			}
		}
	}
	//< Обрабатываем главный блок страницы =
	//= Обрабатываем блоки (позиции) шаблона >
	if(isset($page['tpl']['blk'])&&is_array($page['tpl']['blk'])){foreach($page['tpl']['blk']as$pk=>$pos){
		if(isset($page['blk.'.$pos])&&is_array($page['blk.'.$pos])){foreach($page['blk.'.$pos]as$num=>$blk){
			$ret[$pos][$num]=blk_get($blk,array('pos'=>$pos,'num'=>$num,'page'=>$page));
			if(!is_array($ret[$pos][$num])||!isset($ret[$pos][$num]['content'])||(trim($ret[$pos][$num]['content'])=='')){
				if(!getResult('result')){
					$ret[$pos][$num]['ret']=false;
					$ret[$pos][$num]['content']=tpl_parse('e500',array('result'=>getResult(),'pos'=>$pos,'num'=>$num,'blk'=>$blk,'page'=>$page));
					$ret[$pos][$num]['result']=getResult();
				}else{
					//Считается что блок не должен быть показан.
					unset($ret[$pos][$num]);
				}
			}
		}}
		if(isset($ret[$pos])&&empty($ret[$pos])){
			//Если в позиции шаблона нет ни одного блока - позицию прибиваем
			unset($ret[$pos]);
		}
	}setResult(true);}
	else{_msg('Для шаблона ('.$page['tpl']['name'].') не указано ни одной позиции.','N','SWC.core');setResult(false,'Для шаблона ('.$page['tpl']['name'].') не указано ни одной позиции.');}
	//< Обрабатываем блоки (позиции) шаблона =
	if(!isset($GLOBALS['out']['head'])||!is_array($GLOBALS['out']['head'])){$GLOBALS['out']['head']=array();}
	if(!isset($GLOBALS['out']['head']['title'])){if(isset($page['title'])){$GLOBALS['out']['head']['title']=$page['title'];}}
	if(!isset($GLOBALS['out']['head']['keywords'])){if(isset($page['keywords'])){$GLOBALS['out']['head']['keywords']=$page['keywords'];}}
	setResult(true);
	return(tpl_parse($page['tpl'],array('body'=>$ret,'head'=>$GLOBALS['out']['head'])));
}

?>
