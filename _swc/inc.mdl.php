<?php
/**
@title: Файл функций работы с модулями
@package: SWC-6
@subpackage: core
@version: 1.0.a <08/01/2011>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
if(!defined('htaccess')){die('SWC: Direct access disabled');}


/**
@title: Получить (загрузить) информацию о модуле
	Поиск информации о модуле производится в:
		- текущем сайте
		- общих модулях
		- модулях ядра
	Если модуль не имеет собственной папки - это системный модуль. Для него устанавливается флаг ['sys']=true

@version: 1.0.rc <04/01/2011>
@param: string
@return: array|false
*/
function mdl_info($mName){
	$site='%site%';
	if(strpos($mName,':')){list($site,$mName)=explode(':',$mName,2);}
	$ret=ini_load('@S/'.$site.'/_mdl/'.$mName.'/mdl.info');
	if(is_array($ret)){$ret['site']=$site;}
	if(!$ret){
		$ret=ini_load('@S/'.$site.'/'.$mName.'.info');
		if(is_array($ret)){$ret['sys']=true;$ret['site']=$site;}
	}
	if(!$ret){$ret=ini_load('@M/'.$mName.'/mdl.info');}
	if(!$ret){
		$ret=ini_load('@M/'.$mName.'.info');
		if(is_array($ret)){$ret['sys']=true;}
	}
	if(!$ret){$ret=ini_load(swc_base.'/_mdl/'.$mName.'/mdl.info');}
	if(!$ret){
		$ret=ini_load(swc_base.'/_mdl/'.$mName.'.info');
		if(is_array($ret)){$ret['sys']=true;}
	}
	if(!$ret){$ret=array('name'=>$mName);}
	return($ret);
}


/**
@title: Получить список модулей системы
	Модули не имеющие собственной папки являются системными. Для них устанавливается параметр ['sys']=true
	Если параметр $s установлен в true - производится поиск модулей во всех сайтах системы
	Для модуля сайта устанавливается параметр ['site'] содержащий наименование соответствующего сайта.
	Ключ массива для модуля сайта предваряется именем сайта и двоеточием.

@version: 1.0.rc <28/12/2010>
@param: [bool]=false - загружать глобальные параметры модуля.
@param: [bool]=false - загружать все модули системы.
@return: array|false

@todo: Загрузка доступных модулей с сервера обновлений
@fixme: Наличие модуля должно определяться по файлу mdl.info

#rev.6 <19/03/2011>	Eugeny Leonov
	[+] Загрузка базовых модулей ядра
	[f] Наличие модуля определяется по файлу *.info
 
#rev.5 <07/02/2011> Eugeny Leonov
	[!] Изменены параметры функции:
		первый параметр - загружать конфиг модуля или нет, по умолчанию - не загружать.
		второй параметр - искать все модули или только доступные, по умолчанию - только доступные (в области видимости)
	[!] Данные берутся из файла конфигурации модуля.

#rev.4 <11/01/2011> Eugeny Leonov
	[-] Создание папок-хранилищ модулей.
	[+] Проверка существования папок-хранилищ модулей.

#rev.3 <09/01/2011> Eugeny Leonov
	[f]	Ошибка открытия папок-хранилищ модулей при их отсутствии
	[+]	Создание папки-хранилища модулей ядра при ее отсутствии
	[+]	Создание файла правил запрещающего прямой доступ к папке
	[+]	Создание папки-хранилища общих модулей при ее отсутствии
	[+]	Создание файла правил запрещающего прямой доступ к папке

#rev.2 <30/12/2010> Eugeny Leonov
	[+] Реализована загрузка информации несамостоятельных модулей

#rev.1 <29/12/2010> Eugeny Leonov
	[+] Реализована загрузка локальных модулей.
*/
function mdl_list($f=false,$s=false){
	$ret=false;
	//= Загружаем список базовых модулей ядра системы >
	$mPath=fname(swc_base);
	if(file_exists($mPath)&&is_dir($mPath)){
		$dh=opendir($mPath);
		if($dh==false){_e500('Ошибка открытия папки ядра системы.');}
		while($fn=readdir($dh)){
			$fn=trim($fn,'.');
			if(($fn!='')&&strpos($fn,'.')){
				$fe=trim(substr($fn,strrpos($fn,'.')),'.');
				if($fe=='info'){
					$mi=ini_load($fn);
					if(is_array($mi)&&isset($mi['name'])){
						$mi['sys']=true;
						$ret[$mi['name']]=$mi;
					}
				}
			}
		}
		closedir($dh);
	}
	//< Загружаем список базовых модулей ядра системы =
	//= Загружаем список дополнительных модулей ядра системы >
	$mPath=fname(swc_base.'/_mdl');
	if(file_exists($mPath)&&is_dir($mPath)){
		$dh=opendir($mPath);
		if($dh!==false){while($fn=readdir($dh)){
			$fn=trim($fn,'.');
			if($fn!=''){
				$mi=false;
				if(is_dir(fname($mPath.'/'.$fn))&&file_exists(fname($mPath.'/'.$fn.'/mdl.info'))){
					$mi=ini_load($mPath.'/'.$fn.'/mdl.info');
				}elseif(file_ext($fn)=='info'){
					$mi=ini_load($mPath.'/'.$fn);
					if(is_array($mi)){$mi['sys']=true;}
				}
				if(is_array($mi)&&isset($mi['name'])){$ret[$mi['name']]=$mi;}
			}
		}closedir($dh);}
	}
	//< Загружаем список дополнительных модулей ядра системы =
	//= Загружаем список общих модулей >
	// Общие модули ВСЕГДА находятся в собственных папках.
	$mPath=fname('@M/');
	if(file_exists($mPath)&&is_dir($mPath)){
		$dh=opendir($mPath);
		if($dh!==false){while($fn=readdir($dh)){
			$fn=trim($fn,'.');
			if(($fn!='')&&file_exists(fname($mPath.'/'.$fn.'/mdl.info'))){
				$mi=ini_load($mPath.'/'.$fn.'/mdl.info');
				if(is_array($mi)&&isset($mi['name'])){$ret[$mi['name']]=$mi;}
			}
		}closedir($dh);}
	}
	//< Загружаем список общих модулей =
	if(!$s){
		//= Загрузка модулей текущего сайта >
		$sn=fname('%site%');
		if($sn!='_default'){
			$mPath=fname('@S/'.$sn.'/_mdl/');
			if(file_exists($mPath)&&is_dir($mPath)){
				$dh=opendir($mPath);
				if($dh!==false){while($fn=readdir($dh)){
					$fn=trim($fn,'.');
					if($fn!=''){
						$mi=false;
						if(file_exists(fname($mPath.'/'.$fn.'/mdl.info'))){
							$mi=ini_load(fname($mPath.'/'.$fn.'/mdl.info'));
						}elseif(file_ext($fn)=='info'){
							$mi=ini_load($mPath.'/'.$fn);
							if(is_array($mi)){$mi['sys']=true;}
						}
						if(is_array($mi)&&isset($mi['name'])){$ret[$mi['name']]=$mi;}
					}
				}closedir($dh);}
			}
		}
		//< Загрузка модулей текущего сайта =
	}else{
		//= Загрузка модулей всех сайтов >
		$sPath=fname('@S/');
		if(file_exists($sPath)&&is_dir($sPath)){
			$sdh=opendir($sPath);
			if($sdh!==false){while($sn=readdir($sdh)){
				$sn=trim($sn,'.');
				if(($sn!='')&&is_dir($sPath.'/'.$sn.'/_mdl/')){
					$mPath=$sPath.'/'.$sn.'/_mdl';
					if(file_exists($mPath)&&is_dir($mPath)){
						$dh=opendir($mPath);
						if($dh!==false){while($fn=readdir($dh)){
							$fn=trim($fn,'.');
							if($fn!=''){
								$mi=false;
								if(is_dir($mPath.'/'.$fn)&&file_exists($mPath.'/'.$fn.'/mdl.info')){
									$mi=ini_load($mPath.'/'.$fn.'/mdl.info');
								}elseif(file_ext($fn)=='info'){
									$mi=ini_load($mPath.'/'.$fn);
									if(is_array($mi)){$mi['sys']=true;}
								}
								if(is_array($mi)&&isset($mi['name'])){
									$mi['site']=$sn;
									$ret[$sn.':'.$mi['name']]=$mi;
								}
							}
						}closedir($dh);}
					}
				}
			}closedir($sdh);}
		}
		//< Загрузка модулей всех сайтов =
	}
	setResult(true);
	return($ret);
}

/**
@title: Выполнить интерфейс данных модуля
@version: 1.0.rc.2 <06/01/2011>
@param: string
@param: [array]=false
@return: string
*/
function int_get($int,$pars=false){
	if(!is_string($int)||(trim($int)=='')){setResult(false,'Неверный формат имени интерфейса.');return(tpl_parse('e500',array('content'=>getResult('reason'))));}
	$blk=blk_load($int);
	if(!$blk){
		setResult(false,'Ошибка выполнения интерфейса ('.$int.'): '.getResult('reason'));
		return(tpl_parse('e500',array('content'=>getResult('reason'))));
	}
	if(!isset($blk['pars'])||!is_array($blk['pars'])){$blk['pars']=array();}
	if(is_array($pars)){
		$blk['pars']=array_merge_recursive($blk['pars'],$pars);
	}
	$ret=blk_get($blk);
	if(!$ret){
		return(tpl_parse('e500',array('content'=>'Ошибка выполнения интерфейса модуля: '.getResult('reason'))));
	}
	if(!isset($ret['result']['reason'])){$ret['result']['reason']='';}
	if(!isset($ret['content'])||(trim($ret['content'])=='')){if($ret['result']['reason']!=''){$ret['content']=$ret['result']['reason'];}else{$ret['content']='<div class="empty">Empty content</div>';}}
	setResult(true);
	return($ret['content']);
}


/**
@title: Проверить, не запрещен ли модуль.
@version: 1.0.rc <21/11/2010>
@param: string
@return: bool
*/
function mdl_disabled($mName){
	setResult(true);
	if(isset($_SESSION['swc.cfg']['mdl.disabled'])&&is_array($_SESSION['swc.cfg']['mdl.disabled'])){return(in_array($mName,$_SESSION['swc.cfg']['mdl.disabled']));}
	return(false);
}

/**
@title: Проверить наличие модуля по имени
@version: 1.0 <13/03/2011>
@param: string
@param: bool
@return: bool
*/
function mdl_exists($name,$all=false){
	$sa=array(
		'@S/%site%/_mdl/'.$name.'/mdl.php',
		'@S/%site%/_mdl/'.$name.'.mdl.php',
		'@M/'.$name.'/mdl.php',
		'@M/'.$name.'.mdl.php',
		swc_base.'/_mdl/'.$name.'/mdl.php',
		swc_base.'/_mdl/'.$name.'.mdl.php',
		swc_base.'/'.$name.'.mdl.php',
	);
	return(where_is($sa)!==false);
}

/**
@title: Загрузить и инициализировать класс модуля
@version: 1.0.rc <01/11/2010>
@param: string
@param: [string|array]=false
@return: class|false

@fixme: При попытке повторной загрузки класса модуля (при неудачной предыдущей), не производится реальная загрузка модуля.

#rev.4 <19/03/2011> Eugeny Leonov
	[+] Добавлен поиск модулей ядра системы.

#rev.3 <26/01/2011> Eugeny Leonov
	[-] Убрана проверка запрещенных модулей.
	[*] Имя модуля может быть составным. При этом именем класса модуля является часть строки за последним символом "/".

#rev.2 <20/11/2010> Eugeny Leonov
	[+] Обработка параметра ядра системы $_session['swc.cfg']['mdl.disabled']

#rev.1 <09/11/2010> Eugeny Leonov
	[+] поиск и сохранение созданного класса модуля производится по хешу (имя модуля + параметры)
	[+] классу при создании передаются параметры, или значение false
	[*] отладочные сообщения выводятся с собственным постфиксом (SWC.core-mdl_load)
*/

function mdl_load($mName,$mPars=false){
	setResult(true);
//	if(isset($_SESSION['swc.cfg']['mdl.disabled'])&&is_array($_SESSION['swc.cfg']['mdl.disabled'])){if(in_array($mName,$_SESSION['swc.cfg']['mdl.disabled'])){_msg('Запрещенный модуль ('.$mName.') не загружен.','D','SWC.core');return(setResult(false,'Модуль запрещен.'));}}
	$mFile=false;
	$mHash=md5($mName.serialize($mPars));
	if(isset($GLOBALS['swc.mdl'][$mHash])&&is_object($GLOBALS['swc.mdl'][$mHash])){
		if(defined('dbgEnabled')&&dbgEnabled){_msg('Найден класс модуля '.$mName.' Хеш: '.$mHash.' По запросу: '._dbg(1,'%file%:%line%'),'D','SWC.core-mdl_load');}
		return($GLOBALS['swc.mdl'][$mHash]);
	}
	//= Ищем файл класса модуля >
	$sa=array(
		'@S/%site%/_mdl/'.$mName.'/mdl.php',
		'@S/%site%/_mdl/'.$mName.'.mdl.php',
		'@M/'.$mName.'/mdl.php',
		'@M/'.$mName.'.mdl.php',
		swc_base.'/_mdl/'.$mName.'/mdl.php',
		swc_base.'/_mdl/'.$mName.'.mdl.php',
		swc_base.'/'.$mName.'.mdl.php',
	);
	$mFile=where_is($sa);
	if(!$mFile){return(setResult(false,'Не найден файл класса модуля ('.$mName.').'));}
	//< Ищем файл класса модуля =
	if(!require_once(fname($mFile))){
		_msg('Ошибка загрузки файла класса модуля: '.getResult('reason'),'N','SWC.core');
		return(setResult(false,'Ошибка загрузки класса модуля ('.$mName.'): '.getResult('reason')));
	}
	if(strpos($mName,'/')){$cName=trim(substr($mName,strrpos($mName,'/')),'/');}else{$cName=$mName;}
	if(class_exists($cName)){$GLOBALS['swc.mdl'][$mHash]=new $cName($mPars);}
	else{$GLOBALS['swc.mdl'][$mHash]=false;_msg('Для модуля '.$mName.' не определен класс ('.$cName.').','N','SWC.core');return(setResult(false,'Для модуля '.$mName.'не определен класс ('.$cName.').'));}
	if(defined('dbgEnabled')&&dbgEnabled){_msg('Загружен класс модуля: '.$mName.', файл: '.fname($mFile).' Хеш: '.$mHash.' По запросу: '._dbg(1,'%file%:%line%'),'D','SWC.core-mdl_load');}
	if(getResult('result')){return($GLOBALS['swc.mdl'][$mHash]);}else{$GLOBALS['swc.mdl'][$mHash]=false;return(setResult(false,'Ошибка инициализации класса модуля '.$cName.' ('.$mName.'): '.getResult('reason')));}
}

/**
@title: Найти файл интерфейса модуля
@version: 1.0.rc.1 <18/12/2010>
@param: string
@return: string|false
*/
function int_find($mName,$int='get'){
	if(!is_string($mName)){return(setResult(false,'Неверный вызов функции.'));}
	$pList=false;
	if(isset($_SESSION['swc.cfg']['mdl.disabled'])&&is_array($_SESSION['swc.cfg']['mdl.disabled'])){
		if(in_array($mName,$_SESSION['swc.cfg']['mdl.disabled'])){
			$pList=array(
				swc_base.'/_mdl/'.$mName.'/'.$int.'.php',
				swc_base.'/_mdl/'.$mName.'.'.$int.'.php',
				swc_base.'/'.$mName.'.'.$int.'.php',
				swc_base.'/'.$int.'.php',
			);
		}
	}
	if(!$pList){$pList=array(
		'@T/%theme%/_res/'.$mName.'/'.$int.'.php',
		'@T/%theme%/_res/'.$mName.'.'.$int.'.php',
		'@S/%site%/_blk/'.$mName.'/'.$int.'.php',
		'@S/%site%/_blk/'.$mName.'.'.$int.'.php',
		'@S/%site%/_mdl/'.$mName.'/'.$int.'.php',
		'@S/%site%/_mdl/'.$mName.'.'.$int.'.php',
		'@M/'.$mName.'/'.$int.'.php',
		'@M/'.$mName.'.'.$int.'.php',
		swc_base.'/_blk/'.$mName.'/'.$int.'.php',
		swc_base.'/_blk/'.$mName.'.'.$int.'.php',
		swc_base.'/_mdl/'.$mName.'/'.$int.'.php',
		swc_base.'/_mdl/'.$mName.'.'.$int.'.php',
		swc_base.'/'.$mName.'.'.$int.'.php',
		swc_base.'/'.$int.'.php',
	);}
	$ret=where_is($pList);
	if(!$ret){if(!getResult('result')){return(setResult(false,'Интерфейс модуля ('.$mName.') не найден. ['.getResult('reason').']'));}return(setResult(false,'Интерейс модуля не найден. Неожиданная ошибка.'));}
	setResult(true);return($ret);
}

?>
