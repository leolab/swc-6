<?php
/**
@title: Функции работы с датой/временем
@author: Eugeny Leonov <eleon@leolab.info>
@package: SWC-6
@subpackage: core
@version: 0.7.r <02/10/2008>

#Rev.1 <02/10/2008> Eugeny Leonov
	[*] sqlDate2sDate()
		Добавлен параметр bool $sh

	[*] is_sqlDate()
	[*] is_sqlTime()
*/
if(!defined('htaccess')){die('SWC : Direct access disabled.');}
//== Функции проверки валидности даты/времени >>

/**
@title: Преобразовать невалидную MySQL дату в валидную
@author: Eugeny Leonov <eleonov@leolab.info>
@version: 0.1 <31/03/2011>
@param: string
@return: string|false
*/
function badSQL2sqlDate($str){
	$ret='';
	$dta=explode('-',$str);
	if(count($dta)!=3){return false;}
	if(strlen($dta[2])==1){$dta[2]='0'.$dta[2];}
	if(strlen($dta[1])==1){$dta[1]='0'.$dta[1];}
	$ret=$dta[0].'-'.$dta[1].'-'.$dta[2];
	if(!is_sqlDate($ret)){return(false);}
	return($ret);
}

/**
@title: Проверить валидность MySQL даты
@author: Eugeny Leonov <eleon@leolab.info>
@version: 0.1 <10/12/2004>
@param: string
@return: bool

#rev.1 <19/09/2008> Eugeny Leonov
	[*] Нулевая дата (0000-00-00) считается валидной
*/
function is_sqlDate($dt){
	if($dt=='0000-00-00'){return true;}
	$dta=explode('-',$dt);
	if(count($dta)!=3){return false;}
	if(strlen($dta[2])!=2){return false;} //Day
	if(strlen($dta[1])!=2){return false;} //Month
	if(strlen($dta[0])!=4){return false;} //Year
	if(!is_numeric(trim($dta[0],'0'))){return false;}
	if(!is_numeric(trim($dta[1],'0'))){return false;}
	if(!is_numeric(trim($dta[2],'0'))){return false;}
	return checkdate($dta[1],$dta[2],$dta[0]);
}

/**
@title: Проверить валидность MySQL времени
@author: Eugeny Leonov <eleon@leolab.info>
@version: 0.1 <10/12/2004>
@param: string
@return: bool

#rev.1 <19/09/2008> Eugeny Leonov
	[*] Время заданное двумя числами считается валидным
*/
function is_sqlTime($dt){
	$dta=explode(':',$dt);
	if(count($dta)<2){return false;}
	if(strlen($dta[0])!=2){return false;}
	if(!is_numeric($dta[0])){return false;}
	if(strlen($dta[1])!=2){return false;}
	if(!is_numeric($dta[1])){return false;}
	if(count($dta)==3){
		if(strlen($dta[2])!=2){return false;}
		if(!is_numeric($dta[2])){return false;}
	}else{$dta[2]=0;}
	return checktime($dta[0],$dta[1],$dta[2]);
}

/**
@desc: Проверитть валидность MySQL даты-времени
@author: Eugeny Leonov <eleon@leolab.info>
@version: 0.1 <10/12/2004>
@param: string
@return: bool

#rev.1 <19/09/2008> Eugeny Leonov
*/
function is_sqlDateTime($dt){
	$dta = explode(' ',$dt);
	if(!is_sqlDate($dta[0])){return false;}
	if(!is_sqlTime($dta[1])){return false;}
	return true;
}

/**
@title: Проверить валидность строковой даты
@author: Eugeny Leonov <eleon@leolab.info>
@version: 0.1 <10/12/2004>
@param: string
@return: bool

#rev.1 <19/09/2008> Eugeny Leonov
*/
function is_sDate($dt){
	$dta=explode('.',$dt);
	if(count($dta)!=3){return false;}
	if(strlen($dta[0])!=2){return false;} //Day
	if(strlen($dta[1])!=2){return false;} //Month
	if((strlen($dta[2])!=2)&&(strlen($dta[2])!=4)){return false;} //Year
	if(strlen($dta[2]==2)){$dta[2] = '20'.$dta[2];}
	return checkdate($dta[1],$dta[0],$dta[2]);
}

/**
@title: Проверить валидность времени
@author: Eugeny Leonov <eleon@leolab.info>
@version: 0.1 <10/12/2004>
@param: int
@param: int
@param: int
@return: bool

#rev.1 <19/09/2008> Eugeny Leonov
*/
function checktime($h,$m,$s=0){
	if(!is_numeric($h)){return false;}
	if($h>23){return false;}
	if(!is_numeric($m)){return false;}
	if($m>59){return false;}
	if(!is_numeric($s)){return false;}
	if($s>59){return false;}
	return true;
}

//<< Функции проверки валидности даты/времени ==

//== Функции преобразования форматов даты/времени >>
/**
@title: Преобразовать строковую нотацию даты в MySQL
@author: Eugeny Leonov <eleon@leolab.info>
@version: 0.1 <10/12/2004>
@param: string
@return: string

#rev.1 <19/09/2008> Eugeny Leonov
*/
function sDate2sqlDate($dt){
	if(!is_sDate($dt)){return false;}
	$dta=explode('.',$dt);
	if($dta[2]==2){
		return '20'.$dta[2].'-'.$dta[1].'-'.$dta[0];
	}else{
		return $dta[2].'-'.$dta[1].'-'.$dta[0];
	}
}

/**
@title: Преобразовать дату из MySQL в строковую нотацию
@author: Eugeny Leonov <eleon@leolab.info>
@version: 0.2 <02/10/2008>
@param: string
@param: bool
@return: string

#rev.1 <13/12/2010> Eugeny Leonov
	[*] Добавлена "обрезка" параметра $dt до формата даты.
*/
function sqlDate2sDate($dt,$sh=false){
	if(strpos($dt,' ')){$dt=substr($dt,0,strpos($dt,' '));}
	if(!is_sqlDate($dt)){return false;}
	$dta=explode('-',$dt);
	if($sh){$dta[0]=substr($dta[0],2,2);}
	return $dta[2].'.'.$dta[1].'.'.$dta[0];
}

/**
@title: Из SQL в UNIX
@author: Eugeny Leonov <eleon@leolab.info>
@version: 0.2 <17/07/2008>
@param: string
@return: int

#rev.1 <19/09/2008> Eugeny Leonov
*/
function sql2nix($dt){
	if(strpos($dt,' ')){
		list($d,$t) = explode(' ',$dt);
	}else{$d=$dt;$t=false;}
	if(!is_sqlDate($d)){return false;}
	list($dY,$dM,$dD) = explode('-',trim($d));
	if($t){
		list($tH,$tM,$tS) = explode(':',trim($t));
		return mktime($tH,$tM,$tS,$dM,$dD,$dY);
	} else {
		return mktime(0,0,0,$dM,$dD,$dY);
	}
}

/**
@title: Из UNIX в SQL
@author: Eugeny Leonov <eleon@leolab.info>
@version: 0.1 <10/12/2004>
@param: int
@return: string

#rev.1 <19/09/2008> Eugeny Leonov
*/
function nix2sql($dt){
	return date('Y-m-d H:i:s',$dt);
}

/**
@title: Из SQL в полный формат
	[пятница, 10 декабря 2004]
@author: Eugeny Leonov <eleon@leolab.info>
@version: 0.2 <21/04/2004>
@param: string
@param: string
@param: string
@return: string

#rev.1 <19/09/2008> Eugeny Leonov
*/
function sqldate2str_full($sd='',$f='full',$p='') {
global $mon_names,$week_names;
	if($p!=''){$p = '_'.$p;}
	if ($sd == '') {$sd = date('Y-m-d');}
	list($y,$m,$d) = explode("-",$sd);
	$dt = getdate(mktime(0,0,0,$m,$d,$y));
	$ret = $week_names[$f.$p][$dt['wday']].', '.$d.' '.$mon_names['fully'][$m].' '.$y;
	return $ret;
}

/**
@title: Из SQL в короткий формат
	[10/12/2004]
@author: Eugeny Leonov <eleon@leolab.info>
@version: 0.1 <10/12/2004>
@param: string
@return: string

#rev.2 <14/12/2010> by Eugeny Leonov <eleonov@leolab.info>
	[+] "Обрезка" времени

#rev.1 <19/09/2008> Eugeny Leonov
*/
function sqldate2short($sd) {
	if(strpos($sd,' ')){$sd=substr($sd,0,strpos($sd,' '));}
	list($y,$m,$d) = explode("-",$sd);
	$ret = $d."/".$m."/".$y;
	return $ret;
}

/**
@title: Из SQL в полный формат
	[10 декабря 2004]
@author: Eugeny Leonov <eleon@leolb.info>
@version: 0.1 <10/12/2004>
@param: string
@return: string

#rev.1 <19/09/2008> Eugeny Leonov
*/
function sqldate2str($sd) {
global $mon_names;
	list($y,$m,$d) = explode("-",$sd);
	$ret = $d." ".$mon_names['fully'][$m]." ".$y;
	return $ret;
}

/**
@title: Из SQL в полный формат месяц + год
	[Декабрь 2004]
@author: Eugeny Leonov <eleon@leolab.info>
@version: 0.1 <10/12/2004>
@param: string
@return: string

#rev.1 <19/09/2008> Eugeny Leonov
*/
function sqldate2mon_yr($sd) {
global $mon_names;
	list($y,$m,$d) = explode("-",$sd);
	$ret = $mon_names['full'][$m]." ".$y;
	return $ret;
}

/**
@title: Из SQL в короткий формат
	[10 дек. 2004]
@author: Eugeny Leonov <eleon@leolab.info>
@version: 0.1 <10/12/2004>
@param: string
@return: string

#rev.2 <14/12/2010> Eugeny Leonov <eleono@leolab.info>
	[+] Отсечение времени

#rev.1 <19/09/2008> Eugeny Leonov <eleon@leolab.info>
*/
function sqldate2sh_str($sd) {
global $mon_names;
	if(strpos($sd,' ')){$sd=substr($sd,0,strpos($sd,' '));}
	if(!is_sqlDate($sd)){return false;}
	list($y,$m,$d) = explode("-",$sd);
	$ret = $d." ".$mon_names['short'][$m]." ".$y;
	return $ret;
}

/**
@title: Из SQL в короткий формат со временем.
	[4 дек. 2005 22:44]
@author: Eugeny Leonov <eleon@leolab.info>
@version: 0.1 <04/12/2005>
@param: string
@return: string

#rev.1 <19/09/2008> Eugeny Leonov <eleon@leolab.info>
*/
function sqldt2sh_str($sdt) {
	if(strpos($sdt,' ')){
		list($sd,$st) = explode(' ',$sdt);
	}else{$sd=$sdt;}
	$dt = sqldate2sh_str($sd);
	if(!$dt){return false;}
	if(isset($st)){
		list($h,$m,$s) = explode(':',$st);
		$ret = $dt.' '.$h.':'.$m;
	}else{$ret=$dt;}
	return $ret;
}

//<< Функции преобразования форматов даты/времени ==

//== Массивы данных >>

$mon_names = array(
	'short'=>array(
		'01'=>'янв.',
		'02'=>'фев.',
		'03'=>'мар.',
		'04'=>'апр.',
		'05'=>'май.',
		'06'=>'июн.',
		'07'=>'июл.',
		'08'=>'авг.',
		'09'=>'сен.',
		'10'=>'окт.',
		'11'=>'ноя.',
		'12'=>'дек.'
	),
	'full'=>array(
		'01'=>'Январь',
		'02'=>'Февраль',
		'03'=>'Март',
		'04'=>'Апрель',
		'05'=>'Май',
		'06'=>'Июнь',
		'07'=>'Июль',
		'08'=>'Август',
		'09'=>'Сентябрь',
		'10'=>'Октябрь',
		'11'=>'Ноябрь',
		'12'=>'Декабрь'
	),
	'fully'=>array(
		'01'=>'января',
		'02'=>'февраля',
		'03'=>'марта',
		'04'=>'апреля',
		'05'=>'мая',
		'06'=>'июня',
		'07'=>'июля',
		'08'=>'августа',
		'09'=>'сентября',
		'10'=>'октября',
		'11'=>'ноября',
		'12'=>'декабря'
	)
);

$week_names = array(
	'short'=>array(
		'0'=>'вс.',
		'1'=>'пн.',
		'2'=>'вт.',
		'3'=>'ср.',
		'4'=>'чт.',
		'5'=>'пт.',
		'6'=>'сб.'
	),
	'full'=>array(
		'0'=>'воскресенье',
		'1'=>'понедельник',
		'2'=>'вторник',
		'3'=>'среда',
		'4'=>'четверг',
		'5'=>'пятница',
		'6'=>'суббота'
	),
	'full_rod'=>array(
		'0'=>'воскресения',
		'1'=>'понедельника',
		'2'=>'вторника',
		'3'=>'среды',
		'4'=>'четверга',
		'5'=>'пятницы',
		'6'=>'субботы'
	),
)
?>
