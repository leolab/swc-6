<?php
/**
@title: Файл функций ядра системы
@package: SWC-6
@subpackage: core
@author: Eugeny Leonov <eleonov@leolab.info>
@version: 6.0.a.4 <17/08/2015>

#rev.6 <17/08/2015> Eugeny Leonov
	[f:->reqGet]

#rev.5 <12/01/2011> Eugeny Leonov
	[*] Функции работы с модулями вынесены во внешний файл: ./inc.mdl.php

#rev.4 <09/01/2011> Eugeny Leonov
	[+:->str2arr]
*/
if(!defined('htaccess')){die('SWC: Direct access disabled.');}
if(!defined('swc_base')){$bp=str_replace('\\','/',__FILE__);define('swc_base',rtrim(substr($bp,0,strrpos($bp,'/'))),'/');unset($bp);}
if(!defined('base_path')){define('base_path',rtrim(str_replace('\\','/',$_SERVER['DOCUMENT_ROOT']),'/'));}
if(!defined('base_url')){define('base_url','');} // (?) Корневой URL
if(!defined('data_path')){define('data_path',base_path.'/_data');}
if(!defined('sites_path')){define('sites_path',base_path.'/_sites');}
if(!defined('themes_path')){define('themes_path',base_path.'/_themes');}
if(!defined('users_path')){define('users_path',base_path.'/_users');}
if(!defined('mdl_path')){define('mdl_path',base_path.'/_mdl');}
if(!defined('cfg_noPack')){define('cfg_noPack',true);}

//== Функции отлова ошибок >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
set_error_handler('swc_error_handler',E_ALL);
/**
@title: Обработчик ошибок
@version: 1.0.rc <02/11/2010>


#rev.1 <17/08/2015> Eugeny Leonov
	[+] Добавлено принудительное выставление результата в ошибку.
*/
function swc_error_handler($errno,$errstr,$errfile,$errline,$errcontext){
	if(defined('dbgEnabled')&&dbgEnabled){setResult(false,'Exception!!!');_die($errno.': '.$errstr.' ('.$errfile.':'.$errline.')');}
	return(true);
}
//<< Функции отлова ошибок ====================================================

//== Вспомогательные функции обработки данных >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/**
@title: Заменить значения первого массива вторым при их наличии
@version: 1.0.a <30/01/2011>
@param: array
@param: array
*/
function swc_arrayFill($a1,$a2){
	if(!is_array($a1)){return(false);}
	if(!is_array($a2)){return($a1);}
	foreach($a1 as $k=>$v){if(isset($a2[$k])){$a1[$k]=$a2[$k];}}
	return($a1);
}

/**
@title: Преобразовать массив в строку
@version: 1.0.r <14/03/2010>
@param: array
@param: [string|false]
@return: string

#rev.1 <18/04/2011> Eugeny Leonov
	[*] Revision
*/
function arr2str($ar,$tl=false){
	if(!is_array($ar)){return('');}
	$ret='';$p='';$s='';
	if($tl){$p=$tl.'[';$s=']';}
	foreach($ar as $k=>$v){if(is_array($v)){$ret.='&'.arr2str($v,$p.$k.$s);}else{$ret.='&'.$p.$k.$s.'='.$v;}}
	return(trim($ret,'&'));
}

/**
@title: Разбор строки в массив
	Аналог: http://ru2.php.net/manual/en/function.parse-str.php
	Не производит замен символов.
@version: 1.0.a <20/01/2011>
@author: Eugeny Leonov <eleonov@leolab.info>
@param: &array - возвращаемый результат
@param: string
@param: mixed

#rev.1 <29/01/2011> Eugeny Leonov
	[f] Deprecated: Call-time pass-by-reference has been deprecated
*
*/
function implode_saPath(&$ret,$key,$value){
	if(strpos($key,'[')){
		$k1=trim(substr($key,0,strpos($key,'[')),'[]');
		$k2=trim(substr($key,strpos($key,'[')),'[]');
		if(!isset($ret[$k1])){$ret[$k1]=array();}
		implode_saPath($ret[$k1],$k2,$value);
	}else{$ret[trim($key,'[]')]=$value;}
	return;
}

/**
@title: Преобразовать строку в массив
@name: str2arr
@version: 1.0.a <09/01/2011>
@param: string
@return: array|false

@todo: Реализовать собственный алгоритм преобразования строк вида: k[k1][k2]=v1 в массив.

#rev.3 <18/04/2011> Eugeny Leonov
	[f] Отсечение параметра после второго символа '='

#rev.2 <29/01/2011> Eugeny Leonov
	[f] Deprecated: Call-time pass-by-reference has been deprecated

#rev.1 <20/01/2011> Eugeny Leonov
	[+] Реализована функция implode_saPath(), сейчас строки разбираются в массив без потерь символов.
*/
function str2arr($str){
	if(!is_string($str)||(trim($str)=='')){return(setResult(false,'Неверный формат параметра.'));}
	$ps=explode("&",$str);
	$ret=array();
	foreach($ps as $p){
		list($k,$v)=array_map("urldecode",explode("=",$p,2));
		if(strpos($k,'[')){
			implode_saPath($ret,$k,$v);
		}else{$ret[$k]=$v;}
	}
	setResult(true);
	return($ret);
}

/**
@title: Значение - эквивалент true?
@version: 1.0.r <28/03/2010>
@param: mixed
@return: bool
*/
function is_true($val){return(in_array(strtolower($val),array(1,'yes','on','true','y','t')));}

/**
@title: Значение - эквивалент false?
@version: 1.0.r <28/03/2010>
@param: mixed
@return: bool
*/
function is_false($val){return(in_array(strtolower($val),array(0,false,'no','off','false','n','f')));}

/**
@title: Есть ли пересечения ключей в друх массивах
@version: 1.0.rc <10/03/2010>
@param: array
@param: array
@return: bool
*/
function akey_inArray($a1,$a2){
	if(!is_array($a1)){return(false);}
	if(!is_array($a2)){return(false);}
	foreach($a1 as $k=>$v){if(isset($a2[$k])){return(true);}}
	return(false);
}

/**
@title: Есть ли пересечение значений в двух массивах
@version: 1.0.rc <10/03/2010>
@param: array
@param: array
@return: bool
*/
function aval_inArray($a1,$a2){
	if(!is_array($a1)){return(false);}
	if(!is_array($a2)){return(false);}
	foreach($a1 as $k=>$v){if(in_array($v,$a2)){return(true);}}
	return(false);
}

//<< Вспомогательные функции обработки данных =================================
//== Вспомогательные функции >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/**
@title: Разобрать строку версии
@version: 1.0.r <29/09/2008>
@param: string
@param: [string|false]
@return: string|array|false
*/
function swc_version($ver,$field=false){
	$vrec=array('module'=>0,'version'=>1,'date'=>2,'author'=>3,'ext'=>4);
	if(!strpos($ver,'#')){return(false);}
	$aver=explode('#',$ver);
	if(!$field){$ret=array();foreach($vrec as $k=>$v){if(isset($aver[$v])){$ret[$k]=$aver[$v];}}}
	elseif(!array_key_exists($field,$vrec)){$ret=false;}else{if(isset($aver[$vrec[$field]])){$ret=$aver[$vrec[$field]];}else{$ret=false;}}
	return($ret);
}

/**
@title: Собрать строку версии
@version: 1.0.rc <25/04/2010>
@param: array|string
@return: string|false
*/
function swc_version2str($ver){
	$ret='';
	if(!is_array($ver)){return(setResult(false,'Неверный формат данных.'));}
	if(!isset($ver['module'])){return(setResult(false,'Не указано наименование модуля.'));}else{$ret.=$ver['module'];}
	if(!isset($ver['version'])){return(setResult(false,'Не указана версия модуля.'));}else{$ret.='#'.$ver['version'];}
	if(!isset($ver['date'])){return(setResult(false,'Не указана дата модуля.'));}
	if(!is_sqlDate($ver['date'])){if(!is_sDate($ver['date'])){return(setResult(false,'Неверный формат даты модуля.'));}else{$ver['date']=sDate2sqlDate($ver['date']);}}
	$ret.='#'.$ver['date'].'#';
	if(isset($ver['author'])){$ret.=$ver['author'].'#';}
	if(isset($ver['ext'])){$ret.=$ver['ext'];}
	return($ret);
}

/**
@title: Получить mime-тип файла (для Content-type)
@copy: Функция основана на дискуссиях http://php.net/
@version: 1.0.rc <30/10/2010>
@param: string
@return: string
*/
function mime_type($fName){
	setResult(true);
	preg_match("|\.([a-z0-9]{2,4})$|i", $fName,$m);    # Get File extension for a better match
	switch(strtolower($m[1])){
		case "js":return("application/x-javascript");
		case "json":return("application/json");
		case "jpg":case "jpeg":case "jpe":return("image/jpg");
		case "png":case "gif":case "bmp":case "tiff":return("image/".strtolower($m[1]));
		case "css":return("text/css");
		case "xml":return("application/xml");
		case "doc":case "docx":return("application/msword");
		case "xls":case "xlt":case "xlm":case "xld":case "xla":case "xlc":case "xlw":case "xll":return("application/vnd.ms-excel");
		case "ppt":case "pps":return("application/vnd.ms-powerpoint");
		case "rtf":return("application/rtf");
		case "pdf":return("application/pdf");
		case "html":case "htm":case "php":return("text/html");
		case "txt":return("text/plain");
		case "mpeg":case "mpg":case "mpe":return("video/mpeg");
		case "mp3":return("audio/mpeg3");
		case "wav":return("audio/wav");
		case "aiff":case "aif":return("audio/aiff");
		case "avi":return("video/msvideo");
		case "wmv":return("video/x-ms-wmv");
		case "mov":return("video/quicktime");
		case "zip":return("application/zip");
		case "tar":return("application/x-tar");
		case "swf":return("application/x-shockwave-flash");
		default:
			if(function_exists("mime_content_type")){ # if mime_content_type exists use it.
				$m=mime_content_type($fName);
			}elseif(function_exists("")){    # if Pecl installed use it
				$finfo=finfo_open(FILEINFO_MIME);
				$m=finfo_file($finfo,$fName);
				finfo_close($finfo);
			}else{    # if nothing left try shell
				if(strstr($_SERVER[HTTP_USER_AGENT], "Windows")){ # Nothing to do on windows
					return ""; # Blank mime display most files correctly especially images.
				}
				if(strstr($_SERVER[HTTP_USER_AGENT], "Macintosh")){ # Correct output on macs
					$m=trim(exec('file -b --mime '.escapeshellarg($fName)));
				}else{    # Regular unix systems
					$m=trim(exec('file -bi '.escapeshellarg($fName)));
				}
			}
			$m=explode(";",$m);
			return(trim($m[0]));
		break;
	}
}

/**
@title: Получить строку отладочной информации
@version: 1.1.b <09/10/2010>
@param: int
@param: [string]
@return: string

#rev.2 <02/11/2010> Eugeny Leonov
	[f] [?] Почему при выполнении функции str_replace возникает варнинг: Array to string conversion?

#rev.1 <10/10/2010> Eugeny Leonov
	[*] Требуется доработка макросов подстановки
*/
function _dbg($lvl,$frmt='%file%:%line%'){
	if(!is_string($frmt)){return('Неверная строка формата.');}
	$dbg=debug_backtrace();
	if(!isset($dbg[$lvl])){return('Указанный уровень вложенности ('.$lvl.') не существует.');}
	foreach($dbg[$lvl]as$k=>$v){
		if(is_array($v)){$v=arr2str($v);}
		if(is_object($v)){$v='{Object}';}
		$rpl['%'.$k.'%']=$v;
	}
	return(@str_replace(array_keys($rpl),array_values($rpl),$frmt));
}

/**
@title: Принудительное завершение выполнения
@version: 1.0.rс <23/02/2010>
@param: [string]
@return: Unavaliable
@todo: Реализовать поддержку шаблонов вывода
@bug: Неверная информация в выводе.
	Выводится информиция последнего установленного результата.
	Должна выводиться информация о функции/методе из которого вызвана функция.
@todo: [?] Реализовать поддержку языков
*/
function _die($msg=''){
	$r=getResult();$html=false;
	while(ob_get_level()>0){ob_end_clean();}
	//== Поиск шаблона >>
	if(file_exists(fname('@T/%theme%/_die.phtml'))){include(fname('@T/%theme%/_die.phtml'));die();}
	elseif(file_exists(fname('@S/%site%/_tpl/_die.phtml'))){include(fname('@S/%site%/_tpl/_die.phtml'));die();}
	elseif(file_exists(swc_base.'/_tpl/_die.phtml')){include(swc_base.'/_tpl/_die.phtml');die();}
	//<< Поиск шаблона ==
	if(!headers_sent()){header('Content-type: text/plain; charset=utf-8');}else{$html=true;}
	if($r['result']){echo('@Ok');}else{echo('@Error');}
	if(!empty($msg)){echo(': '.$msg);}else{echo('.');};
	echo("\n");
	if($html){echo('<br/>');}
	if($r['reason']!=''){echo("@Reason: ".$r['reason']."\n");if($html){echo('<br/>');}}
	if(defined('dbgEnabled')&&dbgEnabled){
		echo("@From: ".$r['from']['file']."\n");if($html){echo('<br/>');}
	}else{
		echo('@From: ');
		if($r['from']['class']!='%class%'){echo($r['from']['class'].':');}
		echo($r['from']['function']."\n");if($html){echo('<br/>');}
	}
	if(isset($GLOBALS['swc.msg'])&&is_array($GLOBALS['swc.msg'])){
		echo("\n".'@Messages>:'."\n");if($html){echo('<br/>');}
		foreach($GLOBALS['swc.msg']as$mk=>$mv){
			echo('['.$mk.']'."\n");if($html){echo('<br/>');}
			foreach($mv as $k=>$v){echo('['.$v['type'].'] '.$v['msg']);if(defined('dbgEnabled')&&dbgEnabled&&isset($v['dbg'])){if($html){echo('<br/><span style="margin-left:25px;">');}echo("\n\t".$v['dbg']);}echo("\n");if($html){echo('</span><br/>');}}
		}
		echo('@Messages<:'."\n");if($html){echo('<br/>');}
	}
	if(defined('dbgEnabled')&&dbgEnabled){
		if($html){echo('<br/>');}
		echo("\n@Dbg:"._dbg(1,'Terminated in %file%:%line%'));
		if($html){echo('<br/>');}
		echo("\n@BackTrace:\n");
		if($html){echo('<br/><pre>');}
		print_r(debug_backtrace());
		if($html){echo('</pre><br/>');}
		echo("\n".'@$GLOBALS:'."\n");
		if($html){echo('<br/><pre>');}
		echo(print_r($GLOBALS,true));
		echo("\n");if($html){echo('<br/>');}
	}
	if($html){echo('<br/>');}
	echo("\n".'Иногда помогает сброс текущей сессии: '.base_url.'/?act=swc.resetSession');
	die();
}

/**
@title: Добавить сообщение в стек
@version: 1.0.rc <26/10/2010>
@param: string
@param: char (type)
@param: string (module)

#rev.1 <09/11/2010> Eugeny Leonov
	[+] в стек сообщений добавлено значение запроса (reqGet)

@todo: Реализовать параметр в конфигурации ядра определяющий список игнорируемых (или неигнорируемых) типов сообщений.
*/
function _msg($msg,$type='N',$mdl='*'){
	if(strtolower($type[0])=='d'){if(!defined('dbgEnabled')||!dbgEnabled){return;}}
	if(!defined('swc_msgHash')){
		define('swc_msgHash',md5(time().serialize(reqGet())));
		$_SESSION['swc.msg'][swc_msgHash]['swc.msg.req']=reqGet();
		if(defined('dbgEnabled')&&dbgEnabled){
			$_SESSION['swc.msg'][swc_msgHash]['SWC.core'][]=array(
				'msg'=>'New messages log thread',
				'type'=>'D',
				'from'=>_dbg(0,'%file%:%line%'),
				'req'=>reqGet(),
			);
		}
	}
	$_SESSION['swc.msg'][swc_msgHash][$mdl][]=array(
		'msg'=>$msg,
		'type'=>$type,
		'from'=>_dbg(1,'%file%:%line%'),
		'req'=>reqGet(),
	);
}

/**
@title: Вывести блок c ошибкой
@version: 1.0.a <27/01/2011>
@param: [array]=false
@param: [string]=false
@param: [bool]=false
@return: false

#rev.1 <21/02/2011>
	[*] Параметр $data может быть строкой-сообщением.
*/
function _e500($data=false,$from=false,$die=false){
	if(!is_array($data)){$data=array('content'=>$data);}
	$data['result']=getResult();
	if(is_string($from)){$data['from']=$from;}
	echo(tpl_parse('e500',$data));
	if($die){die();}
	return(false);
}

/**
@title: Вывести блок "Доступ запрещен"
@version: 1.0.a <29/01/2011>
@param: [array]=false
@param: [string]=false
@return: false
*/
function _e403($data=false,$from=false){
	if(!is_array($data)){$data=array();}
	$data['result']=getResult();
	if(is_string($from)){$data['from']=$from;}
	echo(tpl_parse('e403',$data));
	return(false);
}

//<< Вспомогательные функции ===================================================
//== Функции работы с результатом выполнения >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/**
@title: Установить результат выполнения
@author: Eugeny Leonov <eleonov@leolab.info>
@version: 1.1.rc <10/10/2010>
@param: boolean
@param: [string|array|false]
@param: [string]

#rev.2 <13/10/2010> Eugeny Leonov
	[+] Отрицательный результат логгируется в глобальной переменной

#rev.1 <10/10/2010> Eugeny Leonov
	[rc] Release Candidate version

#ver: 1.1.a <09/10/2010>
	[i] Initial version.
*/
function setResult($result,$reason=false){
	$result=is_true($result);
	$_SESSION['swc.result']=array(
		'result'=>$result,
		'reason'=>$reason,
		'from'=>array(
			'file'=>_dbg(1,'%file%:%line%'),
			'line'=>_dbg(1,'%line%'),
			'function'=>_dbg(2,'%function%'),
			'class'=>_dbg(2,'%class%'),
		),
	);
	if(defined('dbgEnabled')&&dbgEnabled){if(!$result){
		_msg('Reason: '.$reason,'N','SWC.core-setResult');
		$GLOBALS['swc.result.log'][]=$_SESSION['swc.result'];}
	}
	return($result);
}

/**
@title: Получить результат выполнения последней функции
@author: Eugeny Leonov <eleonov@leolab.info>
@version: 1.0.r <15/08/2010>
@param: [string]
@return: mixed
*/
function getResult($field=false){
	if(!isset($_SESSION['swc.result'])){$_SESSION['swc.result']=false;}
	if(!$field){return($_SESSION['swc.result']);}
	if(isset($_SESSION['swc.result'][$field])){return($_SESSION['swc.result'][$field]);}
	return(NULL);
}
//<< Функции работы с результатом выполнения ===================================
//== Функции разбора запросов >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

/**
@title: Получить и разобрать запрос
@name: reqGet
@author: Eugeny Leonov <eleon@leolab.info>
@version: 1.0.r <12/03/2008>
@return: array(url,uri,href,ref,path,file,ext[,blk])

#rev.4 <17/08/2015> Eugeny Leonov
	[f] Исправлена ошибка: Неверное определение наличия блока в  запросе при работе без .htaccess

#rev.3 <20/01/2011> Eugeny Leonov
	[*] контрольная ревизия.

#rev.2 <18/01/2011> Eugeny Leonov
	[+] Разбор нового формата указания метода интерфейса.
		Метод интерфейса указывается после расширения (имени файла или папки) через двоеточие до конца строки или символа '?' ('&', если запрошиваемая страница указывается параметром ?url и не используется .htaccess)

#rev.1 <13/11/2010> Eugeny Leonov
	[f] Исправлена ошибка неверного отделения модуля/метода от адреса страницы при работе без .htaccess
*/
function reqGet($uri=false){
	if(!$uri||!is_string($uri)){
		if(htaccess){
			if(strpos($_SERVER['REQUEST_URI'],'?')){
				$req['url']=substr($_SERVER['REQUEST_URI'],0,strpos($_SERVER['REQUEST_URI'],'?'));
			}else{
				$req['url']=$_SERVER['REQUEST_URI'];
			}
		}else{
			if(isset($_REQUEST['url'])&&(trim($_REQUEST['url'])!='')){$req['url']=$_REQUEST['url'];}else{$req['url']='/';}
		}
	}else{
		if(strpos($uri,'?')){
			$req['url']=substr($uri,0,strpos($uri,'?'));
		}else{
			$ret['url']=$uri;
		}
	}
	unset($uri);
	if(strpos($req['url'],':')!==false){ //В запросе указан блок
		$req['blk']=trim(substr($req['url'],strpos($req['url'],':')),':');
		$req['url']=trim(substr($req['url'],0,strpos($req['url'],':')),':');
	}
	$req['path']=trim(substr($req['url'],0,strrpos($req['url'],"/")),'\\/');
	$req['file']=trim(substr($req['url'],strrpos($req['url'],'/')),'/');
	if($req['file']==''){$req['file']='index';}
	if(strpos($req['file'],'.')){
		$req['ext']=trim(substr($req['file'],strrpos($req['file'],'.')),'.');
		$req['file']=substr($req['file'],0,strrpos($req['file'],'.'));
	}else{$req['ext']='';}
	if(htaccess){$req['path']=urldecode(addslashes(substr($req['path'],strlen(base_url))));
	}else{$req['path']=urldecode(addslashes($req['path']));}
	$req['file']=urldecode(addslashes($req['file']));
	$req['ext']=urldecode(addslashes($req['ext']));

	$req['uri']=trim(trim($req['path'],'/').'/'.trim($req['file'].'.'.$req['ext'],'.'),'/');
	if(isset($req['blk'])){$req['uri'].=':'.$req['blk'];}
	if(!isset($_REQUEST['ref'])){
		if(!isset($_SERVER['HTTP_REFERER'])){$req['ref']=base_url;
		}else{$req['ref']=$_SERVER['HTTP_REFERER'];}
	}else{$req['ref']=$_REQUEST['ref'];}
	if($req['file']=='index'){$req['href'] = $req['path'].'/';
	}else{$req['href']=$req['path'].'/'.$req['file'];}
	return $req;
}

/**
@title: Сформировать ссылку на страницу сайта
@author: Eugeny Leonov <eleon@leolab.info>
@version: 1.1.rc <18/03/2010>
@param: string - адрес страницы
@param: [array|string]=false - параметры передаваемые странице
@param: [array|string]=false - параметры текущей страницы сохраняемые для передачи
@param: [string]=false - "якорь" страницы (предполагается использование для обработки скриптами)
@return: string

@todo: Реализовать ссылки на внешние ресурсы ($href начинается с http://, ftp://, mailto:// и т.п.)
@todo: Реализовать возможность в параметре "сохраняемых" переменных указывать конкретные элементы, если переменная - массивов
*/
function href($href,$gVars=false,$sVars=false,$add=false){
	if(!isset($GLOBALS['swc.req'])){$GLOBALS['swc.req']=reqGet();}
	//= Формируем массив замен >
	$rep=array();
	foreach($GLOBALS['swc.req']as$k=>$v){if(!is_array($v)){$rep['%'.$k.'%']=$v;}}
	//< Формируем массив замен =
	//= Заменяем макросы >
	$href=str_replace(array_keys($rep),array_values($rep),$href);
	//< Заменяем макросы =
	if(!strpos($href,'://')){
		$href=str_replace('//','/',ltrim($href,'/'));
//== Убираем последнюю точку >>
		if($href!=''){
			if($href[strlen($href)-1]=='.'){
				$href=trim(trim($href,'/'),'./');
				if(strpos($href,'/')){$href=substr($href,0,strrpos($href,'/')).'/';}else{$href='';}
			}
		}
//<< Убираем последнюю точку ==
		if(htaccess){$ret=rtrim(base_url,'/').'/'.$href;$pref='?';}else{$ret=rtrim(base_url,'/').'/'.'?url='.$href;$pref='&';}
	}else{//Внешняя ссылка
		// Нужна ли обработка интерфейсом
		if(isset($_SESSION['swc.cfg']['handlers']['href'])&&is_string($_SESSION['swc.cfg']['handlers']['href'])&&(trim($_SESSION['swc.cfg']['handlers']['href'])!='')){
			$gVars['url']=base64encode($href);
			$gVars['act']='redir';
			return(href($_SESSION['swc.cfg']['handlers']['href'],$gVars,$sVars,$add));
		}else{
//@todo: Проверка и разбор параметров переданных ссылкой.
		}
		$ret=trim($href);
	}
	//= Формируем передаваемые параметры >
//@todo: Обработка параметра как строки
	if(is_array($gVars)){$ret.=$pref.arr2str($gVars);$pref='&';}
	//< Формируем передаваемые параметры =
	//= Формируем сохраняемые параметры >
	if(!is_array($sVars)){$sVars=explode(',',$sVars);}
	if(is_array($sVars)){
		foreach($sVars as $k){
			if(!isset($gVars[$k])&&(!in_array($k,array('url','PHPSESSID','PHPR6C1F5')))){
				if(isset($_REQUEST[$k])){
					if(is_array($_REQUEST[$k])){
						$ret.=$pref.arr2str($_REQUEST[$k],$k);
						$pref='&';
					}else{
						$ret.=$pref.$k.'='.$_REQUEST[$k];
						$pref='&';
					}
				}
			}
		}
	}
	//< Формируем сохраняемые параметры =
	if($add){$ret.='#'.$add;}
	return $ret;
}

//<< Функции разбора запросов =================================================
//== Функции работы с файлами и файловой системой >>>>>>>>>>>>>>>>>>>>>>>>>>>>>

/**
@title: Проверить наличие и возможность создания файла
@version: 1.1.r <03/09/2010>
@param: string
@param: [bool]=false
@return: bool
 
#rev.1 <19/02/2011> Eugeny Leonov
	[f] 8: Undefined variable: fn (/store/domains/indoor.open-com.dev/http/_swc/inc.php:611)
*/
function ftest($fName,$create=false){
	$fName=fname($fName);
	if(!$fName){return(false);}
	if(file_exists($fName)){return(setResult(true));}
	if($create){
		if(!dat_save($fName,array(0=>1))){return(setResult(false,'Файл не существует и его нельзя создать.'));}
		return(setResult(unlink($fName)));
	}else{return(setResult(false,'Файл не существует.'));}
}

/**
@title: Преобразовать имя в полное имя файла с обработкой макросов.
@version: 1.1.rc <10/10/2010>
@param: string
@return: string|false
 
@todo: Параметр $fName может содержать имя модуля.
 
#rev.2 <13/02/2011> Eugeny Leonov
 	[+] Символы ':' в имени файла заменяется символами '/'

#rev.1 <10/10/2010> by Eugeny Leonov
	[*] Не возвращался результат, исправлено.

#ver.: 1.0.r <03/09/2010>
*/
function fname($fName){
	setResult(true);
	$fName=rtrim(str_replace(array('\\','..','//',':'),'/',$fName),'/');
	if(!strlen($fName)){return(setResult(false,'Не указано имя файла.'));}
	if($fName[0]=='@'){
		switch(strtoupper($fName[1])){
			case 'D': //data_path
				if(defined('data_path')){$fName=data_path.'/'.rtrim(substr($fName,2),'/');}
				else{$fName=base_path.'/_data/'.rtrim(substr($fName,2),'/');}
			break;
			case 'B': //base_path
				$fName=base_path.'/'.rtrim(substr($fName,2),'/');
			break;
			case 'S': //sites_path
				if(defined('sites_path')){$fName=sites_path.'/'.rtrim(substr($fName,2),'/');}
				else{$fName=base_path.'/_site/'.rtrim(substr($fName,2));}
			break;
			case 'U': //users_path
				if(defined('users_path')){$fName=users_path.'/'.rtrim(substr($fName,2),'/');}
				else{$fName=base_path.'/_user/'.rtrim(substr($fName,2));}
			break;
			case 'T': //themes_path
				if(defined('themes_path')){$fName=themes_path.'/'.rtrim(substr($fName,2),'/');}
				else{$fName=base_path.'/_theme/'.rtrim(substr($fName,2));}
			break;
			case 'M': //mdl_path
				if(defined('mdl_path')){$fName=mdl_path.'/'.rtrim(substr($fName,2),'/');}
				else{$fName=base_path.'/_mdl/'.rtrim(substr($fName,2));}
			break;
			default: //base_path (старая нотация, оставлена для совместимости)
				$fName=base_path.'/'.rtrim($fName,'@/');
			break;
		}
	}
	//== Подготовка макросов замены >>
	$repl=array();
	//%site%
	if(isset($_SESSION['swc.site']['name'])){$repl['%site%']=$_SESSION['swc.site']['name'];}
	elseif(isset($_SESSION['swc.cfg']['default']['site'])){$repl['%site%']=$_SESSION['swc.cfg']['default']['site'];}
	else{$repl['%site%']='';}
	//%theme%
	if(isset($_SESSION['swc.theme']['name'])){$repl['%theme%']=$_SESSION['swc.theme']['name'];}
	elseif(isset($_SESSION['swc.site']['theme'])){$repl['%theme%']=$_SESSION['swc.site']['theme'];}
	elseif(isset($_SESSION['swc.cfg']['default']['theme'])){$repl['%theme%']=$_SESSION['swc.cfg']['default']['theme'];}
	else{$repl['%theme%']='';}
	//%user%
	if(isset($_SESSION['swc.user']['logName'])){$repl['%user%']=$_SESSION['swc.user']['logName'];}
	elseif(isset($_SESSION['swc.cfg']['default']['user'])){$repl['%user%']=$_SESSION['swc.cfg']['default']['user'];}
	else{$repl['%user%']='';}
	//%lang%
	if(isset($_SESSION['swc.cfg']['lang'])){$repl['%lang%']=$_SESSION['swc.cfg']['lang'];}
	elseif(isset($_SESSION['swc.cfg']['default']['lang'])){$repl['%lang%']=$_SESSION['swc.cfg']['default']['lang'];}
	else{$repl['%lang%']='';}
	//<< Подготовка макросов замены ==
	$ret=str_replace('\\','/',str_replace(array_keys($repl),array_values($repl),$fName));
	$ret=str_replace(array('//','..'),array('/','.'),$ret);
	return($ret);
}

/**
@title: Загрузить файл данных
@version: 1.2.r.1 <03/09/2010>
@param: string
@return: array|false{swc_result}
*/
function dat_load($fName){
	setResult(true);
	$fName=fname($fName);
	if(!$fName){return(setResult(false,getResult('reason')));}
	if(!file_exists($fName)){return(setResult(false,'Файл не найден: '.$fName));}
	$fd=file($fName);
	if(isset($fd[1])){return(unserialize(base64_decode($fd[1])));}else{return(unserialize(base64_decode($fd[0])));}
}

/**
@title: Сохранить файл данных
@version: 1.2.r.1 <03/09/2010>
@param: string
@param: array
@return: swc_result
*/
function dat_save($fName,$data){
	$fName=fname($fName);
	if(!$fName){return(setResult(false,getResult('reason')));}
	$path=substr($fName,0,strrpos($fName,'/'));
	if(!mkPath($path)){return(setResult(false,getResult('reason')));}
	if(!@file_put_contents($fName,'<?php die(\'SWC: Direct access disabled.\');'."\n".base64_encode(serialize($data)))){return(setResult(false,'Ошибка сохранения файла: '.$fName));}
	if(defined('swc_file_mod')){if(!chmod($fName,swc_file_mod)){return(setResult(false,'Ошибка изменения прав доступа.'));}}
	return(setResult(true));
}

/**
@title: Сохранить массив в .ini-файл
@version: 1.2.r.1 <03/09/2010>
@param: string
@param: array
@return: boolean
*/
function ini_save($fName,$data){
	$fName=fname($fName);
	if(!$fName){return(setResult(false,getResult('reason')));}
	if(!is_array($data)){return(setResult(false,'Неверный формат данных.'));}
	$path=substr($fName,0,strrpos($fName,'/'));
	if(!mkPath($path)){return(setResult(false,getResult('reason')));}
	//== Преобразовываем массив >>
	$if=array();
	foreach($data as $k=>$v){
		if(is_array($v)){
			foreach($v as $sk=>$sv){
				if(is_array($sv)){
					$if[$k][]=$sk.'="$'.arr2str($sv).'"';
				}else{
					$if[$k][]=$sk.'='.$sv;
				}
			}
		}else{
			$if['*'][]=$k.'='.$v;
		}
	}
	//<< Преобразовываем массив ==
	$fh=@fopen($fName,'w');
	if(!$fh){return(setResult(false,'Ошибка создания файла: '.$fName));}
	fwrite($fh,'#SWC.ini#1.2#'.date('Y-m-d H:i:s')."\n"); //Строка-заголовок
	if(isset($if['*'])){
		fwrite($fh,'[*]'."\n");
		foreach($if['*']as$k=>$v){fwrite($fh,$v."\n");}
	}
	foreach($if as $k=>$v){if($k!='*'){
		fwrite($fh,'['.$k.']'."\n");
		foreach($v as $vv){fwrite($fh,$vv."\n");}
	}}
	fclose($fh);
//[?] - Ошибка установки прав на созданный файл - это критическая ошибка или нет?
	if(defined('swc_file_mod')){if(!chmod($fName,swc_file_mod)){setResult(false,'Ошибка установки прав на файл.');}}
	return(setResult(true));
}

/**
@title: Загрузить массив из .ini-файла
@version: 1.2.r.3 <08/01/2011>
@param: string
@return: array|false

@fixme: В именах параметров сложных значений производится замена символа "точка" на символ "подчеркивание".

#rev.4 <09/01/2011> Eugeny Leonov
	[*] Разбор сложных значений производится функцией str2arr()

#rev.3 <08/01/2011> Eugeny Leonov
	[*] Если в строке отсутствует знак '=', в массив заносится значение равное ключю.

#rev.2 <09/11/2010> Eugeny Leonov
	[*] Исправлена ошибка при обработки пустых строк файла.

#rev.1 <02/11/2010> Eugeny Leonov
	[+] Реализован собственный механизм загрузки и разбора файла.
	[!] В значениях параметров кавычки для сложных значений НЕ СТАВЯТСЯ
*/
function ini_load($fName){
	$fName=fname($fName);
	if(!$fName){return(setResult(false,getResult('reason')));}
	if(!file_exists($fName)){return(setResult(false,'Файл не найден: '.$fName));}
	$f=file($fName);
	if(!is_array($f)){return(setResult(false,'Пустой файл.'));}
	$part='*';$rt=array();
	foreach($f as $k=>$v){
		$v=trim($v);
		if(!empty($v)){if(!in_array($v[0],array('#',';'))){
			if($v[0]=='['){
				$part=trim($v,'[]');
			}else{
				$va=explode('=',$v,2);
				if(!is_array($va)||!isset($va[1])){
					if(is_array($va)){$rt[$part][$va[0]]=$va[0];}else{$rt[$part][$va]=$va;}
				}else{$rt[$part][$va[0]]=$va[1];}
			}
		}}
	}
	if(!$rt||!is_array($rt)){return(setResult(false,'Ошибка загрузки файла: '.$fName));}
	$ret=array();
	if(isset($rt['*'])&&is_array($rt)){
		foreach($rt['*']as$k=>$v){
			if(isset($ret[$k])){_msg('Переопределение ключа: '.$fName.':'.$k,'N');}
			if(!is_array($v)&&(strlen($v)&&($v[0]=='$'))){$v=str2arr(trim($v,'$'));}
			if(!is_array($v)&&(strlen($v)&&($v[0]=='&'))){$ret[$k]=cfg_load(trim($v,'&'));if(!$ret[$k]){return(setResult(false,getResult('reason')));}}
			elseif(!is_array($v)){$ret[$k]=trim($v);}else{$ret[$k]=$v;}
		}
	}
	foreach($rt as $k=>$v){if($k!='*'){
		if(is_array($v)){foreach($v as $vk=>$vv){
			if(isset($ret[$k][$vk])){_msg('Переопределение ключа: '.$fName.':'.$k,'N');}
			if(!is_array($vv)&&(strlen($vv)&&($vv[0]=='$'))){$vv=str2arr(trim($vv,'$'));}
			if(!is_array($vv)&&(strlen($vv)&&($vv[0]=='&'))){$ret[$k][$vk]=cfg_load(trim($vv,'&'));if(!$ret[$k][$vk]){return(setResult(false,getResult('reason')));}}
			elseif(!is_array($vv)){$ret[$k][$vk]=trim($vv);}else{$ret[$k][$vk]=$vv;}
		}}elseif(strlen($v)&&($v[0]=='$')){$vv=str2arr(trim($vv,'$'));}
		elseif(strlen($v)&&($v[0]=='&')){$ret[$k]=cfg_load(trim($vv,'&'));if(!$ret[$k]){return(setResult(false,getResult('reason')));}}
		else{$ret[$k]=trim($v);}
	}}
	setResult(true);
	return($ret);
}

/**
@title: Загрузить файл конфигурации
@version: 1.2.r.2 <17/10/2010>
@param: string
@return: array|false

@todo: Параметр $cName может быть составным и сожержать имя модуля.
 
#rev.3 <26/10/2010> Eugeny Leonov
	[*+] При отсутствии в загруженных данных поля 'name' - оно заполняется именем файла конфига без путей.
	[*] Константа cfg_noPack может быть представлением булевого значения.

#rev.2 <17/10/2010> Eugeny Leonov
	[*] Сброс состояния результата перед возвращением данных.

#rev.1 <10/10/2010> Eugeny Leonov
	[*] Исправлена ошибка загрузки упакованного файла конфигурации, в функцию загрузки передавалось имя файла без расширения.
*/
function cfg_load($fName){
	$fName=fname($fName);
_msg('Запрошен конфиг: '.$fName,'D','SWC.core');
	$ret=false;
	if(file_exists($fName.'.pcfg')){$ret=dat_load($fName.'.pcfg');}
	if(!$ret&&file_exists($fName.'.conf')){
		$ret=ini_load($fName.'.conf');
		if(!$ret){return(setResult(false,getResult('reason')));}
		if(defined('cfg_noPack')&&(is_false(cfg_noPack))){if(!dat_save($fName.'.pcfg',$ret)){_msg('Невозможно сохранить упакованный файл: '.$fName.' ('.getResult('reason').')','W');}}
	}
	if(!$ret){return(setResult(false,'Файл конфигурации ('.$fName.') не найден.'));}
	if(!is_array($ret)){return(setResult(false,'Ошибка загрузки файла конфигурации. ('.$fName.')'));}
	$ret['cfg_name']=$fName;
	setResult(true);
	return($ret);
}

/**
@title: Сохранить файл конфигурации
@version: 1.2.r.1 <03/09/2010>
@param: string
@param: array
@return: bool

@todo: Параметр $cName может быть составным и соржать имя модуля.
*/
function cfg_save($fName,$data){
	$fName=fname($fName);
	if(!$fName){return(setResult(false,getResult('reason')));}
	if(!ini_save($fName.'.conf',$data)){return(setResult(false,getResult('reason')));}
	if(defined('cfg_noPack')&&(cfg_noPack==true)){return(setResult(true));}
	if(!dat_save($fName.'.pcfg',$data)){_msg('Невозможно сохранить упакованный файл: '.$fName.' ('.getResult('reason').')','W');}
	return(dat_save($fName.'.pcfg',$data));
}

/**
@title: Существует ли файл конфигурации
@version: 1.0.r <03/09/2010>
@param: string
@return: bool

@todo: Параметр $cName может быть составным и сожержать имя модуля.
*/
function cfg_exists($cName){
	setResult(true);
	$cName=fname($cName);
_msg('Поиск конфига: '.$cName,'D','SWC.core');
	if(!$cName){return(setResult(false,getResult('reason')));}
	return((file_exists($cName.'.conf')||file_exists($cName.'.pcfg')));
}

/**
@title: Найти файл конфигурации (модуля)
@version: 1.0.rc <06/11/2010>
@param: string
@return: string|false

@todo: Параметр $cName можут быть составным и содержать имя модуля.

#rev.1 <29/03/2011>
	[f] Ошибочное возвращаемое значение.
	[!] Файл конфигурации модуля называется ./$cName$/mdl.{conf|pcfg}
*/
function cfg_find($cName){
	setResult(true);
	if(cfg_exists('@S/%site%/_conf/'.$cName)){return('@S/%site%/_conf/'.$cName);}
	elseif(cfg_exists('@S/%site%/_mdl/'.$cName.'/mdl')){return('@S/%site%/_mdl/'.$cName.'/mdl');}
	elseif(cfg_exists('@S/%site%/_mdl/'.$cName)){return('@S/%site%/_mdl/'.$cName);}
	elseif(cfg_exists('@D/conf/'.$cName)){return('@D/conf/'.$cName);}
	elseif(cfg_exists('@M/'.$cName.'/mdl')){return('@M/'.$cName.'/mdl');}
	elseif(cfg_exists('@M/'.$cName)){return('@M/'.$cName);}
	elseif(cfg_exists(swc_base.'/_conf/'.$cName)){return(swc_base.'/_conf/'.$cName);}
	elseif(cfg_exists(swc_base.'/_mdl/'.$cName.'/mdl')){return(swc_base.'/_mdl/'.$cName.'/mdl');}
	elseif(cfg_exists(swc_base.'/_mdl/'.$cName)){return(swc_base.'/_mdl/'.$cName);}
	else{return(setResult(false,'Файл конфигурации ('.$cName.') не найден.'));}
}

/**
@title: Удалить файл(ы) конфигурации
@version: 1.0.r <03/09/2010>
@param: string
@return: swc_result
*/
function cfg_remove($cName){
	$cName=fname($cName);
	if(!$cName){return(setResult(false,getResult('reason')));}
	if(file_exists($cName.'.conf')){if(!unlink($cName.'.conf')){return(swc_setResult(false,'Ошибка удаления текстового файла конфигурации.'));}}
	if(file_exists($cName.'.pcfg')){if(!unlink($cName.'.pcfg')){return(swc_setResult(false,'Ошибка удаления упакованного файла конфигурации.'));}}
	return(setResult(true));
}

/**
@title: Получить список файлов конфигурации
@version: 1.0.b <05/09/2010>
@param: string
@return: array|false

#rev: 1.0.b.1 <06/01/2011>
	[*] Проверка найденного имени на папку (предотвращение попытки загрузки папки как конфига)
*/
function cfg_list($cPath){
	$cPath=fname($cPath);
	if(!$cPath){return(setResult(false,getResult('reason')));}
	if(!is_dir($cPath.'/')){return(setResult(false,'Ошибка открытия папки: '.$cPath.', папка не существует.'));}
	$dh=@opendir($cPath.'/');
	if(!$dh){return(setResult(false,'Ошибка открытия папки: '.$cPath));}
	$ret=array();setResult(true);
	while(($f=readdir($dh))!=false){
		$f=trim($f,'.');
		if(($f!='')&&strpos($f,'.')&&!is_dir($cPath.'/'.$f)){
			$fn=trim(substr($f,0,strrpos($f,'.')),'.');
			$fe=trim(substr($f,strrpos($f,'.')),'.');
			if(in_array($fe,array('pcfg','conf'))){$ret[$fn]=$fn;}
		}
	}
	setResult(true);
	return($ret);
}

/**
@title: Найти в каком месте находится файл.
	Поиск файла производится в порядке нахождения путей в массиве.
@version: 1.0.rc <30/10/2010>
@param: array
@return: string|false
*/
function where_is($pList){
	if(!is_array($pList)){return(setResult(false,'Неверный вызов функции. Неожиданный параметр.'));}
	setResult(true);
	foreach($pList as $k=>$v){if(file_exists(fname($v))){return($v);}}
	return(setResult(false,'Файл в указанных путях не найден.'));
}

/**
@title: Загрузить произвольный файл
@version: 1.0.rc <15/12/2010>
@param: string
@return: string|false

#rev.1 <09/01/2011> Eugeny Leonov
	[*] Оптимизировано преобразование имени файла.
*/
function fload($fName){
	$fName=fname($fName);
	if(trim($fName)==''){return(setResult(false,'Не указано или неверное имя файла.'));}
	if(!file_exists($fName)){return(setResult(false,'Файл не найден: '.$fName));}
	$ret=file_get_contents($fName);
	setResult(true);
	return($ret);
}

/**
@title: Сохранить произвольный файл
@version: 1.0.rc <15/12/2010>
@param: string
@param: string
@return: bool

#rev.1 <09/01/2011> Eugeny Leonov
	[f] К имени файла в любом случае добавляется путь к папке данных.
	[*]	Имя файла обрабатывается функцией fname(). Соответственно, сейчас в имени файла можно указывать макросы.
*/
function fsave($fName,$content){
	if(!is_string($content)){return(setResult(false,'Неверный формат параметра.'));}
	$fName=fname($fName);
	if(trim($fName)==''){return(setResult(false,'Не указано или неверное имя файла.'));}
	$fPath=substr($fName,0,strrpos($fName,'/'));
	if(!mkPath($fPath)){return(setResult(false,'Ошибка открытия/создания папки: '.getResult('reason')));}
	if(!file_put_contents(fname($fName),$content)){return(setResult(false,'Ошибка сохранения файла: '.$fName));}
	return(setResult(true));
}

//<< Функции работы с файлами и файловой системой ==============================
//== Функции работы с блоками >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/**
@title: Загрузить параметры блока
	Параметры блока ищутся в последовательности:
		- блоки текущего сайта
		- блоки ядра системы
@version: 1.0.rc.1 <09/11/2010>
@param: string
@return: array|false
*/
function blk_load($bName){
	if(!is_string($bName)){return(setResult(false,'Неверный вызов функции.'));}
	if(strpos($bName,':')){
		$blk=array(
			'mdl'=>trim(substr($bName,0,strpos($bName,':')),':'),
			'get'=>trim(substr($bName,strpos($bName,':')),':'),
			'act'=>trim(substr($bName,strpos($bName,':')),':'),
			'blk_name'=>$bName,
		);
		_msg('Параметры блока получены из имени ('.$bName.')','D','SWC.core');
	}else{
		if(cfg_exists('@S/%site%/_blk/'.$bName)){$blk=cfg_load('@S/%site%/_blk/'.$bName);}
		elseif(cfg_exists(swc_base.'/_blk/'.$bName)){$blk=cfg_load(swc_base.'/_blk/'.$bName);}
		else{return(setResult(false,'Параметры блока ['.$bName.'] не найдены.'));}
	}
	$blk['blk.name']=$bName;
	setResult(true);return($blk);
}

/**
@title: Получить данные интерфейса модуля

	Если параметры блока заданы строкой (именем блока), производится поиск файла параметров в следующем порядке:
		- в папке описаний блоков текущего сайта
		- в папке описаний блоков ядра.

	Поиск интерфейса действия производится в следующем порядке:
		- в папке модулей текущего сайта
		- в папке описаний блоков текущего сайта
		- в общей папке модулей
		- в папке модулей ядра
		- в папке описания блоков ядра

@version: 1.0.rc.5 <18/12/2010>
@param: string|array
@return: array|false

#rev.8 <31/01/2011> Eugeny Leonov
	[f] Бага.

#rev.7 <21/01/2011> Eugeny Leonov
	[+] обработка параметра deny.blk указывающего блок обрабатываемый в случае запрета обработки текущего.

#rev.6 <20/01/2011> Eugeny Leonov
	[+] Проверка прав пользователя
*/
function blk_get($blk){
	setResult(true);
	$ret=array();
	if(!is_array($blk)){if(!is_string($blk)){return(setResult(false,'Неверный вызов функции: Неожиданный тип параметра.'));}_msg('Try to load block ('.$blk.')','D','SWC.core');$blk=blk_load($blk);}
	if(!is_array($blk)){if(!getResult('result')){return(setResult(false,'Ошибка: '.getResult('reason')));}else{return(setResult(false,'Неверный вызов функции. Неожиданный тип параметра.'));}}
	$data=array();
	//= Проверка прав доступа >
	$a=true;
	if(isset($blk['grp.allow'])&&is_array($blk['grp.allow'])){if(!user_isMember($blk['grp.allow'])){$a=false;}}
	if(isset($blk['grp.deny'])&&is_array($blk['grp.deny'])){if(user_isMember($blk['grp.deny'])){$a=false;}}
	if(!$a){
		if(isset($blk['deny.blk'])&&is_string($blk['deny.blk'])&&(trim($blk['deny.blk'])!='')){return(blk_get($blk['deny.blk']));}
		else{
			$ret['ret']=true;
			$ret['content']=tpl_parse('e403',$data);
			$ret['result']=getResult();
			return($ret);
//		return(setResult(false,'Блок не разрешен.'));
		}
	}
	//< Проверка прав доступа =
	if(!isset($blk['blk_name'])){
		if(isset($blk['mdl'])&&isset($blk['get'])){$blk['blk_name']=$blk['mdl'].':'.$blk['get'];}
		else{return(setResult(false,'Неверный вызов функции: Ошибка в параметрах.'));}
	}
	if(!isset($blk['mdl'])){return(setResult(false,'Для блока ('.$blk['blk_name'].') не определен модуль.'));}
	if(!isset($blk['get'])){
		if(isset($data['get'])){$blk['get']=$data['get'];}
		elseif(isset($data['pars']['get'])){$blk['get']=$data['pars']['get'];}
		else{$blk['get']='_undefined_';}
	}
	if(!isset($blk['pos'])){if(isset($data['pos'])){$blk['pos']=$data['pos'];}else{$blk['pos']='_undef_';}}
	if(!isset($blk['num'])){if(isset($data['num'])){$blk['num']=$data['num'];}else{$blk['num']=rand(1000,9999);}}
	//= Ищем интерфейс модуля блока >
	$fName=false;
	if(!$fName){$fName=int_find($blk['mdl'],'get');}
	if(!$fName){if(!getResult('result')){return(setResult(false,'Ошибка поиска интерфейса модуля ('.$blk['mdl'].'): '.getResult('reason')));}return(setResult(false,'Неожиданная ошибка поиска интерфейса модуля ('.$blk['mdl'].').'));}
	//< Ищем интерфейс модуля блока =
	//= Проверяем данные для модуля >
	if(!isset($blk['req'])||!is_array($blk['req'])){$blk['req']=reqGet();}
	if(!isset($blk['pars'])||!is_array($blk['pars'])){$blk['pars']=$_REQUEST;}
	//< Проверяем данные для модуля =
	ob_start();
	$ret['ret']=include(fname($fName));
	$ret['content']=ob_get_clean();
	$ret['result']=getResult();
	return($ret);
}
//<< Функции работы с блоками ==================================================
//== Вспомогательные функции работы со страницами >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/**
@title: Установить тип документа
@version: 1.0.rc <20/11/2010>
@param: string
*/
function set_contentType($cType='text/html; charset=utf-8'){$GLOBALS['out']['headers']['Content-type']=$cType;}

/**
@title: Добавить стиль на страницу
@version: 1.0.rc <30/10/2010>
@param: string
@param: string|false
@return: boolean
*/
function css_add($cName,$cData=false){
	setResult(true);
	if(!$cData){//Внешний CSS-файл
		$GLOBALS['out']['head']['css']['ext'][$cName]=$cName;
	}else{//Встраиваемый стиль
		$GLOBALS['out']['head']['css']['int'][$cName]=$cData;
	}
	return(true);
}

/**
@title: Добавить скрипт на страницу
@version: 1.0.rc <30/10/2010>
@param: string
@param: [string|false]
@param: [string]
@return: boolean
 
#rev.1 <10/02/2011> Eugeny Leonov
	[+] Встраивание скриптов в события страницы.
*/
function js_add($jName,$jData=false,$event=false){
	setResult(true);
	if(!$jData){//Внешний JS-файл
//@todo: Определение местонахождения файла скрипта
		$GLOBALS['out']['head']['js']['ext'][$jName]=$jName;
	}elseif(!$event){//Встраиваемый скрипт
		$GLOBALS['out']['head']['js']['int'][$jName]=$jData;
	}else{//Обработчик события страницы (JQuery)
		$GLOBALS['out']['head']['js']['evt'][$event][]=$jData;
	}
	return(true);
}

//<< Вспомогательные функции работы со страницами ==============================
//== Функции работы с ресурсами >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/**
@title: Найти файл ресурса
@version: 1.0.rc <30/10/2010>
@param: string
@param: [string]=false
@return: string|false
 
#rev.2 <18/02/2011> Eugeny Leonov
	[*] исправления параметров.

#rev.1 <07/01/2011> Eugeny Leonov
	[+] Добавлены пути поиска. Поиск ресурсов производится в папках ./_tpl/* сайта, модуля и ядра
	[-] Убран поиск ресурсов по умолчанию (./res.php)
*/
function res_find($res,$mdl=false){
	$ret=false;setResult(true);
	if(!is_string($res)){return(setResult(false,'Неверный тип имени ресурса.'));}
	if(strpos($res,':')){
		$mdl=trim(substr($res,0,strrpos($res,':')),':/');
		$res=trim(substr($res,strrpos($res,':')),':/');
	}
	if(!$mdl){$mdl='_undefined_';}
	if(defined('dbgEnabled')&&dbgEnabled){_msg('mdl:'.$mdl.', res:'.$res,'D','SWC.core');}
	//= Собираем массив путей поиска >
	$sa=array();
	$sa[]='@T/%theme%/_res/'.$res;
	$sa[]='@T/%theme%/_res/'.$res.'.php';
	$sa[]='@S/%site%/_res/'.$res;
	$sa[]='@S/%site%/_res/'.$res.'.php';
	$sa[]='@S/%site%/_mdl/'.$mdl.'/_res/'.$res;
	$sa[]='@S/%site%/_mdl/'.$mdl.'/_res/'.$res.'.php';
	$sa[]='@S/%site%/_mdl/'.$mdl.'/_tpl/'.$res;
	$sa[]='@S/%site%/_mdl/'.$mdl.'/_tpl/'.$res.'.php';
	$sa[]='@S/%site%/_tpl/'.$mdl.'/'.$res.'.res.php';
	$sa[]='@S/%site%/_tpl/'.$res.'.php';
	$sa[]='@M/'.$mdl.'/_res/'.$res;
	$sa[]='@M/'.$mdl.'/_res/'.$res.'.php';
	$sa[]='@M/'.$mdl.'/_tpl/'.$res.'.res.php';
	$sa[]='@M/'.$mdl.'/'.$res.'.res.php';
	$sa[]=swc_base.'/_res/'.$res;
	$sa[]=swc_base.'/_res/'.$res.'.php';
	$sa[]=swc_base.'/_mdl/'.$mdl.'/_res/'.$res;
	$sa[]=swc_base.'/_mdl/'.$mdl.'/_res/'.$res.'.php';
	$sa[]=swc_base.'/_tpl/'.$mdl.'/'.$res.'.res.php';
	$sa[]=swc_base.'/_tpl/'.$res.'.res.php';
	//< Собираем массив путей поиска =
	$ret=where_is($sa);
	return($ret);
}
//<< Функции работы с ресурсами ================================================
//== Функции работы с файлами и файловой системой >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
if(file_exists(swc_base.'/inc.file.php')){require_once(swc_base.'/inc.file.php');}else{_die('Отсутствует файл функций ядра [file]');}
//<< Функции работы с файлами и файловой системой ==============================
//== Функции работы с пользователями (базовые) >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
if(file_exists(swc_base.'/inc.user.php')){require_once(swc_base.'/inc.user.php');}else{_die('Отсутствует файл функций ядра [user]');}
//<< Функции работы с пользователями (базовые) =================================
//== Функции работы с модулями и интерфейсами >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
if(file_exists(swc_base.'/inc.mdl.php')){require_once(swc_base.'/inc.mdl.php');}else{_die('Отсутствует файл функций ядра [mdl]');}
//<< Функции работы с модулями и интерфейсами ==================================
//== Функции работы с темами и шаблонами >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
if(file_exists(swc_base.'/inc.theme.php')){require_once(swc_base.'/inc.theme.php');}else{_die('Отсутствует файл функций ядра [theme]');}
//<< Функции работы с темами и шаблонами =======================================
//== Функции работы с сайтами >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
if(file_exists(swc_base.'/inc.site.php')){require_once(swc_base.'/inc.site.php');}else{_die('Отсутствует файл функций ядра [site]');}
//<< Функции работы с сайтами ==================================================
//== Функции работы со страницами >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
if(file_exists(swc_base.'/inc.page.php')){require_once(swc_base.'/inc.page.php');}else{_die('Отсутствует файл функций ядра [page].');}
//<< Функции работы со страницами ==============================================
//== Функции работы с изображениями >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
if(file_exists(swc_base.'/inc.img.php')){require_once(swc_base.'/inc.img.php');}else{_die('Отсутствует файл функций ядра [img]');}
//<< Функции работы с изображениями ============================================
//== Функции работы с датой >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
if(file_exists(swc_base.'/inc.date.php')){require_once(swc_base.'/inc.date.php');}else{_die('Отсутствует файл функций ядра [date]');}
//<< Функции работы с датой ====================================================
//== Функции обработки текста >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
if(file_exists(swc_base.'/inc.text.php')){require_once(swc_base.'/inc.text.php');}else{_die('Отсутствует файл функций ядра [text]');}
//<< Функции обработки текста ==================================================
?>
