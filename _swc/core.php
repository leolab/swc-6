<?php
/**
@title: Основной обрабатывающий файл системы
@package: SWC-6
@subpackage: core
@author: Eugeny Leonov <eleonov@leolab.info>
@version: 1.0.a.9 <26/02/2011>

#rev.9 <26/02/2011> Eugeny Leonov
	[*] Попытка исправить ошибку неверной установки default_time_zone

#rev.8 <10/02/2011> Eugeny Leonov
	[r] Общая ревизия, чистка устаревшего кода.

#rev.7 <02/02/2011> Eugeny Leonov
	[R] Release: 1.0.a.4

#rev.6 <20/01/2011> Eugeny Leonov
	[f] Откорректировано определение главного блока страницы для обработки текущего формата задания модуля/метода.

#rev.5 <18/01/2011> Eugeny Leonov
	[+] тип запроса интерфейса ?gt (get)
	[+] ресурсом считается файл имеющий одно из расширений указанное в разделе [res.ext] конфигурации ядра.
	[+] ресурсом считается файл находящийся в пути начинающимся с одной из папок указанных в разделе [res.pref] конфигурации ядра. :!: Можно указать только папку верхнего уровня. Все папки ниже ее будут считаться путем поиска файла ресурса.
	[!] если в конфигурации ядра не указаны (отсутствуют) разделы [res.ext] и (или) [res.pref], им присваивается значение пустого массива. Соответственно в этом случае обработка запроса как ресурса не производится.
	[W] требуется актуализация конфигурации ядра системы во всех проектах.

#rev.4 <11/01/2011> Eugeny Leonov
	[+] const devMode - режим разработки
*/

/*
	Включение режима отладки
	При явной установке константы изменения режима не допускается.
*/
//if(!defined('dbgEnabled')){define('dbgEnabled',false);}
/*
	Включение режима разработки
	Константа влияет на некоторые шаблоны ядра. При установке значения в true режим отладки включается в принудительном режиме.
*/
if(!defined('devMode')){define('devMode',true);}

define('swc_version','SWC-6#1.0.a.9#2011-02-26#ELeonov');

if(!defined('htaccess')){die('SWC: Direct access disabled.');}
if(!session_start()){die('SWC: Can not initialize session.');}
if(!ob_start()){die('SWC: Can not start output buffer.');}
if(!defined('swc_base')){$bp=str_replace('\\','/',__FILE__);define('swc_base',rtrim(substr($bp,0,strrpos($bp,'/'))),'/');unset($bp);}
require_once(swc_base.'/inc.php');
$GLOBALS['swc.req']=reqGet();

//== Методы внутреннего интерфейса ядра системы (Неперекрываемые) >>>>>>>>>>>>>>
/**
	Методы не перекрываемые
*/
if(isset($_REQUEST['act'])||isset($_REQUEST['do'])){
	if(!isset($_REQUEST['act'])){$_REQUEST['act']=$_REQUEST['do'];}
	switch($_REQUEST['act']){
/**
@title: Включить или выключить режим отладки
@version: 1.0.r <26/02/2011>
@param: &value={on|off}
*/
		case 'swc.dbgEnabled':
			if(!isset($_SESSION)||!is_array($_SESSION)){
				//Сессии не существует. Для предотвращения ошибки потери сессии производим принудительное завершение.
				setResult(false,'Сессия устарела.');
				_die('Сессия устарела.');
			}
			if(!defined('dbgEnabled')){
				if(isset($_REQUEST['value'])&&(is_true($_REQUEST['value'])||is_false($_REQUEST['value']))){
					$_SESSION['swc.cfg']['dbgEnabled']=$_REQUEST['value'];
					setResult(true);
				}else{setResult(false,'Incorrect value.');}
			}else{setResult(false,'SWC: Not in development mode.');}
			while(ob_get_level()){ob_end_clean();}
			if(!headers_sent()){
				header('Content_type: text/plain; charset=utf-8;');
				if(!isset($_REQUEST['do'])){header('Location: '.$GLOBALS['swc.req']['ref']);}
			}
			if(!getResult('result')){echo('@Error: '.getResult('reason'));}else{echo('@Ok: ');if(is_true($_SESSION['swc.cfg']['dbgEnabled'])){echo('dbgEnabled=on');}else{echo('dbgEnabled=off');}}
			echo("\n".'@Location: '.$GLOBALS['swc.req']['ref']);
			die("\n".'@EOF');
		break;

/**
Сбросить текущую сессию
@version: 1.0.r <26/02/2011>
*/
		case 'swc.resetSession':
			$_SESSION=false;
			unset($_SESSION);
			session_destroy();
			while(ob_get_level()){ob_end_clean();}
			if(!headers_sent()){header('Content-type: text/plain; charset=utf-8;');if(!isset($_REQUEST['do'])){header('Location: /');}}
			echo('@Ok.'."\n".'@Location: /');
			die("\n@EOF");
		break;
		default:
		break;
	}
}elseif(isset($_REQUEST['get'])){
	switch($_REQUEST['get']){
		default:
		break;
	}
}
//<< Методы внутреннего интрефейса ядра системы (Неперекрываемые) ==============

//== Определение конфигурации ядра системы >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/**
Файл конфигурации ядра системы может находиться в следующих местах (указаны в порядке очередности поиска):
	1. в общей папке данных (const data_path)/conf/swc.conf
	2. в папке конфигурации ядра системы (const swc_base)/_conf/swc.conf (дефолтный)

Для использования собственной конфигурации рекомендуется использовать первое местоположение.
Так же, конфигурация ядра системы может быть определена до вызова основного скрипта системы (этого файла) в сессионом параметре swc.conf
Параметры конфигурации см.: https://leolab.info/swc/wiki/docs/swc.conf
*/
if(!isset($_SESSION['swc.cfg'])||!is_array($_SESSION['swc.cfg'])){$_SESSION['swc.cfg']=false;}
if(!$_SESSION['swc.cfg']){
	if(cfg_exists('@D/conf/swc')){$_SESSION['swc.cfg']=cfg_load('@D/conf/swc');}
	elseif(cfg_exists(swc_base.'/_conf/swc')){$_SESSION['swc.cfg']=cfg_load(swc_base.'/_conf/swc');}
	else{_die('Не найдена конфигурация ядра системы.');}
}
if(!$_SESSION['swc.cfg']||!is_array($_SESSION['swc.cfg'])){_die('Отсутствует конфигурация ядра системы.');}
if(isset($_SESSION['swc.cfg']['timezone'])){if(!date_default_timezone_set($_SESSION['swc.cfg']['timezone'])){_die('Ошибка установки временной зоны.');}}
elseif(!date_default_timezone_set(date_default_timezone_get())){_die('Ошибка установки временной зоны по умолчанию.');}
//Временная зона установлена.
//<< Определение конфигурации ядра системы =====================================

//= Не запрошен ли ресурс? >
if(!isset($_SESSION['swc.cfg']['res.ext'])){$_SESSION['swc.cfg']['res.ext']=array();}
if(!isset($_SESSION['swc.cfg']['res.pref'])){$_SESSION['swc.cfg']['res.pref']=array();}
if(in_array($GLOBALS['swc.req']['ext'],$_SESSION['swc.cfg']['res.ext'])){
	$_REQUEST['get']='resource';
	if(in_array(trim(substr($GLOBALS['swc.req']['path'],0,strpos($GLOBALS['swc.req']['path'],'/')),'/'),$_SESSION['swc.cfg']['res.pref'])){
		$_REQUEST['res']=trim(substr($GLOBALS['swc.req']['path'],strpos($GLOBALS['swc.req']['path'],'/')).'/'.$GLOBALS['swc.req']['file'].'.'.$GLOBALS['swc.req']['ext'],'./');
	}elseif(in_array($GLOBALS['swc.req']['path'],$_SESSION['swc.cfg']['res.pref'])){
		$_REQUEST['res']=trim($GLOBALS['swc.req']['file'].'.'.$GLOBALS['swc.req']['ext'],'.');
	}else{$_REQUEST['res']=$GLOBALS['swc.req']['uri'];}
	$GLOBALS['swc.req']['get']='resource';
	$GLOBALS['swc.req']['res']=$_REQUEST['res'];
}elseif(in_array(trim(substr($GLOBALS['swc.req']['path'],0,strpos($GLOBALS['swc.req']['path'],'/')),'/'),$_SESSION['swc.cfg']['res.pref'])){
	$_REQUEST['get']='resource';$GLOBALS['swc.req']['get']='resource';
	$_REQUEST['res']=trim(substr($GLOBALS['swc.req']['path'],strpos($GLOBALS['swc.req']['path'],'/')).'/'.$GLOBALS['swc.req']['file'].'.'.$GLOBALS['swc.req']['ext'],'./');
}elseif(in_array($GLOBALS['swc.req']['path'],$_SESSION['swc.cfg']['res.pref'])){
	$_REQUEST['get']='resource';$GLOBALS['swc.req']['get']='resource';
	$_REQUEST['res']=trim($GLOBALS['swc.req']['file'].'.'.$GLOBALS['swc.req']['ext'],'.');
}
//< Не запрошен ли ресурс? =

//= Обработка флага отладки >
if(!defined('dbgEnabled')&&isset($_SESSION['swc.cfg']['dbgEnabled'])){define('dbgEnabled',is_true($_SESSION['swc.cfg']['dbgEnabled']));}elseif(!defined('dbgEnabled')){define('dbgEnabled',false);}
//< Обработка флага отладки =

if(defined('dbgEnabled')&&dbgEnabled){
	$rd='';
	if(isset($GLOBALS['swc.req']['act'])){$rd.='[act:'.$GLOBALS['swc.req']['act'].']';}
	elseif(isset($GLOBALS['swc.req']['do'])){$rd.='[do:'.$GLOBALS['swc.req']['do'].']';}
	elseif(isset($GLOBALS['swc.req']['get'])){$rd.='[get:'.$GLOBALS['swc.req']['get'].']';}
	elseif(isset($GLOBALS['swc.req']['gt'])){$rd.='[gt:'.$GLOBALS['swc.req']['gt'].']';}
	_msg('Запрос: '.$GLOBALS['swc.req']['path'].'/'.$GLOBALS['swc.req']['file'].' '.$rd,'DN','SWC.core');
	unset($rd);
}
//== Инициализация классов ядра системы >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/**
Классы системы являются ее расширением и их наличие не является обязательным.
@todo: Редактирование загружаемых классов.
@todo: Классы ядра системы замещают и дополняют основные функции системы (работа с пользователями, сайтами, страницами и шаблонами)
@deprecated: Вместо загрузки классов требуется реализовать выполенение интерфейсов
*/
/*
if(isset($_SESSION['swc.cfg']['mdl.preload'])&&is_array($_SESSION['swc.cfg']['mdl.preload'])){
	foreach($_SESSION['swc.cfg']['mdl.preload']as$k=>$v){
		$mName=false;$mData=false;
		if(is_array($v)&&isset($v['mdl'])){
			$mName=$v['mdl'];$mData=$v;
		}elseif(is_string($v)){
			$mName=$v;$mData=false;
		}else{
			$mName=false;_msg('Неверное значение параметра конфигурации: [mdl.preload]:'.$k,'W','SWC.core');
		}
		if($mName){
			if(!mdl_load($mName,$mData)){
				_msg('Ошибка загрузки модуля: '.$mName.' Reason: '.getResult('reason'),'E','SWC.core');
			}
		}
	}
}
*/
//<< Инициализация классов ядра системы ========================================
//== Определение текущего пользователя системы >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/**
Пользователь может быть определен модулем user (или любым другим), загружаемым выше.
Если пользователь не определен, производится определение имени (логина) пользователя по умолчанию с последующим поиском описания пользователя.
:!: Ядро системы не работает с пользователями определенными в БД
Пользователь (логин) по умолчанию может быть определен в конфигурации ядра системы (см. документацию ядра системы) или файлом _default.conf
Поиск пользователя производится сначала в общей папке пользователей (const users_path) затем в папке пользователей ядра системы.
Описание пользователей ядра системы при необходимости может быть переопределено в общей папке пользователей.
*/
if(!isset($_SESSION['swc.user'])||!is_array($_SESSION['swc.user'])){$_SESSION['swc.user']=false;}
//@todo: Здесь может находиться обработчик (вызов) внешней авторизации пользователея
if(!$_SESSION['swc.user']){if(isset($_SESSION['swc.cfg']['user'])&&is_string($_SESSION['swc.cfg']['user'])&&(trim($_SESSION['swc.cfg']['user'])!='')){$_SESSION['swc.user']=user_load($_SESSION['swc.cfg']['user']);}}
if(!$_SESSION['swc.user']){
	if(!isset($_SESSION['swc.cfg']['default']['user'])||!is_string($_SESSION['swc.cfg']['default']['user'])||(trim($_SESSION['swc.cfg']['default']['user'])=='')){$_SESSION['swc.cfg']['default']['user']='_default';}
	$_SESSION['swc.user']=user_load($_SESSION['swc.cfg']['default']['user']);
}
//По идее, если не смогли определить пользователя - должны по любому умереть.
if(!$_SESSION['swc.user']){_die('Ошибка определения пользователя.');}
//Проверка и установка записи текущего пользователя
if(!isset($_SESSION['swc.cfg']['user'])||($_SESSION['swc.cfg']['user']!=$_SESSION['swc.user']['logName'])){
//Пользователь вошел впервые за сессию, или перелогинился (разлогинился).
	$_SESSION['swc.cfg']['user']=$_SESSION['swc.user']['logName'];
}
//<< Определение текущего пользователя системы =================================
//== Определение текущего сайта >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
if(!isset($_SESSION['swc.site'])||!is_array($_SESSION['swc.site'])){
//@todo: Здесь может находится вызов (обработчик) внешнего определения (изменения) текущего сайта.
	if(!site_change()){_die('Ошибка изменения текущего сайта: '.getResult('reason'));}
}
//<< Определение текущего сайта ================================================
//== Определение текущей темы сайта >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
if(!isset($_SESSION['swc.theme'])||!is_array($_SESSION['swc.theme'])){
//@todo: Здесь может находиться вызов (обработчик) внешнего определения (изменения) текущей темы.
	if(!theme_change()){_die('Ошибка изменения текущего сайта: '.getResult('reason'));}
}
//<< Определение текущей темы сайта ============================================

//== Определение параметров запрошенной страницы >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
//Страница может быть определена модулем page (или любым другим), загружаемым выше
if(!isset($GLOBALS['swc.page'])||!is_array($GLOBALS['swc.page'])){$GLOBALS['swc.page']=false;}
//@todo: Здесь может находится вызов (обработчик) внешнего определения запрошенной страницы.
if(!$GLOBALS['swc.page']){$GLOBALS['swc.page']=pg_load($GLOBALS['swc.req']);}
if(!$GLOBALS['swc.page']){if(isset($_SESSION['swc.site']['default']['e404'])){$GLOBALS['swc.page']=pg_load($_SESSION['swc.site']['default']['e404']);}}
if(!$GLOBALS['swc.page']){if(isset($_SESSION['swc.cfg']['default']['e404'])){$GLOBALS['swc.page']=pg_load($_SESSION['swc.cfg']['default']['e404']);}}

if(isset($GLOBALS['swc.page']['denited'])&&is_true($GLOBALS['swc.page']['denited'])){ //Страница отмечена как запрещенная.
	$GLOBALS['swc.page']=false;
	if(!$GLOBALS['swc.page']){if(isset($_SESSION['swc.site']['default']['e403'])){$GLOBALS['swc.page']=pg_load($_SESSION['swc.site']['default']['e403']);}}
	if(!$GLOBALS['swc.page']){if(isset($_SESSION['swc.cfg']['default']['e403'])){$GLOBALS['swc.page']=pg_load($_SESSION['swc.cfg']['default']['e403']);}}
	if(!$GLOBALS['swc.page']){if(cfg_exists(swc_base.'/_pages/e403')){$GLOBALS['swc.page']=cfg_load(swc_base.'/_pages/e403');}}
}

if(!$GLOBALS['swc.page']){
	if(cfg_exists('@S/%site%/e404')){$GLOBALS['swc.page']=cfg_load('@S/%site%/e404');}
	elseif(cfg_exists(swc_base.'/_pages/e404')){$GLOBALS['swc.page']=cfg_load(swc_base.'/_pages/e404');}
	else{$GLOBALS['swc.page']=false;}
}

if(!$GLOBALS['swc.page']){setResult(false,'Запрошенная страница не найдена.');_die('Запрошенная страница не найдена.');}

//= Добавление главного блока на страницу (при необходимости) >
if(isset($GLOBALS['swc.req']['blk'])&&(trim($GLOBALS['swc.req']['blk'])!='')){
	if(strpos($GLOBALS['swc.req']['blk'],':')){
		$GLOBALS['swc.page']['blk']=array(
			'mdl'=>trim(substr($GLOBALS['swc.req']['blk'],0,strpos($GLOBALS['swc.req']['blk'],':')),':'),
			'get'=>trim(substr($GLOBALS['swc.req']['blk'],strpos($GLOBALS['swc.req']['blk'],':')),':'),
			'act'=>trim(substr($GLOBALS['swc.req']['blk'],strpos($GLOBALS['swc.req']['blk'],':')),':'),
		);
	}else{
		$GLOBALS['swc.page']['blk']=$GLOBALS['swc.req']['blk'];
	}
}
//< Добавление главного блока на страницу (при необходимости) =
//<< Определение параметров запрошенной страницы ==============================

//== Обработка запросов >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
if(isset($_REQUEST['act'])||isset($_REQUEST['do'])){
/**
Выполнение интерфейса действия
Интерфейс действия выполняется в случае если в запросе присутствует один из параметров: ?act или ?do

[!] Обработка запрещенных модулей удалена

Если строка запроса страницы содержит имя/описание блока - производится попытка определения/загрузки его параметров.
Если строка запроса страницы не содержит указания на блок и в запросе не указан параметр ?blk - производится попытка получить имя модуля из параметра ?act
Если параметр ?blk задан строкой - производится попытка поиска описания запрошенного блока (функция blk_load())
Если описание блока не найдено - производится определение и загрузка параметров основного (главного) блока текущей (запрошенной) страницы.
:!: Основной (главный) блок текущей страницы может быть переписан запросом вида /page:mdl.method?params (см. выше)
Вне зависимости от того определен модуль для выполнения действия или нет, производится разбор параметра ?act (?do) на предмет определения модуля для выполнения.
	[+] В этом случае, значение параметра может иметь вид: ?act={module}:{method}
	[-] Если запрошенный модуль запрещен - производится выполнение метода действия ядра системы, которому передается параметр ?act в полной нотации ({module}.{method})
Поиск файла интерфейса требуемого для выполнения производится функцией int_find()

#upd <20/01/2011> Eugeny Leonov
	[f] при найденном описании главного блока запрашиваемой страницы в параметры блока не попадают параметры переданные запросом.
	[+] параметры переданные запросом объединяются с параметрами определенными в описании блока.

#upd <19/01/2011> Eugeny Leonov
	[+] обработка параметра [blk] полученного из строки запрашиваемой страницы.
*/

	$GLOBALS['out']['content']=false;
	if(!isset($_REQUEST['act'])){$_REQUEST['act']=$_REQUEST['do'];}
	//= Определение и загрузка параметров блока >
	$blk=false;
	if(isset($GLOBALS['swc.req']['blk'])&&is_string($GLOBALS['swc.req']['blk'])){$blk=blk_load($GLOBALS['swc.req']['blk']);}
	if(!is_array($blk)){if(isset($_REQUEST['blk'])&&is_string($_REQUEST['blk'])){$blk=blk_load($_REQUEST['blk']);}}
	if(!is_array($blk)){if(isset($GLOBALS['swc.page']['blk'])){if(is_array($GLOBALS['swc.page']['blk'])){$blk=$GLOBALS['swc.page']['blk'];}else{$blk=blk_load($GLOBALS['swc.page']['blk']);}}}
	//@todo: Должна формироваться ошибка: Блок не указан или не найден.
	//@hint: Ошибка формируется при невозможности найти файл интефейса модуля во внутренних методах ядра ниже.
	if(!$blk){$blk=array('req'=>reqGet(),'pars'=>$_REQUEST);}
	//< Определение и загрузка параметров блока =
	//= Определение и поиск интерфейса >
	$iFile=false;
	if(strpos($_REQUEST['act'],':')){
		$blk['mdl']=trim(substr($_REQUEST['act'],0,strpos($_REQUEST['act'],':')),':');
		$blk['act']=trim(substr($_REQUEST['act'],strpos($_REQUEST['act'],':')),':');
	}else{$blk['act']=$_REQUEST['act'];}
	if(isset($blk['mdl'])){$iFile=int_find($blk['mdl'],'act');}
	//< Определение и поиск интерфейса =
	//= Заполнение параметров блока для вызова интерфейса >
	if(!isset($blk['pars'])){$blk['pars']=array();}
	$blk['pars']=array_merge_recursive($blk['pars'],$_REQUEST);
	//< Заполнение параметров блока для выхова интерфейса =
	if(!$iFile){
		//= Внутренние методы >
		switch($_REQUEST['act']){
			default:
				$GLOBALS['out']['ret']=false;
				setResult(false,'Не указан интерфейс, неизвестный метод: '.$_REQUEST['act']);
				$GLOBALS['out']['content']='@Error: '.getResult('reason');
			break;
		}
		//< Внутренние методы =
	}
	if(is_string($iFile)){
		ob_start();
		$GLOBALS['out']['ret']=include(fname($iFile));
		if(!$GLOBALS['out']['ret']){if(getResult('result')){setResult(false,'Неопределенная ошибка выполнения интрефейса действия');}}
		$GLOBALS['out']['content']=ob_get_clean();
	}elseif(isset($GLOBALS['out']['ret'])&&is_true($GLOBALS['out']['ret'])){
		if($GLOBALS['out']['content']!=''){
//@todo: Сохранение отданного контента
//			$_SESSION['']=$GLOBALS['out']['content'];
		}
	}else{_die('Критическая ошибка: Интерфейс модуля определен некорректно и не найден.');} //Так-то выполнение этого условия не должно быть ни в одном случае.

	if(isset($_REQUEST['do'])){
		if(!isset($GLOBALS['out']['headers']['Content-type'])){$GLOBALS['out']['headers']['Content-type']='text/plain; charset=utf-8;';}
		if(!isset($GLOBALS['out']['content'])||(trim($GLOBALS['out']['content'])=='')){
			if(getResult('result')){
				$GLOBALS['out']['content']='@Ok.';
			}else{
				$GLOBALS['out']['content']='@Error: '.getResult('reason');
			}
		}
	}else{
		if(!getResult('result')){_msg('Ошибка выполнения действия. ('.$_REQUEST['act'].'): '.getResult('reason'),'E','SWC.act');}
		if(!isset($GLOBALS['out']['headers']['Location'])){
			if(isset($GLOBALS['swc.req']['ref'])){$GLOBALS['out']['headers']['Location']=$GLOBALS['swc.req']['ref'];}
			else{$GLOBALS['out']['headers']['Location']=href('/');}
		}
		if(trim($GLOBALS['out']['headers']['Location'])==''){$GLOBALS['out']['headers']['Location']=href('/');}
	}
}elseif(isset($_REQUEST['get'])||isset($_REQUEST['gt'])){
	if(!isset($_REQUEST['get'])){$_REQUEST['get']=$_REQUEST['gt'];}
/**
Выполнение интерфейса данных.
Интерфейс данных выполняется в случае если в запросе присутствует параметр ?get или ?gt

Модуль, интерфейс и метод выполняемого интерфейса получается из значения ?get путем вызова функции blk_get()

@todo: Реализовать обработку блока указанного для страницы если параметр 'get' не содержит имени модуля.
*/
	$GLOBALS['out']['content']=false;
	//= Определение и поиск интерфейса >
	$blk_out=blk_get($_REQUEST['get']);
	if(is_array($blk_out)){
		if(isset($blk_out['content'])){$GLOBALS['out']['content']=$blk_out['content'];}
		if(isset($blk_out['ret'])){$GLOBALS['out']['ret']=$blk_out['ret'];}
		if(isset($blk_out['result'])){$GLOBALS['out']['result']=$blk_out['result'];}
	}
	unset($blk_out);
	//< Определение и поиск интерфейса =
	if(!$GLOBALS['out']['content']){
	//= Методы ядра >
		switch($_REQUEST['get']){
			case 'resource': // Запрошен ресурс
				setResult(true);
				if(!isset($_REQUEST['res'])){setResult(false,'Не указан ресурс');
				}else{
					$rFile=res_find($_REQUEST['res']);
					if(is_string($rFile)){
						ob_start();
_msg('Файл: '.$rFile,'D','SWC.core_resource');
						if(substr($rFile,-3)=='php'){$GLOBALS['out']['ret']=include(fname($rFile));}else{$GLOBALS['out']['ret']=true;echo(file_get_contents(fname($rFile)));}
						$GLOBALS['out']['content']=ob_get_clean();
						$GLOBALS['out']['result']=getResult();
						if(!isset($GLOBALS['out']['headers']['Content-type'])){$GLOBALS['out']['headers']['Content-type']=mime_type(fname($rFile));}
					}else{setResult(false,'Ресурс не найден: '.$_REQUEST['res']);}
				}
			break;
			default:
				if(getResult('result')){setResult(false,'@Error: Request interface not found or incorrect method. ('.$_REQUEST['get'].')');}
			break;
		}
	//< Методы ядра =
	}
	if(isset($_REQUEST['gt'])){
		//= Если был запрос ?gt - возвращаемое содержание дополняется тегами >
		$cBody=$GLOBALS['out']['content'];$GLOBALS['out']['content']='@';
		$GLOBALS['out']['result']=getResult();
		if(!getResult('result')){$GLOBALS['out']['content'].='Error';}else{$GLOBALS['out']['content'].='Ok';}
		if(isset($GLOBALS['out']['result']['reason'])&&(trim($GLOBALS['out']['result']['reason'])!='')){$GLOBALS['out']['content'].=':'.$GLOBALS['out']['result']['reason'];}else{$GLOBALS['out']['content'].='.';}
		$GLOBALS['out']['content'].="\n";
		if(isset($GLOBALS['out']['head']['js']['int'])&&is_array($GLOBALS['out']['head']['js']['int'])){
			$GLOBALS['out']['content'].='@JavaScript:Begin'."\n";
			foreach($GLOBALS['out']['head']['js']['int']as$k=>$v){$GLOBALS['out']['content'].='@Block.Start:'.$k."\n".$v."\n".'@Block.End:'.$k."\n";}
			$GLOBALS['out']['content'].='@JavaScript:End'."\n";
		}
		//< Если был запрос ?gt - возвращаемое содержание дополняется тегами =
		if(trim($cBody)!=''){$GLOBALS['out']['content'].="\n".'@Content:Begin'."\n".$cBody."\n".'@Content:End'."\n";}
		
		$GLOBALS['out']['content'].="\n".'@EOF.'."\n";
		//Принудительно выставляем контент-тип в простой текст
		$GLOBALS['out']['headers']['Content-type']='text/plain; charset=utf-8;';
	}else{
		//= Если был запрос ?get - возвращается содержание блока без изменений >
		//К блоку добавляются скрипты.
		if(isset($GLOBALS['out']['head']['js'])&&is_array($GLOBALS['out']['head']['js'])){
			if(isset($GLOBALS['out']['head']['js']['ext'])&&is_array($GLOBALS['out']['head']['js']['ext'])){foreach($GLOBALS['out']['head']['js']['ext']as$k=>$v){
				$GLOBALS['out']['content'].='<script language="JavaScript" src="'.href($v).'"></script>';
			}}
			if(isset($GLOBALS['out']['head']['js']['int'])&&is_array($GLOBALS['out']['head']['js']['int'])){foreach($GLOBALS['out']['head']['js']['int']as$k=>$v){
				$GLOBALS['out']['content'].='<script language="JavaScript">'.$v.'</script>';
			}}
		}
		//< Если был запрос ?get - возвращается содержание блока без изменений =
	}
	//@todo: Проверка и установка $GLOBALS['out']['headers']['Content-type']
}else{
	// (?) Содержание страницы может быть определено модулем загружаемым при старте системы
	if(!isset($GLOBALS['out']['content'])||!is_string($GLOBALS['out']['content'])){$GLOBALS['out']['content']=false;}
	if(!isset($GLOBALS['out']['head']['title'])){
		if(isset($GLOBALS['swc.page']['title'])){$GLOBALS['out']['head']['title']=$GLOBALS['swc.page']['title'];}
		else{$GLOBALS['out']['head']['title']='Undefined';}
	}
	if(!$GLOBALS['out']['content']){
	//== Ищем шаблон и собираем страницу >>
		//= Ищем шаблон >
/*
		//Шаблон может быть найден и определен модулем theme или page, загружаемым при инициализации ядра.
		if(!isset($GLOBALS['swc.tpl'])||!is_array($GLOBALS['swc.tpl'])){$GLOBALS['swc.tpl']=false;}
		if(!$GLOBALS['swc.tpl']){if(isset($GLOBALS['swc.page']['tpl'])){$GLOBALS['swc.tpl']=tpl_load($GLOBALS['swc.page']['tpl']);}}
		if(!$GLOBALS['swc.tpl']){if(isset($_SESSION['swc.theme']['default']['tpl'])){$GLOBALS['swc.tpl']=tpl_load($_SESSION['swc.theme']['default']['tpl']);}}
		if(!$GLOBALS['swc.tpl']){if(isset($_SESSION['swc.site']['default']['tpl'])){$GLOBALS['swc.tpl']=tpl_load($_SESSION['swc.site']['default']['tpl']);}}
		if(!$GLOBALS['swc.tpl']){if(isset($_SESSION['swc.cfg']['default']['tpl'])){$GLOBALS['swc.tpl']=tpl_load($_SESSION['swc.cfg']['default']['tpl']);}}
		if(!$GLOBALS['swc.tpl']){_die('Не определен шаблон страницы.');}
		//< Ищем шаблон =
*/
		$GLOBALS['out']['content']=pg_parse($GLOBALS['swc.page']);
		if(!$GLOBALS['out']['content']){$GLOBALS['out']['content']=tpl_parse('e500.pg',getResult());}
	//<< Ищем шаблон и собираем страницу ==
	}
	if(!$GLOBALS['out']['content']){
		$GLOBALS['out']['content']='<html><head><title>SWC.core:Error</title></head><body><h1>SWC.core:Error</h1><p>Empty page content.</p>';
		//= Вывод сообщений >
		if(isset($GLOBALS['swc.msg'])&&is_array($GLOBALS['swc.msg'])){foreach($GLOBALS['swc.msg']as$mk=>$mv){$GLOBALS['out']['content'].='<b>'.$mk.':</b><pre>'.print_r($mv,true).'</pre>';}}
		//< Вывод сообщений =
		$GLOBALS['out']['content'].='</body></html>';
	}
}
//<< Обработка запросов =======================================================

if(isset($GLOBALS['swc.msg'])){_die('Установлены старые сообщения.');}
//== Финиш-сборка и отдача контента >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
while(ob_get_level()){ob_end_clean();}
if(headers_sent()){setResult(false,'Несанкционированный вывод данных.');die('Headers already sent.');}
//= Вывод HTTP-заголовков >
if(!isset($GLOBALS['out']['headers'])||!is_array($GLOBALS['out']['headers'])){$GLOBALS['out']['headers']=array();}
if(!isset($GLOBALS['out']['headers']['Content-type'])){ //Не определен тип контента
	if(isset($GLOBALS['swc.tpl']['content-type'])){$GLOBALS['out']['headers']['Content-type']=$GLOBALS['swc.tpl']['content-type'];}
	elseif(isset($_SESSION['swc.theme']['content-type'])){$GLOBALS['out']['headers']['Content-type']=$_SESSION['swc.theme']['content-type'];}
	elseif(isset($GLOBALS['swc.page']['content-type'])){$GLOBALS['out']['headers']['Content-type']=$GLOBALS['swc.page']['content-type'];}
	elseif(isset($_SESSION['swc.site']['default']['content-type'])){$GLOBALS['out']['headers']['Content-type']=$_SESSION['swc.site']['default']['content-type'];}
	elseif(isset($_SESSION['swc.cfg']['default']['content-type'])){$GLOBALS['out']['headers']['Content-type']=$_SESSION['swc.cfg']['default']['content-type'];}
	else{_msg('Не указан Content-type.','W','SWC.core');$GLOBALS['out']['headers']['Content-type']='text/html; charset=utf-8;';}
}
foreach($GLOBALS['out']['headers']as$k=>$v){header($k.': '.$v);}
//< Вывод HTTP-заголовков =
//die($GLOBALS['out']['content']);
//= Вывод содержания >
//@fixme: Ошибочное определение пустого документа при попытке вывода изображений (бинарных данных).
/*
if(!isset($GLOBALS['out']['content'])||(trim($GLOBALS['out']['content'])=='')){
		$GLOBALS['out']['content']='<html><head><title>SWC.core:Error</title></head><body><h1>Empty content</h1>';
		$GLOBALS['out']['content'].='<p>'.getResult('reason').'</p>';
		$GLOBALS['out']['content'].='</body></html>';
}
*/
echo($GLOBALS['out']['content']);
//< Вывод содержания =
//<< Финиш-сборка и отдача контента ===========================================
if(defined('dbgEnabled')&&dbgEnabled&&(isset($_REQUEST['dbg']))&&is_true($_REQUEST['dbg'])){_die('Dbg');}
?>
