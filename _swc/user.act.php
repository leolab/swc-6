<?php
/**
@title: Интерфейс действия подсистемы аутентификации и управления пользователями ядра системы.
@package: SWC-6
@subpackage: core.user
@version: 1.0.rc <18/12/2010>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
if(!defined('htaccess')){die('SWC: Direct access disabled.');}
if(!isset($blk)||!is_array($blk)){return(setResult(false,'Отсутствуют данные интерфейса. [swc]:user'));}
if(!isset($blk['act'])){return(setResult(false,'Не указан метод интерфейса. [swc]:user'));}

switch($blk['act']){
/**
@title: Авторизовать пользователя
@version: 1.0.a <20/11/2010>
@param: &user[logName]
@param: &user[logWord]
*/
	case 'logOn':
		if(!isset($blk['pars']['user']['logName'])||!isset($blk['pars']['user']['logWord'])){return(setResult(false,'Не указаны требуемые поля.'));}
		return(user_logOn($blk['pars']['user']['logName'],$blk['pars']['user']['logWord']));
	break;

/**
@title: Завершить работу пользователя
@version: 1.0.rc <07/01/2011>
*/
	case 'logOff':
		return(user_logOff());
	break;
	
/**
@title: Создать нового пользователя
	Метод доступен пользователям фходящим в группы администраторов пользователей [user.admin] или администраторов ядра системы [swc.admin] конфигурации ядра системы.
@version: 1.0.a <01/01/2011>
@param: &user[]
 
#rev.2 <31/01/2011> Eugeny Leonov
	[f] Поправлен баг

#rev.1 <28/01/2011> Eugeny Leonov
	[+] Проверка прав пользователя
	[+] Вызов функции user_check()
*/
	case 'create':
		//= Проверка прав текущего пользователя >
		$a=false;
		if(isset($_SESSION['swc.cfg']['user.admin'])&&is_array($_SESSION['swc.cfg']['user.admin'])){$a=user_isMember($_SESSION['swc.cfg']['user.admin']);}
		if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])&&is_array($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
		if(!$a){return(setResult(false,'Доступ запрещен.'));}
		unset($a);
		//< Проверка прав текущего пользователя =
		if(!isset($blk['pars']['user'])||!is_array($blk['pars']['user'])){return(setResult(false,'Отсутствуют данные.'));}
		if(!isset($blk['pars']['user']['logName'])||(trim($blk['pars']['user']['logName'])=='')){return(setResult(false,'Не указан логин пользователя.'));}
//		if(!isset($blk['pars']['user']['logWord'])||(trim($blk['pars']['user']['logWord'])=='')){return(setResult(false,'Не указан пароль пользователя.'));}
		if(!user_create($blk['pars']['user'])){return(setResult(false,'Ошибка создания пользователя: '.getResult('reason')));}
		return(user_check($blk['pars']['user']));
	break;

/**
@title: Изменить пользователя
	Метод доступен пользователям входящим в группы администраторов пользователей [user.admin] или администраторов ядра системы [swc.admin] конфигурации ядра системы.
@version: 1.0.rc <06/01/2011>
@param: &user[]

#rev.1 <28/01/2011> Eugeny Leonov
	[+] Проверка прав пользователя
	[+] Вызов функции user_check()
*/
	case 'update':
		//= Проверка прав текущего пользователя >
		$a=false;
		if(isset($_SESSION['swc.cfg']['user.admin'])&&is_array($_SESSION['swc.cfg']['user.admin'])){$a=user_isMember($_SESSION['swc.cfg']['user.admin']);}
		if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])&&is_array($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
		if(!$a){return(setResult(false,'Доступ запрещен.'));}
		unset($a);
		//< Проверка прав текущего пользователя =
		if(!isset($blk['pars']['user'])||!is_array($blk['pars']['user'])){return(setResult(false,'Отсутствуют данные.'));}
		if(!isset($blk['pars']['user']['logName'])||(trim($blk['pars']['user']['logName'])=='')){return(setResult(false,'Не указан логин пользователя.'));}
		if(!user_update($blk['pars']['user'])){return(setResult(false,'Ошибка изменения пользователя: '.getResult('reason')));}
		return(user_check($blk['pars']['user']));
	break;

/**
@title: Установить пароль
	Метод доступен пользователям входящим в группы администраторов пользователей [user.admin], администраторов ядра системы [swc.admin] и для смены своего пароля текущему пользователю
	Метод недоступен неавторизовавшимся пользователям.
@version: 1.0.rc <07/01/2011>
@param: &user[logName]&user[logWord]
 
#rev.2 <29/01/2011> Eugeny Leonov
	[f] Исправлены ошибки и опечатки.

#rev.1 <28/01/2011> Eugeny Leonov
	[+] Проверка прав доступа
*/
	case 'setPassword':
		if(!isset($blk['pars']['user'])||!is_array($blk['pars']['user'])){return(setResult(false,'Отсутствуют данные.'));}
		if(!isset($blk['pars']['user']['logName'])||(trim($blk['pars']['user']['logName'])=='')){return(setResult(false,'Не указан логин пользователя.'));}
		//= Проверка прав текущего пользователя >
		$a=false;
		if(!user_isGuest()){
			$a=($blk['pars']['user']['logName']==$_SESSION['swc.user']['logName']);
			if(!$a){if(isset($_SESSION['swc.cfg']['user.admin'])&&is_array($_SESSION['swc.cfg']['user.admin'])){$a=user_isMember($_SESSION['swc.cfg']['user.admin']);}}
			if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])&&is_array($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
		}
		if(!$a){return(setResult(false,'Доступ запрещен.'));}
		unset($a);
		//< Проверка прав текущего пользователя =
		if(!isset($blk['pars']['user']['logWord'])){return(setResult(false,'Не указан пароль пользователя.'));}
		return(user_setPassword($blk['pars']['user']['logName'],$blk['pars']['user']['logWord']));
	break;

/**
@title: Удалить пользователя
@version: 1.0.rc <06/01/2011>
@param: &logName string

@todo: Проверка прав текущего пользователя
*/
	case 'remove':
		if(!isset($blk['pars']['logName'])||!is_string($blk['pars']['logName'])||(trim($blk['pars']['logName'])=='')){return(setResult(false,'Не указан логин пользователя.'));}
		return(user_remove($blk['pars']['logName']));
	break;

/**
@title: Добавить пользователя в группу
@version: 1.0.a <16/15/2010>
@param: &user[logName]
@param: &group[name]

#rev.1 <01/02/2011> Eugeny Leonov
	[+] Проверка прав текущего пользователя
*/
	case 'grpAdd':
		//= Проверка прав пользователя >
		$a=false;
		if(isset($_SESSION['swc.cfg']['user.manage'])){$a=user_isMember($_SESSION['swc.cfg']['user.manage']);}
		if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
		if(!$a){return(setResult(false,'Недостаточно прав.'));}
		//< Проверка прав пользователя
		if(!isset($blk['pars']['user']['logName'])||!is_string($blk['pars']['user']['logName'])||(trim($blk['pars']['user']['logName'])=='')){return(setResult(false,'Не указан пользователь'));}
		if(!isset($blk['pars']['group']['name'])||!is_string($blk['pars']['group']['name'])||(trim($blk['pars']['group']['name'])=='')){return(setResult(false,'Не указана группа.'));}
		return(user_grpAdd($blk['pars']['user']['logName'],$blk['pars']['group']['name']));
	break;

/**
@title: Удалить пользователя из группы
@version: 1.0.a <16/15/2010>
@param: &user[logName]
@param: &group[name]

#rev.1 <01/02/2011> Eugeny Leonov
	[+] Проверка прав пользователя.
*/
	case 'grpDel':
		//= Проверка прав пользователя >
		$a=false;
		if(isset($_SESSION['swc.cfg']['user.manage'])){$a=user_isMember($_SESSION['swc.cfg']['user.manage']);}
		if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
		if(!$a){return(setResult(false,'Недостаточно прав.'));}
		//< Проверка прав пользователя =
		if(!isset($blk['pars']['user']['logName'])||!is_string($blk['pars']['user']['logName'])||(trim($blk['pars']['user']['logName'])=='')){return(setResult(false,'Не указан пользователь.'));}
		if(!isset($blk['pars']['group']['name'])||!is_string($blk['pars']['group']['name'])||(trim($blk['pars']['group']['name'])=='')){return(setResult(false,'Не указана группа.'));}
		return(user_grpDel($blk['pars']['user']['logName'],$blk['pars']['group']['name']));
	break;

//== Методы работы с группами >>
/**
@title: Создать группу
	Права на создание группы пользователей имеют пользователи:
		- пользователи входящие в одну из групп перечисленных в разделе [user.manage] конфигурации ядра системы.
		- при отсутствии раздела [user.manage] - пользователи перечисленные в разделе [swc.manage] конфигурации ядра системы.
		- при отсутствии указанных разделов метод недоступен.

@version: 1.0.a.1 <12/01/2011>
@param: &group[name]&group[title]

#rev.2 <29/01/2011> Eugeny Leonov
	[*] Изменена логика проверки прав пользователя
	[*] Изменена функция изменения/создания группы. (в связи с переименованием функции изменения/создания группы)
	[+] Изменение группы.
*/
	case 'group.update':
	case 'group.create':
		//= Проверка прав пользователя >
		$a=false;
		if(isset($_SESSION['swc.cfg']['user.manae'])&&is_array($_SESSION['swc.cfg']['user.manage'])){$a=user_isMember($_SESSION['swc.cfg']['user.manage']);}
		if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])&&is_array($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
		if(!$a){return(setResult(false,'Недостаточно прав.'));}
		//< Проверка прав пользователя =
		if(!isset($blk['pars']['group']['name'])){return(setResult(false,'Не указано имя группы.'));}
		return(user_group_save($blk['pars']['group']));
	break;

/**
@title: Удалить группу
@version: 1.0.a <29/01/2011>
@param: &group[name]
*/
	case 'group.remove':
		//= Проверка прав пользователя >
		$a=false;
		if(isset($_SESSION['swc.cfg']['user.manage'])&&is_array($_SESSION['swc.cfg']['user.manage'])){$a=user_isMember($_SESSION['swc.cfg']['user.manage']);}
		if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])&&is_array($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
		if(!$a){return(setResult(false,'Недостаточно прав.'));}
		//< Проверка прав пользователя =
		if(!isset($blk['pars']['group']['name'])){return(setResult(false,'Не указано имя группы.'));}
		return(user_group_remove($blk['pars']['group']['name']));
	break;

//<< Методы работы с группами ==
	default:
		return(setResult(false,'Неизвестный метод интерфейса [swc.user]:'.$blk['act']));
	break;
}
?>
