<?php
/**
@title: Интерфейс данных ядра системы
@package: SWC-6
@subpackage: core
@version: 1.0.rc.2 <07/01/2011>
@author: Eugeny Leonov <eleonov@leolab.info>

#rev.3 <19/01/2011> Eugeny Leonov
	[*] контрольная ревизия, подробности см. в описаниях методов.

#rev.2 <07/01/2011>
	[*] Очередное изменение именования методов интерфейса.
	[+] Добавлены методы управления ядра системы (группа *.manage.*)

#rev.1 <20/12/2010>
 	[*] В связи с изменением обработки интерфейсов модулей, из имен методов убраны имена модулей.
 	[*] Методы модулей вынесены в собственные интерфейсы.
*/
if(!defined('htaccess')){die('SWC: Direct access disabled.');}
if(!isset($blk)||!is_array($blk)){return(setResult(false,'Отсутствуют параметры вызова интерфейса.'));}
if(!isset($blk['get'])||!is_string($blk['get'])){return(setResult(false,'Не указан метод интерфейса (SWC.core:get).'));}

$data['blk']=$blk;

switch($blk['get']){
//== Основные методы ядра системы >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/**
@title: Вывести содержание файла.
@version: 1.0 <23/02/2011>
*/
	case 'text':
		//= Проверка прав доступа >
		$a=user_isAllow($_SESSION['swc.site'],'site.manage');
		if(!$a){$a=user_isAllow($_SESSION['swc.cfg']['swc.manage']);}
		$data['blk']['pars']['acl']=$a;
		unset($a);
		//< Проверка прав доступа =
		if(!isset($data['blk']['pars']['fName'])){$data['blk']['pars']['fName']=$GLOBALS['swc.req']['href'];}
		$data['content']=fload('@S/%site%/_data/text/'.$data['blk']['pars']['fName'].'.html');
		if(!$data['content']){$data['content']=fload('@D/%site%/text/'.$data['blk']['pars']['fName'].'.html');}
		if(!$data['content']){$data['content']=fload('@D/text/'.$data['blk']['pars']['fName'].'.html');}
		if(!$data['content']){$data['content']=fload('@S/%site%/_data/text/_default.html');}
		if(!$data['content']){$data['content']='';}
		echo(tpl_parse('text',$data));
		return(getResult('result'));
	break;

/**
@title: Блок E403
@version: 1.0.rc <07/01/2011>
@todo: Должен обрабатываться блок указанный в конфигурации темы/сайта/ядра [blk.e403]
*/
	case 'e403':
		echo(tpl_parse('e403',$data));
		return(getResult('result'));
	break;

/**
@title: Блок Е404
@version: 1.0.rc <15/12/2010>
@todo: Должен обрабатываться блок указываемый в конфигурации темы/сайта/ядра [blk.e404]
*/
	case 'e404':
		echo(tpl_parse('e404',$data));
		return(getResult('result'));
	break;

/**
@title: Стартовый блок ядра системы
@version: 1.0.rc <15/12/2010>
@todo: Должен обрабатываться блок указываемый в конфигурации темы/сайта/ядра [blk.start]
*/
	case 'start':
		echo(tpl_parse('start',$data));
		return(getResult('result'));
	break;

/**
@title: Показать файл информации
@version: 1.0.rc <15/12/2010>
@param: &p={core|site|theme|mdl}
@param: &v={release|changes|readme}
*/
	case 'show_info':
		if(!isset($data['blk']['pars']['p'])||!in_array($data['blk']['pars']['p'],array('core','site','theme','mdl'))){$data['blk']['pars']['p']='core';}
		if(!isset($data['blk']['pars']['v'])||!in_array($data['blk']['pars']['v'],array('release','changes','readme'))){$data['blk']['pars']['v']='release';}
		switch($data['blk']['pars']['p']){
			case 'core':
				if(file_exists(swc_base.'/'.$data['blk']['pars']['v'].'.txt')){$data['content']=fload(swc_base.'/'.$data['blk']['pars']['v'].'.txt');}
				else{$data['content']='<p>Файл не найден (%core%/'.$data['blk']['pars']['v'].')</p>';}
			break;
			case 'site':
				if(!isset($data['blk']['pars']['n'])||(trim($data['blk']['pars']['n'])=='')){$data['blk']['pars']['n']='%site%';}
				if(file_exists(fname('@S/'.$data['blk']['pars']['n'].'/'.$data['blk']['pars']['v'].'.txt'))){$data['content']=fload('@S/'.$data['blk']['pars']['n'].'/'.$data['blk']['pars']['v'].'.txt');}
				else{$data['content']='<p>Файл не найден (%site%/'.$data['blk']['pars']['v'].')</p>';}
			break;
			case 'theme':
				if(!isset($data['blk']['pars']['n'])||(trim($data['blk']['pars']['n'])=='')){$data['blk']['pars']['n']='%theme%';}
				if(file_exists(fname('@T/'.$data['blk']['pars']['n'].'/'.$data['blk']['pars']['v'].'.txt'))){$data['content']=fload('@T/'.$data['blk']['pars']['n'].'/'.$data['blk']['pars']['v'].'.txt');}
				else{$data['content']='<p>Файл не найден (%theme%/'.$data['blk']['pars']['v'].'</p>';}
			break;
			case 'mdl':
				$data['content']='<p>@TODO: Определение положения модуля в зависимости от того указан сайт или нет.</p>';
			break;
			default:
				return(setResult(false,'Неизвестный раздел: '.$data['blk']['pars']['view']));
			break;
		}
		echo(tpl_parse('swc:show.info',$data));
		return(getResult('result'));
	break;
//<< Основные методы ядра системы ==============================================
//== Методы настройки и управления ядра системы >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/**
@title: Получить меню разделов управления ядра системы
@version: 1.0.rc <17/12/2010>

#rev.2 <09/01/2011> Eugeny Leonov
	[f]	Критическая ошибка при отсутствии раздела [swc.manage] в файле конфигурации ядра системы (swc.conf)
	[+]	При отсутствии, раздел создается, в раздел добавляется группа swc.admin
	[!]	При создании раздела файл конфигурации ядра системы не перезаписывается. Раздел существует только для текущей сессии.

#rev.1 <07/01/2011> Eugeny Leonov
	[+] Проверка прав пользователей
		Меню управления отображается только пользователям входящим в одну из групп указанных в разделе [swc.manage] файла конфигурации ядра системы

@todo: Разделение прав пользователей по пунктам (разделам) меню.
*/
	case 'manage.menu':
//		if(!isset($_SESSION['swc.cfg']['swc.manage'])||!is_array($_SESSION['swc.cfg']['swc.manage'])){$_SESSION['swc.cfg']['swc.manage']['swc.admin']='swc.admin';}
		if(!isset($_SESSION['swc.cfg']['swc.manage'])||!is_array($_SESSION['swc.cfg']['swc.manage'])){$_SESSION['swc.cfg']['swc.manage']=array();}
		if(!user_isMember($_SESSION['swc.cfg']['swc.manage'])){echo(int_get('swc:e403'));return(setResult(false,'Недостаточно прав.'));}
		$data['blk']['title']='Конфигурация:';
		$data['items']=array(
			array('href'=>'%path%/:swc:manage.config','title'=>'Параметры ядра',),
			array('href'=>'%path%/:user:manage','title'=>'Пользователи',),
			array('href'=>'%path%/:user:group.manage','title'=>'Группы пользователей',),
			array('href'=>'%path%/:mdl:manage','title'=>'Модули',),
			array('href'=>'%path%/:site:manage','title'=>'Сайты',),
			array('href'=>'%path%/:file:list','title'=>'Файлы',),
		);
		if(mdl_exists('dbi')){$data['items'][]=array('href'=>'%path%/:dbi:manage','title'=>'Управление БД.');}
		echo(tpl_parse('menus:list',$data));
		return(getResult('result'));
	break;

/**
@title: Получить форму настройки ядра системы
@version: 1.0.rc <22/12/2010>

#rev.3 <30/01/2011> Eugeny Leonov
	[-] Убрана попытка загрузки конфигурации ядра системы из корня ядра системы.

#rev.2 <25/01/2011> Eugeny Leonov
	[!] Изменен раздел по которому производится определение прав доступа.

#rev.1 <11/01/2011> Eugeny Leonov
	[+] Реализована проверка прав доступа.
		Метод выполняется только если текущий пользователь входит в группы указанные в разделе [swc.manage] конфигурации ядра системы.
*/
	case 'manage.config':
		if(!isset($_SESSION['swc.cfg']['swc.admin'])||!is_array($_SESSION['swc.cfg']['swc.admin'])){$_SESSION['swc.cfg']['swc.admin']=array();}
		if(!user_isMember($_SESSION['swc.cfg']['swc.admin'])){echo(int_get('swc:e403'));return(setResult(false,'Недостаточно прав.'));}
		$data['conf']=cfg_load('@D/conf/swc');
		if(!$data['conf']){$data['conf']=cfg_load(swc_base.'/_conf/swc');}
		$data['users']=user_list(false,true);
		echo(tpl_parse('swc:manage.frm',$data));
		return(getResult('result'));
	break;

//<< Методы настройки и управления ядра системы ================================
	default:
		echo(tpl_parse('e500',array('blk'=>$blk,'content'=>'Метод ['.$blk['mdl'].']:'.$blk['get'].' не определен.')));
		return(setResult(false,'Метод: ['.$blk['mdl'].']:'.$blk['get'].' неопределен.'));
	break;
}
?>
