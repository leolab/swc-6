<?php
/**
@title: Интерфейс данных субмодуля управления модулями ядра системы
@package: SWC-6
@subpackage: core.mdl
@version: 1.0.a <08/01/2011>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
if(!defined('htaccess')){die('SWC: Direct access diabled.');}
if(!isset($blk)||!is_array($blk)){return(setResult(false,'Отсутствуют параметры интрефейса.'));}
if(!isset($blk['get'])||(trim($blk['get'])=='')){return(setResult(false,'Не указан метод интрефейса.'));}
$data['blk']=$blk;

switch($blk['get']){

/**
@title: Получить строку информации о модуле
@version: 1.0.a <23/01/2011>
@param: &mdlName
@param: &css[row_class], &css[row_id]
@param: &js[handlers][mdl_config], &js[handlers][mdl_remove]

@todo: Использование вспомогательных параметров из описания блока (при их наличии)
 
#rev.1 <07/02/2011> Eugeny Leonov
	[!] Изменено именование параметра "Имя модуля". &mdlName -> &mdl[name]
	[!] Именование параметров приведено к текущим требованиям.
*/
	case 'row':
		if(!isset($blk['pars']['mdl']['name'])||!is_string($blk['pars']['mdl']['name'])||(trim($blk['pars']['mdl']['name'])=='')){return(setResult(false,'Не указан модуль.'));}
		$data['mdl']=mdl_info($blk['pars']['mdl']['name']);
		if(!$data['mdl']){return(setResult(false,'Модуль ['.$blk['pars']['mdl']['name'].'] не найден.'));}
		//= Проверка прав пользователя >
		if(isset($data['blk']['pars']['jsh']['onRemove'])){ //Удалять модуль из системы может только администратор ядра, и только если модуль не системный.
			$a=false;
			if(isset($data['mdl']['sys'])&&is_true($data['mdl']['sys'])){$a=false;}elseif(isset($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}
			if(!$a){unset($data['blk']['pars']['jsh']['onRemove']);}
			unset($a);
		}
		if(isset($data['blk']['pars']['jsh']['onConfig'])){ //Изменять глобальные настройки модуля могут только пользователи - администраторы и менеджеры ядра.
			$a=false;
			if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
			if(!$a){if(isset($_SESSION['swc.cfg']['swc.manage'])){$a=user_isMember($_SESSION['swc.cfg']['swc.manage']);}}
			if(!$a){unset($data['blk']['pars']['jsh']['onConfig']);}
			unset($a);
		}
		//< Проверка прав пользователя =
		if(isset($blk['pars']['js']['handlers']['mdl_config'])){ // Установлен обработчик настройки модуля
// Настройка (глобальная) модуля доступна администраторам системы и администраторам сайта, если модуль принадлежит сайту
			$a=false;
			if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])&&is_array($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
			if(isset($data['mdl']['site'])&&($data['mdl']['site']==$_SESSION['swc.site']['name'])){
				if(!$a){if(isset($_SESSION['swc.site']['site.admin'])&&is_array($_SESSION['swc.site']['site.admin'])){$a=user_isMember($_SESSION['swc.site']['site.admin']);}}
			}
			if($a){$data['js']['handlers']['mdl_config']=$blk['pars']['js']['handlers']['mdl_config'];}
		}
		if(isset($blk['pars']['js']['handlers']['mdl_delete'])){
// Удаление модуля доступно администраторам системы и администраторам сайта, если модуль принадлежит сайту
			$a=false;
			if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])&&is_array($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
			if(isset($data['mdl']['site'])&&($data['mdl']['site']==$_SESSION['swc.site']['name'])){
				if(!$a){if(isset($_SESSION['swc.site']['site.admin'])&&is_array($_SESSION['swc.site']['site.admin'])){$a=user_isMember($_SESSION['swc.site']['site.admin']);}}
			}
			if($a){$data['js']['handlers']['mdl_delete']=$blk['pars']['js']['handlers']['mdl_delete'];}
		}
		if(isset($blk['pars']['js']['handlers']['mdl_update'])){
// Обновление модуля доступно администраторам системы и администраторам сайта, если модуль принадлежит сайту
			$a=false;
			if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])&&is_array($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
			if(isset($data['mdl']['site'])&&($data['mdl']['site']==$_SESSION['swc.site']['name'])){
				if(!$a){if(isset($_SESSION['swc.site']['site.damin'])&&is_array($_SESSION['swc.site']['site.admin'])){$a=user_isMember($_SESSION['swc.site']['site.admin']);}}
			}
			if($a){$data['js']['handlers']['mdl_update']=$blk['pars']['js']['handlers']['mdl_update'];}
		}
		//< Обработка вспомогательных параметров =
		echo(tpl_parse('mdl:row',$data));
		return(getResult('result'));
	break;

/**
@title: Получить форму управления модулями
@version: 1.0.a <08/01/2011>

@todo: Реализовать дополнительные права пользователей:
	К методу могут иметь доступ пользователи входящие в группы указанные в разделе [site.admin] конфигурации текущего сайта.
	Указанные выше пользователи имеют доступ только к модулям своего сайта.
*/
	case 'manage':
		setResult(true);
		if(!user_isMember($_SESSION['swc.cfg']['swc.manage'])){echo(tpl_parse('e403',$data));return(setResult(false,'Недостаточно прав.'));}
		if(isset($blk['pars']['all_sites'])&&is_true($blk['pars']['all_sites'])){$a=true;}else{$a=false;}
		$data['modules']=mdl_list(false,$a);
		echo(tpl_parse('mdl:manage',$data));
		return(getResult('result'));
	break;
	default:
		return(setResult(false,'Неизвестный метод интерфейса [SWC.mdl]:'.$blk['get']));
	break;
}
//_die('Unrecognized error.');
?>
