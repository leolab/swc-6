<?php
/**
@title: Файл функций работы с сайтами
@package: SWC-6
@subpackage: core.site
@version: 1.0.b <09/01/2011>
@author: Eugeny Leonov <eleonov@leolab.info>
 
#rev.1 <04/02/2011> Eugeny Leonov
	[r] Ревизия файла, убран устаревший код.
*/
if(!defined('htaccess')){die('SWC: Direct access disabled');}

/**
@title: Получить список сайтов
@version: 1.0.rc.1 <09/11/2011>
@return: array|false

#rev.5 <22/02/2011> Eugeny Leonov
	[+] Добавлена загрузка информации о сайте (при ее наличии)

#rev.4 <05/02/2011> Eugeny Leonov
	[f] Для сайтов не загружаются параметры по умолчанию.

#rev.3 <24/01/2011> Eugeny Leonov
	[+] Добавлена загрузка параметров сайта (страниц) ядра системы
	[!] Результат выполнения функции может содержать ошибку, при этом функция всегда возвращает минимум один сайт (страницы ядра системы), даже пустой.
	[+] Добавлен поиск параметров сайта по умолчанию

#rev.2 <11/01/2011> Eugeny Leonov
	[-] Создание папки-хранилища сайтов.
	[+] При отсутствии папки-хранилища сайтов возвращается пустой массив, результат устанавливается в false

#rev.1 <09/11/2011> Eugeny Leonov
	[f]	Ошибка открытия папки-хранилища сайтов при ее отсутствии.
	[+]	Создание папки-хранилища сайтов при ее отсутствии.
	[+]	Создание файла правил запрещающего прямой доступ к папке-хранилищу сайтов.

@check: Возможно определение (и загрузка) папки не являющейся папкой сайта
*/
function site_list(){
	$ret=false;
	$tPath=fname('@S/');
	$ret['_blank']=cfg_load('@D/conf/site');
	if(!$ret['_blank']){$ret['_blank']=cfg_load(swc_base.'/_conf/site');}
	if(!$ret['_blank']){$ret['_blank']=array('name'=>'_blank','cfg_name'=>'@D/conf/site','title'=>'Страницы ядра системы.',);}
	if(file_exists(fname(swc_base.'/swc.info'))){$ret['_blank']['info']=ini_load(swc_base.'/swc.info');}
	if(!file_exists($tPath)||!is_dir($tPath)){setResult(false,'Отсутствует папка-хранилище сайтов.');return($ret);}
	$dh=opendir($tPath);
	if(!$dh){setResult(false,'Ошибка открытия хранилища сайтов.');return($ret);}
	while($f=readdir($dh)){if((trim($f,'.')!='')&&($f[0]!='.')){
		if(cfg_exists($tPath.'/'.$f.'/_conf/site')){$ret[$f]=cfg_load($tPath.'/'.$f.'/_conf/site');}
		elseif(cfg_exists($tPath.'/'.$f.'/site')){$ret[$f]=cfg_load($tPath.'/'.$f.'/site');$ret[$f]['dist']=1;}
		if(file_exists(fname($tPath.'/'.$f.'/site.info'))){$ret[$f]['info']=ini_load($tPath.'/'.$f.'/site.info');}
		if(!isset($ret[$f]['name'])){$ret[$f]['name']=$f;}
	}}
	closedir($dh);
	if(!is_array($ret)){return(setResult(false,'Сайты не найдены.'));}
	setResult(true);
	return($ret);
}

/**
@title: Загрузить параметры сайта
@version: 1.0.b <14/01/2011>
@param: string
@return: array|false

#rev.4 <22/02/2011> Eugeny Leonov
	[+] Добавлена загрузка информации о сайте (при ее наличии)

#rev.3 <04/02/2011> Eugeny Leonov
	[r] Ревизия функции, убран устаревший код.

#rev.2 <30/01/2011> Eugeny Leonov
	[*] Изменена логика поиска параметров сайта по умолчанию

#rev.1 <24/01/2011> Eugeny Leonov
	[+] Добавлен поиск параметров сайта по умолчанию
*/
function site_load($sName){
	if(!is_string($sName)){return(setResult(false,'Неверный формат параметра.'));}
	if(trim($sName)==''){$sName='_default';}
	if($sName=='_default'){if(isset($_SESSION['swc.user']['site'])){$sName=$_SESSION['swc.user']['site'];}}
	if($sName=='_default'){if(isset($_SESSION['swc.cfg']['default']['site'])){$sName=$_SESSION['swc.cfg']['default']['site'];}}
	if($sName=='_default'){$sName='_blank';}
	$ret=false;
	switch($sName){
		case '_blank': //Страницы ядра системы
			$ret=cfg_load('@D/conf/site');
			if(!$ret){$ret=cfg_load(swc_base.'/_conf/site');if(is_array($ret)){$ret['dist']=1;}}
			if(is_array($ret)){$ret['sys']=1;} //Системные страницы.
			if(is_array($ret)){if(file_exists(fname(swc_base.'/swc.info'))){$ret['info']=ini_load(swc_base.'/swc.info');}}
		break;
		default:
			$ret=cfg_load('@S/'.$sName.'/_conf/site');
			if(!$ret){$ret=cfg_load('@S/'.$sName.'/site');if(is_array($ret)){$ret['dist']=1;}}
			if(is_array($ret)){if(file_exists(fname('@S/'.$sName.'/site.info'))){$ret['info']=ini_load('@S/'.$sName.'/site.info');}}
		break;
	}
	if(!$ret){return(setResult(false,'Ошибка загрузки параметров сайта ('.$sName,')'));}
	if(!isset($ret['name'])){$ret['name']=$sName;}
	setResult(true);return($ret);
}

/**
@title: Сохранить параметры сайта
@version: 1.0.a <27/01/2011>
@param: array
 
#rev.1 <04/02/2011> Eugeny Leonov
	[+] Сброс параметра dist
*/
function site_save($sData){
	if(!isset($sData['name'])||!is_string($sData['name'])||(trim($sData['name'])=='')){return(setResult(false,'Не указано имя сайта.'));}
	if($sData['name']=='_default'){ //Сайт по умолчанию
		if(isset($_SESSION['swc.user']['site'])){$sData['name']=$_SESSION['swc.user']['site'];}
		elseif(isset($_SESSION['swc.cfg']['default']['site'])){$sData['name']=$_SESSION['swc.cfg']['default']['site'];}
		else{$sData['name']='_blank';}
	}
	if(isset($sData['dist'])){unset($sData['dist']);}
	switch($sData['name']){
		case '_blank':
			return(cfg_save('@D/conf/site',$sData));
		break;
		default:
			return(cfg_save('@S/'.$sData['name'].'/_conf/site',$sData));
		break;
	}
}

/**
@title: Изменить текущий сайт
@version: 1.0.b <14/01/2011>
@param: [string]
@return: bool

#rev.4 <08/03/2011> Eugeny Leonov
	[+] Если в конфигурации сайта установлен параметр theme производится переключение на соответсвующую тему.

#rev.3 <04/02/2011> Eugeny Leonov
	[r] Ревизия функции, убран устаревший код.

#rev.2 <30/01/2011> Eugeny Leonov
	[*] Если не удалось определить имя сайта - оно устанавливается в _blank (страницы ядра системы)
	[-] Если определенное имя сайта не удалось загрузить - дальнейший поиск не производится.
 
#rev.1 <20/01/2011> Eugeny Leonov
	[*] Если не указан сайт - перегружается текущий. Если текущий сайт не выбран - производится последовательный поиск определения сайта:
		- определенного для пользователя
		- определенного в системе по умолчанию
 		- сайт устанавливается в _blank (страницы ядра системы)
*/
function site_change($sName=false){
	if(!$sName){
		if(isset($_SESSION['swc.site']['name'])){$sName=$_SESSION['swc.site']['name'];}
		elseif(isset($_SESSION['swc.user']['site'])){$sName=$_SESSION['swc.user']['site'];}
		elseif(isset($_SESSION['swc.cfg']['default']['site'])){$sName=$_SESSION['swc.cfg']['default']['site'];}
		else{$sName='_blank';}
	}
	$ns=site_load($sName);
	if(!$ns){return(setResult(false,'Ошибка загрузки сайта ('.$sName.'): '.getResult('reason')));}
	//= Проверка прав >
	if(isset($ns['grp.allow'])&&is_array($ns['grp.allow'])){if(!user_isMember($ns['grp.allow'])){return(setResult(false,'Доступ запрещен.'));}}
	if(isset($ns['grp.deny'])&&is_array($ns['grp.deny'])){if(user_isMember($ns['grp.deny'])){return(setResult(false,'Доступ запрещен.'));}}
	if(isset($ns['disabled'])&&is_true($ns['disabled'])){return(setResult(false,'Доступ запрещен.'));}
	//< Проверка прав =
	//= Проверка и установка темы сайта >
	if(isset($ns['theme'])){if(!theme_change($ns['theme'])){return(setResult(false,'Ошибка установки темы сайта: '.getResult('reason')));}}
	//< Проверка и установка темы сайта =
	$_SESSION['swc.site']=$ns;
	$_SESSION['swc.cfg']['site']=$ns['name'];
	return(setResult(true));
}
?>
