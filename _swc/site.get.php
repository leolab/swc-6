<?php
/**
@title: Интерфейс данных поддержки сайтов
@package: SWC-6
@subpackage: core.site
@version: 1.0.a <21/01/2011>
@author: Eugeny Leonov <eleonov@leolab.info>
 
#rev.1 <22/02/2011> Eugeny Leonov
*/
if(!defined('htaccess')){die('SWC: Direct access disabled.');}
if(!isset($blk)||!is_array($blk)){return(setResult(false,'Отсутствуют данные для интерфейса.'));}
if(!isset($blk['get'])){return(setResult(false,'Не указан метод интрефейса.'));}
$data['blk']=$blk;

switch($blk['get']){
/**
@title: Получить селект-форму смены сайта
	В форме отображаются все сайты определенные в системе.
@version: 1.0.a <21/01/2011>

@todo: Реализовать вывод списка только указанных сайтов.

#rev.1 <22/02/2011> Eugeny Leonov
*/
	case 'select':
		if(!isset($blk['pars']['items'])||!is_array($blk['pars']['items'])){$blk['pars']['items']=false;}
		$data['items']=site_list($blk['pars']['items']);
		echo(tpl_parse('site:select',$data));
		return(getResult('result'));
	break;

/**
@title: Блок информации о сайте
@version: 1.0.a <14/02/2011>
@param: &site[name]

#rev.1 <22/02/2011> Eugeny Leonov
	[+] Если не указан запрашиваемый сайт используется информация текущего сайта.
*/
	case 'info':
		if(!isset($blk['pars']['site']['name'])||!is_string($blk['pars']['site']['name'])||(trim($blk['pars']['site']['name'])=='')){$blk['pars']['site']['name']='%site%';}
		if(!file_exists(fname('@S/'.$blk['pars']['site']['name'].'/site.info'))){return(setResult(false,'Отсутствует информация запрашиваемого сайта.'));}
		$data['info']=ini_load('@S/'.$blk['pars']['site']['name'].'/site.info');
		echo(tpl_parse('site:info',$data));
		return(getResult('result'));
	break;

//== Методы управления сайтами >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/**
@title: Получить форму управления сайтами
@version: 1.0.a <24/01/2011>

#rev.1 <22/02/2011> Eugeny Leonov
*/
	case 'manage':
		$a=false;
		if(isset($_SESSION['swc.cfg']['swc.admin'])&&is_array($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}
		if(!$a){echo(tpl_parse('e403',$data));return(setResult(false,'Недостаточно прав.'));}
		$data['sites']=site_list();
		echo(tpl_parse('site:manage',$data));
		return(getResult('result'));
	break;

/**
@title: Получить строку списка сайтов
@version: 1.0.a <24/01/2011>
@param: &siteName

@todo: Проверка прав на запрошенные обработчики
@todo: Проверка наличия файла описания сайта
@todo: Проверка наличия файла релиза сайта
 
#rev.6 <20/02/2011> Eugeny Leonov
	[-] jsh[onRemove]
	[+] acl[cfgReset]

#rev.5 <06/02/2011> Eugeny Leonov
	[+] Проверка JavaScript-обработчиков
	[+] Проверка прав пользователя.

#rev.4 <05/02/2011> Eugeny Leonov
	[f] Несуществующий индекс при выводе ошибки загрузки параметров сайта.
	[+] Проверка обработчиков.

#rev.3 <31/01/2011> Eugeny Leonov
	[f] Метод отредактирован под текущие требования. 

#rev.2 <27/01/2011> Eugeny Leonov
	[f] Метод не отдает данных.
	[-] Проверка прав текущего пользователя.
 
#rev.1 <26/01/2011> Eugeny Leonov
	[!] Переименование метода (list.row)->(row)
*/
	case 'row':
		if(!isset($blk['pars']['site']['name'])||!is_string($blk['pars']['site']['name'])||(trim($blk['pars']['site']['name'])=='')){setResult(false,'Не указано имя сайта.');return(_e500($data));}
		$data['site']=site_load($blk['pars']['site']['name']);
		if(!$data['site']){return(setResult(false,'Ошибка загрузки параметров сайта: '.$blk['pars']['site']['name']));}
		//= Проверка прав на запрошенные обработчики >
		if(isset($data['blk']['jsh']['onSelect'])){ //Выбор сайта (установка текущим)
			$a=false;
			//Пока разрешено всем, см. ./changes.txt
			unset($a);
		}
		if(isset($data['blk']['pars']['jsh']['onConfig'])){ //Редактирование настроек (параметров) сайта
			$a=user_isAllow($data['site'],'site.admin');
			if(!$a){$a=user_isAllow($_SESSION['swc.cfg'],'swc.admin');}
			if(!$a){unset($data['blk']['pars']['jsh']['onConfig']);}
			unset($a);
		}
		if(isset($data['blk']['pars']['jsh']['onInfo'])){ //Информация о сайте
			$a=false;
			//Пока разрешено всем, см ./changes.txt
			//Если есть файл информации о сайте.
			$a=file_exists(fname('@S/'.$data['site']['name'].'/site.info'));
			if(!$a){unset($data['blk']['pars']['jsh']['onInfo']);}
			unset($a);
		}
		if(isset($data['blk']['pars']['acl']['cfgReset'])&&is_true($data['blk']['pars']['acl']['cfgReset'])){
			$a=user_isAllow($data['site'],'site.admin');
			if(!$a){$a=user_isAllow($_SESSION['swc.cfg'],'swc.admin');}
			if($a){if(isset($data['site']['dist'])&&is_true($data['site']['dist'])){$a=false;}}
			if(!$a){unset($data['blk']['pars']['acl']['cfgReset']);}
			unset($a);
		}
		//< Проверка прав на запрошенные обработчики =
		echo(tpl_parse('site:row',$data));
		return(getResult('result'));
	break;

/**
@title: Получить форму управления сайтом
@version: 1.0.a <24/01/2011>
@param: &siteName

#rev.1 <31/01/2011> Eugeny Leonov
	[f] Метод переработан под текущие требования.
*/
	case 'manage.config':
//		if(!isset($blk['pars']['siteName'])||!is_string($blk['pars']['siteName'])||(trim($blk['pars']['siteName'])=='')){return(setResult(false,'Не указано имя сайта.'));}
		if(!isset($blk['pars']['site']['name'])||!is_string($blk['pars']['site']['name'])||(trim($blk['pars']['site']['name'])=='')){setResult(false,'Не указано имя сайта.');return(_e500($data));}
		if(!isset($data['blk']['pars']['form']['name'])){$data['blk']['pars']['form']['name']='conf';}
		$data[$data['blk']['pars']['form']['name']]=site_load($blk['pars']['site']['name']);
		//= Проверка прав пользователя >
		//Конфигурация доступна для swc.conf:[swc.admin] и site.conf:[site.admin]
		$a=user_isAllow($data[$data['blk']['pars']['form']['name']],'site.admin');
		if(!$a){$a=user_isAllow($_SESSION['swc.cfg'],'swc.admin');}
//		if(!$a){if(isset($data['conf']['site.admin'])&&is_array($data['conf']['site.admin'])){$a=user_isMember($data['conf']['site.admin']);}}
//		if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])&&is_array($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
//		if(!$a){unset($data['conf']);echo(tpl_parse('e403',$data));return(setResult(false,'Недостаточно прав.'));}
		if(!$a){unset($data[$data['blk']['pars']['form']['name']]);setResult(false,'Недостаточно прав.');return(_e403($data));}
		//< Проверка прав пользователя =
		echo(tpl_parse('site:manage.config',$data));
		return(getResult('result'));
	break;
//<< Методы управления сайтами =================================================
	default:
		echo(tpl_parse('e500',array('blk'=>$blk,'content'=>'Метод [SWC.site]:'.$blk['get'].' не определен.')));
		return(setResult(false,'Метод: ['.$blk['mdl'].']:'.$blk['get'].' неопределен.'));
	break;
}
setResult(false,'Ошибка выхода интерфейса.');
_die('Метод [SWC:core.site]:'.$blk['get'].' не вернул управления.');
?>
