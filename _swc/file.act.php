<?php
/**
@title: Интерфейс действия субмодуля работы с файлами и файловой системой.
@package: SWC-6
@subpackage: core.file
@author: Eugeny Leonov <eleonov@leolab.info>
@version: 1.0 <18/03/2011>
*/
if(!defined('htaccess')){die('SWC: Direct access disabled');}
if(!isset($blk)||!is_array($blk)){return(setResult(false,'Отсутствуют данные интерфейса.'));}
if(!isset($blk['act'])||(trim($blk['act'])=='')){return(setResult(false,'Не указан метод интерфейса.'));}

switch($blk['act']){
/**
@title: Создать папку
@version: 1.0.a <18/03/2011>
@param: &path string
@param: &name string

#rev.1 <17/08/2015> Eugeny Leonov
	[*] Обработка вызов функции, возврат через установку результата.
*/
	case 'folder.create':
		if(!isset($blk['pars']['path'])){$blk['pars']['path']='';}else{$blk['pars']['path']=trim($blk['pars']['path'],'/');}
		if(!isset($blk['pars']['name'])||!is_string($blk['pars']['name'])||(trim($blk['pars']['name'])=='')){return(setResult(false,'Не указано наименование папки.'));}
		if(!@mkPath('@D/'.$blk['pars']['path'].'/'.$blk['pars']['name'])){return(setResult(false,'Ошибка удаления папки: '.getResult('reason')));}
		return(setResult(true));
//		return(mkPath('@D/'.$blk['pars']['path'].'/'.$blk['pars']['name']));
//		return(setResult(false,'Метод не реализован.'));
	break;

/**
@title: Удалить папку
@version: 1.0.a <18/03/2011>
@param: &path string
@param: &name string

#rev.1 <17/08/2015> Eugeny Leonov
	[*] Обработка вызова функции, возврат через установку результата.
*/
	case 'folder.remove':
		if(!isset($blk['pars']['name'])||!is_string($blk['pars']['name'])||(trim($blk['pars']['name'])=='')){return(setResult(false,'Не указано наименование папки.'));}
		if(!isset($blk['pars']['path'])){$blk['pars']['path']='';}else{$blk['pars']['path']=trim($blk['pars']['path'],'/');}
//		return(setResult(false,fname('@D/'.$blk['pars']['path'].'/'.$blk['pars']['name'])));
		if(!@rmPath('@D/'.$blk['pars']['path'].'/'.$blk['pars']['name'])){
			return(setResult(false,'Ошибка удаления папки: '.getResult('reason')));
		}else{return(setResult(true));}
//		return();
		return(setResult(false,'Метод не реализован.'));
	break;

/**
@title: Загрузить файл
@version: 1.0.a <18/03/2011>
@param: &name string
@param: &[name] file
*/
	case 'upload':
		if(!isset($blk['pars']['name'])||!is_string($blk['pars']['name'])||(trim($blk['pars']['name'])=='')){return(setResult(false,'Не указано имя поля файла.'));}
		if(!isset($blk['pars']['path'])){$blk['pars']['path']='';}else{$blk['pars']['path']=trim($blk['pars']['path'],'/');}
		return(file_upload('@D/'.$blk['pars']['path'],$blk['pars']['name']));
		return(setResult(false,'Метод не реализован.'));
	break;

/**
@title: Создать файл
@version: 1.0.a <18/03/2011>
@param: &name string
*/
	case 'create':
		return(setResult(false,'Метод не реализован.'));
	break;

/**
@title: Удалить файл
@version: 1.0.a <18/03/2011>
@param: &path string
@param: &name string
*/
	case 'remove':
		if(!isset($blk['pars']['name'])||!is_string($blk['pars']['name'])||(trim($blk['pars']['name'])=='')){return(setResult(false,'Не указано имя файла.'));}
		if(!isset($blk['pars']['path'])||!is_string($blk['pars']['path'])||(trim($blk['pars']['path'])=='')){return(setResult(false,'Не указано хранилище файла.'));}
		if(!file_exists(fname('@D/'.$blk['pars']['path'].'/'.$blk['pars']['name']))){return(setResult(false,'Файл не найден. ('.$blk['pars']['path'].'/'.$blk['pars']['name'].')'));}
		if(!@unlink(fname('@D/'.$blk['pars']['path'].'/'.$blk['pars']['name']))){return(setResult(false,'Ошибка удаления файла.'));}
		return(setResult(true));
	break;

	default:
		return(setResult(false,'Неизвестный метод [swc.file]:'.$blk['act']));
	break;
}

?>
