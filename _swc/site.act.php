<?php
/**
@title: Интерфейс действия поддержки сайтов
@package: SWC-6
@subpackage: core
@author: Eugeny Leonov <eleonov@leolab.info>
@version: 1.0.a <21/01/2011>
*/
if(!defined('htaccess')){die('SWC: Direct access disabled.');}
if(!isset($blk)||!is_array($blk)){return(setResult(false,'Отсутствуют данные интерфейса.'));}
if(!isset($blk['act'])){return(setResult(false,'Не указан метод интерфейса.'));}

switch($blk['act']){
/**
@title: Установить текущий (активный) сайт
@version: 1.0.a <21/01/2011>
@param: site[name]
*/
	case 'select':
		if(!isset($blk['pars']['site']['name'])||(trim($blk['pars']['site']['name'])=='')){return(setResult(false,'Не указана тема.'));}
		return(site_change($blk['pars']['site']['name']));
	break;

/**
@title: Сохранить (обновить) настройки сайта
@version: 1.0.a <23/01/2011>

#rev.2 <20/02/2011> Eugeny Leonov
	[+] Данные конфигурации находятся поле задаваемом параметром &fName
	[+] Проверка корректности параметров.

#rev.1 <19/02/2011> Eugeny Leonov
	[*] Откорректировано определение файла для сохранения конфигурации.
*/
	case 'config.save':
		//= Проверка базовых параметров >
		if(!isset($blk['pars']['fName'])||!is_string($blk['pars']['fName'])||(trim($blk['pars']['fName'])=='')){return(setResult(false,'Не указано поле данных.'));}
		if(!isset($blk['pars'][$blk['pars']['fName']])||!is_array($blk['pars'][$blk['pars']['fName']])){return(setResult(false,'Отсутствуют данные.'));}
		if(!isset($blk['pars'][$blk['pars']['fName']]['name'])||!is_string($blk['pars'][$blk['pars']['fName']]['name'])||(trim($blk['pars'][$blk['pars']['fName']]['name'])=='')){return(setResult(false,'Не указано имя сайта.'));}
		//< Проверка базовых параметров =
		//= Проверка прав пользователя >
//		$a=false;
//		if(isset($_SESSION['swc.site']['site.admin'])&&is_array($_SESSION['swc.site']['site.admin'])){$a=user_isMember($_SESSION['swc.site']['site.admin']);}
//		if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])&&is_array($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}

		if(isset($blk['pars']['fName'])){$nCfg=$blk['pars'][$blk['pars']['fName']];}else{$nCfg=$blk['pars']['conf'];}
		
		$a=user_isAllow($_SESSION['swc.site'],'site.admin');
		if(!$a){$a=user_isAllow($_SESSION['swc.cfg'],'swc.admin');}
		if(!$a){return(setResult(false,'Недостаточно прав'));}
		//< Проверка прав пользоватея =
		if(!isset($blk['pars']['conf'])||!is_array($blk['pars']['conf'])){return(setResult(false,'Отсутствуют данные конфигурации.'));}
		if(!isset($blk['pars']['conf']['name'])||!is_string($blk['pars']['conf']['name'])||(trim($blk['pars']['conf']['name'])=='')){return(setResult(false,'Не указано имя сайта.'));}
		switch($blk['pars']['conf']['name']){
			case '_default':
			case '_blank':
				$blk['pars']['conf']['cfg_name']=fname('@D/conf/site');
			break;
			default:
				$blk['pars']['conf']['cfg_name']=fname('@S/'.$blk['pars']['conf']['name'].'/_conf/site');
			break;
		}
//		if(!isset($blk['pars']['conf']['cfg_name'])){$blk['pars']['conf']['cfg_name']=fname('@S/%site%/_conf/site');}
		//= Проверка обязательных параметров >
		
		//< Проверка обязательных параметров =
		if(!cfg_save($blk['pars']['conf']['cfg_name'],$blk['pars']['conf'])){return(setResult(false,'Ошибка сохранения конфигурации сайта: '.getResult('reason')));}
		return(setResult(true));
	break;

/**
@title: Удалить (сбросить) конфиг сайта
@version: 1.0.a <20/02/2011>
*/
	case 'config.reset':
		if(!isset($blk['pars']['site']['name'])||!is_string($blk['pars']['site']['name'])||(trim($blk['pars']['site']['name'])=='')){return(setResult(false,'Не указано имя сайта.'));}
		$cfg=site_load($blk['pars']['site']['name']);
		if(isset($cfg['dist'])&&is_true($cfg['dist'])){return(setResult(false,'Конфигурация уже сброшена.'));}
		//= Проверяем права текущего пользователя >
		$a=user_isAllow($cfg,'site.admin');
		if(!$a){$a=user_isAllow($_SESSION['swc.cfg'],'swc.admin');}
		if(!$a){return(setResult(false,'Недостаточно прав.'));}
		return(cfg_remove('@S/'.$blk['pars']['site']['name'].'/_conf/site'));
		//< Проверяем права текущего пользователя =
	break;
	default:
		return(setResult(false,'Неизвестный интерфейс метода: [SWC.site]:'.$blk['act']));
	break;
}
?>
