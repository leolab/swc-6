<?php
/**
@title: Файл скриптов шаблонов субмодуля управления пользователями
@package: SWC-6
@subpackage: core.user
@version: 1.0.rc <07/01/2011>
@author: Eugeny Leonov <eleonov@leolab.info>

@deprecated.
*/
if(!defined('htaccess')){die('SWC: Direct access disabled.');}
set_contentType('text/javascript; charset=utf-8');
?>

/**
@title: Удалить пользователя
@package: SWC-6
@subpackage: core.user
@version: 1.0.rc <17/12/2010>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
function swc_user_delete(logName){
	if(confirm('Удалить пользователя "'+$('#swc_user_'+logName+'_title').text()+'"?')){
		$.post('/',{
			'do':'user.remove',
			'logName':logName
		},function(data){
			if(is_ok(data)){
				$('#swc_user_'+logName).remove();
				$.post('/',{
					'get':'user.manage.user.row',
					'logName':logName
				},function(data){
					if(data!=''){
						$('#swc_userList').append(data);
					}
				});
			}else{swc_alert(data);}
		});
	}
}

/**
@title: Изменить пользователя
@package: SWC-6
@subpackage: core.user
@version: 1.0.rc <17/12/2010>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
function swc_userForm_edit(logName){
	$.post('/',{
		'get':'user.manage.edit',
		'logName':logName
	},function(data){
		$('#swc_user_editForm').html(data);
		$('#swc_user_editFormTitle').text('Изменить пользователя ('+logName+'):');
		$('#swc_user_editForm_block').css('display','block');
	});
}

/**
@title: Добавить (создать) пользователя
@package: SWC-6
@subpackage: core.user
@version: 1.0.rc <17/12/2010>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
function swc_userForm_create(){
	$.post('/',{
		'get':'user.manage.edit'
	},function(data){
		$('#swc_user_editForm').html(data);
		$('#swc_user_editFormTitle').text('Добавить пользователя:');
		$('#swc_user_editForm_block').css('display','block');
		$(document).scrollTop($('#swc_user_editFrom_block').offset())
	});
}

/**
@title: Отменить редактирование/создание пользователя
@package: SWC-6
@subpackage: core.user
@version: 1.0.rc <17/12/2010>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
function swc_userForm_cancel(){
	$('#swc_user_editForm').html('');
	$('#swc_user_editForm_block').css('display','none');
}

/**
@title: Изменить пароль пользователя
@package: SWC-6
@subpackage: core.user
@version: 1.0.rc <07/01/2011>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
function swc_userForm_setPassword(logName){
	logWord=prompt('Новый пароль:','');
	if(logWord!=null){
		$.post('/',{
			'do':'user.setPassword',
			'user[logName]':logName,
			'user[logWord]':logWord
		},function(data){
			if(is_ok(data)){
				alert('Пароль изменен.');
			}else{alert(data);}
		});
	}
}

/**
@title: Создать группу пользователей
@package: SWC-6
@subpackage: core.user
@version: 1.0.rc <17/12/2010>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
function swc_userForm_createGroup(){
	$.post('/',{
		'do':'user.group.create',
		'group[name]':$('#newgroup_name').val(),
		'group[title]':$('#newgroup_title').val()
	},function(data){
		if(is_ok(data)){
			$('#swc_conf_userAdd_groups').append('<option value="'+$('#newgroup_name').val()+'">'+$('#newgroup_title').val()+'</option>');
			$('#newgroup_name').val('');
			$('#newgroup_title').val('');
			$('#swc_user_groupNew').css('display','none');
		}else{alert(data);}
	});
}

/**
@title: Добавить пользователя в группу
@package: SWC-6
@subpackage: core.user
@version: 1.0.rc <17/12/2010>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
function swc_userForm_grpAdd(grpName){
	$.post('/',{
		'get':'user.manage.user_group.row',
		'grpName':grpName,
	},function(data){
		$('#swc_conf_userAdd_grpAdd').before(data);
	});
}

/**
@title: Удалить пользователя из группы
@package: SWC-6
@subpackage: core.user
@version: 1.0.rc <17/12/2010>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
function swc_userForm_grpDel(grpName){
	$('#swc_conf_userAdd_group_'+grpName.replace(/\./gi,'_')+'_row').remove();
}
