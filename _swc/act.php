<?php
/**
@title: Интерфейс действия ядра системы
@package: SWC-6
@subpackage: core
@version: 1.0.rc <20/11/2010>

#rev.1 <11/01/2011> Eugeny Leonov
	[+] Проверка прав пользователей ?config_update
*/
if(!defined('htaccess')){die('SWC: Direct access disabled.');}
if(!isset($blk)||!is_array($blk)){return(setResult(false,'Не указаны параметры вызова интерфейса.'));}
if(!isset($blk['act'])||!is_string($blk['act'])){return(setResult(false,'Не указан метод интерфейса.'));}
//_die('<pre>'.print_r($blk,true).'</pre>');
switch($blk['act']){
//== Основные методы ядра системы >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/**
@title: Сохранить текст в файл
@version: 1.0.a <23/02/2011>
*/
	case 'text':
		//= Проверка прав пользователя >
		$a=user_isAllow($_SESSION['swc.site'],'site.manage');
		if(!$a){$a=user_isAllow($_SESSION['swc.cfg'],'swc.manage');}
		if(!$a){return(setResult(false,'Недостаточно прав.'));}
		//< Проверка прав пользователя =
		if(!isset($blk['pars']['fName'])||!is_string($blk['pars']['fName'])||(trim($blk['pars']['fName'])=='')){return(setResult(false,'Не указано имя файла.'));}
		if(!isset($blk['pars']['text'])||!is_string($blk['pars']['text'])){return(setResult(false,'Отсутствуют данные.'));}
	break;
//<< Основные методы ядра системы ==============================================
//== Методы настройки и управления ядра системы >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/**
@title: Установить новое значение параметра конфигурации.
@version: 1.0.b <04/01/2011>
@author: Eugeny Leonov <eleonov@leolab.info>
@param: &field array|string
@param: &value mixed

#rev.2 <26/02/2011> Eugeny Leonov
	[+] Проверка значения временной зоны.

#rev.1 <11/01/2011> Eugeny Leonov
	[+] Реализована проверка прав пользователя.
		Выполнение метода доступно только пользователям входящим в одну из групп указанных в параметре [swc.manage] конфигурации ядра.
	[+] Если не указан параметр конфигурации [swc.manage], в него добавляется группа swc.admin
*/
	case 'config_update':
		if(!isset($_SESSION['swc.cfg']['swc.admin'])||!is_array($_SESSION['swc.cfg']['swc.admin'])){return(setResult(false,'Не определены администраторы ядра системы.'));}
		if(!user_isMember($_SESSION['swc.cfg']['swc.admin'])){return(setResult(false,'Недостаточно прав для выполнения операции.'));}
		if(!isset($blk['pars']['conf'])||!is_array($blk['pars']['conf'])){return(setResult(false,'Отсутствуют данные метода интерфейса.'));}
		$nCfg=&$blk['pars']['conf'];
		$cfg=cfg_load('@D/conf/swc');
		if(!$cfg){$cfg=cfg_load(swc_base.'/_conf/swc');}
		if(!$cfg){$cfg=cfg_load(swc_base.'/swc');}
		//== Проверка параметров ядра >>
		//= Общие значения >
		if(!isset($nCfg['timezone'])||!is_string($nCfg['timezone'])||(trim($nCfg['timezone'])=='')||!date_default_timezone_set($nCfg['timezone'])){$nCfg['timezone']=date_default_timezone_get();}
			//Проверка корректности параметра
			if(!date_default_timezone_set($nCfg['timezone'])){_msg('Неверное значение временной зоны.','W','SWC.core');$nCfg['timezone']=date_default_timezone_get();}
			
		//< Общие значения =
		//= Значения по умолчанию >
		if(!isset($nCfg['default'])||!is_array($nCfg['default'])){if(isset($cfg['default'])){$nCfg['default']=$cfg['default'];}else{$nCfg['default']=array();}}
		if(!isset($nCfg['default']['e404'])){if(isset($cfg['default']['e404'])){$nCfg['default']['e404']=$cfg['default']['e404'];}}
		if(!isset($nCfg['default']['site'])){if(isset($cfg['default']['site'])){$nCfg['default']['site']=$cfg['default']['site'];}}
		if(!isset($nCfg['default']['theme'])){if(isset($cfg['default']['theme'])){$nCfg['default']['theme']=$cfg['default']['theme'];}}
		if(!isset($nCfg['default']['user'])){if(isset($cfg['default']['user'])){$nCfg['default']['user']=$cfg['default']['user'];}}
		//< Значения по умолчанию =
		//= Группы управления ядром системы >
		if(!isset($nCfg['swc.admin'])||!is_array($nCfg['swc.admin'])){if(isset($cfg['swc.admin'])&&is_array($cfg['swc.admin'])){$nCfg['swc.admin']=$cfg['swc.admin'];}else{$nCfg['swc.admin']=array('swc.admin'=>'swc.admin');}}
		if(!isset($nCfg['swc.manage'])||!is_array($nCfg['swc.manage'])){if(isset($cfg['swc.manage'])&&is_array($cfg['swc.manage'])){$nCfg['swc.manage']=$cfg['swc.manage'];}else{$nCfg['swc.manage']=array('swc.admin'=>'swc.admin');}}
		if(!isset($nCfg['user.manage'])||!is_array($nCfg['user.manage'])){if(isset($cfg['user.manage'])&&is_array($cfg['user.manage'])){$nCfg['user.manage']=$cfg['user.manage'];}else{$nCfg['user.manage']['swc.admin']='swc.admin';}}
		//< Группы управления ядром системы =
		//= Режим отладки >
		//$nCfg['dbgEnabled']
		//< Режим отладки =
		//<< Проверка параметров ядра ==
		//== Проверка параметров субмодуля пользователей >>
		//$nCfg['swc.user']['allow_self_registry']
		//<< Проверка параметров субмодуля пользователей ==
		if(!cfg_save('@D/conf/swc',$blk['pars']['conf'])){return(setResult(false,'Ошибка сохранения конфигурации: '.getResult('reason')));}
		if(isset($blk['pars']['config_apply'])&&is_true($blk['pars']['config_apply'])){$_SESSION['swc.cfg']=cfg_load('@D/conf/swc');}
		return(setResult(true));
	break;
//<< Методы настройки и управления ядра системы ================================
	default:
		return(setResult(false,'Неизвестный метод: '.$blk['act']));
	break;
}
?>
