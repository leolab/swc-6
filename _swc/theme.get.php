<?php
/**
@title: Интерфейс данных поддержки тем
@package: SWC
@subpackage: core
@version: 1.0.rc <18/12/2010>
@author: Eugeny Leonov <eleonov@leolab.info>
 
#rev.1 <26/01/2011> Eugeny Leonov
	[f] В шаблоны не передаются параметры блока.
	[+] В параметр передаваемый шаблонам добавлены параметры блока.
*/
if(!defined('htaccess')){die('SWC: Direct access disabled.');}
if(!isset($blk)||!is_array($blk)){return(setResult(false,'Отсутствуют данные для интерфейса.'));}
if(!isset($blk['get'])){return(setResult(false,'Не указан метод интрефейса.'));}
$data['blk']=$blk;

switch($blk['get']){
/**
@title: Получить селект-форму смены темы
@version: 1.0.rc <12/12/2010>
@todo: Реализовать проверку прав текущего пользователя на темы. Не выводить темы которые недоступны пользователю.
*/
	case 'select':
		$data['items']=theme_list();
		echo(tpl_parse('theme:select',$data));
		return(getResult('result'));
	break;

	default:
		echo(tpl_parse('e500',array('blk'=>$blk,'content'=>'Метод ['.$blk['mdl'].']:'.$blk['get'].' не определен. ([SWC]:theme)')));
		return(setResult(false,'Метод: ['.$blk['mdl'].']:'.$blk['get'].' неопределен.'));
	break;
}
?>
