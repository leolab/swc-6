<?php
/**
@title: Файл функций работы с файлами и файловой системой
@package: SWC-6
@subpackage: core
@author: Eugeny Leonov <eleonov@leolab.info>
@version: 1.0.a <18/03/2011>

#rev.1 <17/08/2015> Eugeny Leonov
	[*] rmPath
*/
if(!defined('htaccess')){die('SWC: Direct access disabled');}

/**
@title: Загрузить файлы в папку
@version: 1.0 <19/03/2011>
@param: string
@param: string
*/
function file_upload($path,$name){
	global $_FILES;
//_die('<pre>'.print_r($_FILES,true).'</pre>');
	if(!isset($_FILES[$name])){return(setResult(false,'Отсутствуют файлы для загрузки ('.$name.').'));}
	if(!mkPath($path)){return(setResult(false,'Ошибка создания папки: '.getResult('reason')));}
	setResult(true);
	if(is_array($_FILES[$name]['error'])){
		foreach($_FILES[$name]['error']as$k=>$e){
			if($e==UPLOAD_ERR_OK){
				if(!move_uploaded_file($_FILES[$name]['tmp_name'][$k],fname($path.'/'.$_FILES[$name]['name'][$k]))){setResult(false,'Ошибка загрузки файла: '.$_FILES[$name]['name'][$k]);}
			}
		}
	}else{
		if($_FILES[$name]['error']==UPLOAD_ERR_OK){
		}else{setResult('');}
	}
	return(getResult('result'));
}

/**
@title: Получить расширение файла
@version: 1.0 <19/03/2011>
@param: string
@return: string
*/
function file_ext($fname){
	if(!strpos($fname,'.')){return('');}
	return(trim(substr($fname,strrpos($fname,'.')),'.'));
}

/**
@title: Получить список файлов в папке
@version: 1.0.a <18/03/2011>
@param: string
@return: array(dir,name,ext)|false
*/
function file_list($path){
	$path=fname($path);
	if(!file_exists($path)||!is_dir($path)){return(setResult(false,'Папка не существует.'));}
	$ret=array();
	$dh=opendir($path);
	if(!$dh){return(setResult(false,'Ошибка открытия папки.'));}
	while($fn=readdir($dh)){
		if($fn[0]!='.'){
			if(!is_dir($path.'/'.$fn)){
				$ret[$fn]=array(
					'dir'=>false,
					'name'=>trim(substr($fn,0,strrpos($fn,'.')),'.'),
					'ext'=>trim(substr($fn,strrpos($fn,'.')),'.'),
				);
			}else{
				$ret[$fn]=array('dir'=>true,'name'=>$fn,'ext'=>'');
			}
		}
	}
	return($ret);
}

/**
@title: Создать папку и путь к ней
@version: 1.2.r.1 <03/09/2010>
@param: string
@param: [bool]=true - создать файл /.htaccess запрещающий доступ в создаваемые папки.
@return: bool

#rev.2 <11/01/2011> Eugeny Leonov
	[+] Создание файла правил сервера запрещающего доступ в создаваемую папку.
	[+]	Параметр $hta, при установке его в значение true - создается файл правил.
*/
function mkPath($fPath,$hta=true){
	$fPath=fname($fPath);
	if(!$fPath){return(setResult(false,getResult('reason')));}
	if(file_exists($fPath.'/')&&is_dir($fPath)){return(setResult(true));}
	$ret=false;
	$cf=array($fPath);
	while(!$ret){
		$fPath=rtrim(substr($fPath,0,strrpos($fPath,'/')),'/');
		$ret=file_exists($fPath.'/');
		if($fPath==''){$ret=true;}
		if(!$ret){$cf[]=$fPath;}
	}
	$cf=array_reverse($cf);
	reset($cf);
	foreach($cf as $k=>$v){
		if(!mkDir($v.'/')){return(setResult(false,'Невозможно создать папку: '.$v));}
		if($hta){if(!fsave($v.'/.htaccess',"Order allow,deny\nDeny from all\n")){_msg('Ошибка создания файла правил: '.getResult('reason'),'W','SWC.core-mkPath');}}
		if(defined('swc_folder_mod')){chmod($v.'/',swc_folder_mod);}
	}
	return(setResult(true));
}

/**
@title: Удалить папку с вложенными файлами и папками
@name: rmpath
@version: 1.3.r.1 <03/09/2010>
@param: string
@return: bool
 
#rev.2 <17/08/2015> Eugeny Leonov
	[-] Удалена первая попытка удаления папки.
		Удаление связано с некорректной обработкой ошибки удаления непустой папки.
*/
function rmPath($fPath){
	$fPath=fname($fPath);
	if(!$fPath){return(false);}
	if(!is_dir($fPath)){return(setResult(false,$fPath.' не является папкой.'));}
//	if(@rmDir($fPath)){return(setResult(true));}
	$dh=opendir($fPath);
	if(!$dh){return(setResult(false,'Ошибка открытия папки: '.$fPath));}
	while(($fn=readdir($dh))!=false){
		if(trim($fn,'.')!=''){
			if(is_dir(fname($fPath.'/'.$fn))){
				if(!rmPath($fPath.'/'.$fn)){return(setResult(false,getResult('reason')));}
			}else{
				if(!@unlink($fPath.'/'.$fn)){return(setResult(false,'Невозможно удалить файл: '.$fPath.'/'.$fn));}
			}
		}
	}
	closedir($dh);
	if(!rmDir($fPath)){return(setResult(false,'Ошибка удаления папки: '.$fPath));}
	return(setResult(true));
}

?>
