<?php
/**
@title: Скрипт инициализации ядра системы
@package: SWC
@subpackage: core
@version 1.0.rc <11/01/2011>
*/

//== Определение местанахождения файлов ядра системы >>
$bp=str_replace('\\','/',__FILE__);
$bp=rtrim(substr($bp,0,strrpos($bp,'/')),'/');
//= Поиск файла функций ядра системы >
if(!file_exists($bp.'/_swc/inc.php')){$bp=rtrim(substr($bp,0,strrpos($bp,'/')),'/');}
if(!file_exists($bp.'/_swc/inc.php')){die('SWC.init: SWC.core not found.');}
//< Поиск файла функций ядра системы =
//<< Определение местанахождения файлов ядра системы ==
//= Установка констант >
if(!defined('htaccess')){define('htaccess',false);}
if(!defined('base_path')){define('base_path',$bp);}
if(!defined('swc_base')){define('swc_base',$bp.'/_swc/');}
if(!defined('base_url')){define('base_url',rtrim(str_replace(array('swc_init.php','\\'),array('','/'),$_SERVER['SCRIPT_NAME']),'/'));}
//< Установка констант =
if(!require_once(swc_base.'/inc.php')){die('SWC.init: Error loading SWC.core');}
//= Сброс пароля суперадмина >
$uRoot=user_load('root');
if(!$uRoot){die('SWC.init: System user not found.');}
if(isset($_REQUEST['setWord'])&&is_true($_REQUEST['setWord'])){$uRoot['logWord']=md5('swcadmin');}else{$uRoot['logWord']='';}
if(!cfg_save($uRoot['cfg_name'],$uRoot)){die('SWC.init: Error updating system user.');}
//< Сброс пароля суперадмина =
die('@Ok.');
?>
