<?php
/**
@title: Файл функций работы с темами и шаблонами
@package: SWC-6
@subpackage: core.theme
@version: 1.0.b <09/01/2011>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
if(!defined('htaccess')){die('SWC: Direct access disabled');}

/**
@title: Получить список тем
@version: 1.0.rc <16/15/2010>
@param: array|false
@param: [bool]=true - получение списка установленных тем локально
@return: array|false

@todo: Получение списка тем подходящих под фильтр с сервера обновлений.

#rev.2 <11/01/2011> Eugeny Leonov
	[-] Создание папки-хранилища тем.
	[+]	При отсутствии папки-хранилища возвращается пустой массив, результат устанавливается в false

#rev.1 <09/01/2011> Eugeny Leonov
	[f]	Ошибка открытия папки-хранилища тем, если нет ни одной темы (да и самой папки)
	[+]	Создание папки-хранилища тем при ее отсутствии.
	[+] Создание файла правил запрещающего прямой доступ к папке-хранилищу тем.
*/
function theme_list($f=false,$local=true){
	$ret=false;
	$tPath=fname('@T/');
	if(!file_exists($tPath)||!is_dir($tPath)){ //Папки-хранилища нет, возвращаем пустой массив.
		setResult(false,'Папки хранилища тем не существует.');
		return(array());
	}
	$dh=opendir($tPath);
	if(!$dh){return(setResult(false,'Ошибка открытия хранилища тем.'));}
	while($f=readdir($dh)){
		if((trim($f,'.')!='')&&($f[0]!='.')){
			if(is_dir($tPath.'/'.$f)&&cfg_exists($tPath.'/'.$f.'/theme')){
				$ret[$f]=cfg_load($tPath.'/'.$f.'/theme');
			}
		}
	}
	closedir($dh);
	if(!is_array($ret)){return(setResult(false,'Темы не найдены.'));}
	setResult(true);
	return($ret);
}

/**
@title: Загрузить параметры темы
@version: 1.0.b <14/11/2011>
@param: string
@return: array|false
*/
function theme_load($tName){
	if(!is_string($tName)){return(setResult(false,'Неверный формат параметра.'));}
	if(trim($tName)==''){$tName='_default';}
	if($tName=='_default'){ //Тема по умолчанию
		if(isset($_SESSION['swc.user']['theme'])){$tName=$_SESSION['swc.user']['theme'];}
		elseif(isset($_SESSION['swc.site']['theme'])){$tName=$_SESSION['swc.site']['theme'];}
		elseif(isset($_SESSION['swc.cfg']['default']['theme'])){$tName=$_SESSION['swc.cfg']['default']['theme'];}
		else{$tName='_blank';}
	}
	$ret=false;
	switch($tName){
		case '_blank': //Тема ядра системы
			$ret=cfg_load('@D/conf/theme');
			if(!$ret){$ret=cfg_load(swc_base.'/_conf/theme');}
		break;
		default:
			$ret=cfg_load('@T/'.$tName.'/theme');
		break;
	}
	if(!$ret){return(setResult(false,'Ошибка загрузки параметров темы: '.$tName));}
	setResult(true);return($ret);
}

/**
@title: Изменить текущую тему
@version: 1.0.b <14/11/2011>
@param: [string]
@return: bool

#rev.2 <21/01/2011> Eugeny Leonov
	[f] не установлен параметр сессии [swc.theme]
	[+] проверка наличия у установка параметра сессии [swc.theme]
	[f] ошибочная установка параметра [site] вместо [theme] конфигурации ядра системы.

#rev.1 <20/01/2011> Eugeny Leonov
	[+] Если не указана тема, производится попытка перегрузки текущей. Если текущая тема не определена - производится последовательный поиск темы:
		- в параметрах пользователя
		- в параметрах текущего сайта
		- в параметрах ядра системы по умолчанию
*/
function theme_change($tName=false){
	if(!isset($_SESSION['swc.theme'])){$_SESSION['swc.theme']=false;}
	if(!$tName){
		if(isset($_SESSION['swc.theme']['name'])){$tName=$_SESSION['swc.theme']['name'];}
		elseif(isset($_SESSION['swc.user']['theme'])){$tName=$_SESSION['swc.user']['theme'];}
		elseif(isset($_SESSION['swc.site']['theme'])){$tName=$_SESSION['swc.site']['theme'];}
		elseif(isset($_SESSION['swc.cfg']['default']['theme'])){$tName=$_SESSION['swc.cfg']['default']['theme'];}
		else{$tName='_default';}
	}
	if(!is_string($tName)){return(setResult(false,'Неверный формат параметра.'));}
	$nt=theme_load($tName);
	if(!$nt){$nt=theme_load('_default');}
	if(!$nt){$nt=theme_load('_blank');}
	if(!$nt){return(setResult(false,'Ошибка загрузки темы: ('.$tName.'): '.getResult('reason')));}
	$_SESSION['swc.theme']=$nt;
	$_SESSION['swc.cfg']['theme']=$nt['name'];
	return(setResult(true));
}

/**
@title: Найти файл шаблона
		Файл шаблона может задаваться строкой содержащей модуль: module:tpl_name
@version: 1.0.rc <02/11/2010>
@param: string|array
	Строковой параметр имеет формат: module_name:tpl_name.
		module_name может быть опущен вместе с двоеточием.
	Массив содержит параметры:
		- name
		- mdl
		- blk
	Все параметры кроме name могут быть опущены.
@param: bool
@return: string|false

@todo: Реализовать обработку замещенных модулей
 
#rev.1 <04/02/2011> Eugeny Leonov
	[-] Убрана обработка запрещенных модулей.
*/
function tpl_find($tName,$noResult=false){
	if(is_string($tName)&&strpos($tName,':')){list($tpl['mdl'],$tpl['name'])=explode(':',$tName,2);}elseif(is_string($tName)){$tpl=array('name'=>$tName);}else{$tpl=$tName;}
	if(!isset($tpl['name'])||(trim($tpl['name'])=='')){return(setResult(false,'Не указано имя шаблона.'));}
	if(!isset($tpl['mdl'])){$tpl['mdl']='';}
	if(!isset($tpl['blk'])){$tpl['blk']='';}
	//= Формируем пути поиска >
	$sa=array(
		'@T/%theme%/'.$tpl['mdl'].'/'.$tpl['name'].'.phtml',
		'@S/%site%/_tpl/'.$tpl['mdl'].'/'.$tpl['name'].'.phtml',
		'@S/%site%/_mdl/'.$tpl['mdl'].'/_tpl/'.$tpl['name'].'.phtml',
		'@M/'.$tpl['mdl'].'/_tpl/'.$tpl['name'].'.phtml',
		swc_base.'/_tpl/'.$tpl['mdl'].'/'.$tpl['name'].'.phtml',
		swc_base.'/_mdl/'.$tpl['mdl'].'/_tpl/'.$tpl['name'].'.phtml',
	);
	//< Формируем пути поиска =
	$ret=where_is($sa);
	if(!$ret){
		if($noResult){return(false);}
		return(setResult(false,'Файл шаблона не найден ('.$tpl['name'].').'));
	}else{
		if(!$noResult){setResult(true);}
		return($ret);
	}
}

/**
@title: Загрузить параметры шаблона страницы
@version: 1.0.rc <09/11/2010>
@param: string
@return: array|false
*/
function tpl_load($tName){
	if(!is_string($tName)){return(setResult(false,'Неверный параметр.'));}
	if(!strpos($tName,'.')||(trim(substr($tName,strrpos($tName,'.')),'.')!='pg')){return(setResult(false,'Запрошен не шаблон страницы ('.$tName.').'));}
	//= Поиск шаблона страницы >
	$ret=false;
	if(cfg_exists('@T/%theme%/'.$tName)){$ret=cfg_load('@T/%theme%/'.$tName);}
	if(!$ret){if(cfg_exists('@S/%site%/_tpl/'.$tName)){$ret=cfg_load('@S/%site%/_tpl/'.$tName);}}
	if(!$ret){if(cfg_exists(swc_base.'/_tpl/'.$tName)){$ret=cfg_load(swc_base.'/_tpl/'.$tName);}}
	if(!$ret){return(setResult(false,'Шаблон страницы не найден ('.$tName.')'));}
	if(!isset($ret['tpl.name'])){$ret['tpl.name']=$tName;}
	//< Поиск шаблона страницы =
	setResult(true);return($ret);
}

/**
@title: Обработать данные по шаблону.

@version: 1.0.rc.1 <02/11/2010>
@param: string|array
@param: array
@return: string|false
*/
function tpl_parse($tpl,$data){
	if(is_array($tpl)&&isset($tpl['name'])){$tName=$tpl['name'];}elseif(is_string($tpl)){$tName=$tpl;}else{return(setResult(false,'Не указано имя шаблона.'));}
	//= Проверка данных >
	_msg('Запрошен шаблон: '.$tName,'D','SWC.core-tpl_parse');
	$tFile=tpl_find($tpl);
	if(!$tFile){if(!getResult('result')){
		setResult(false,'Файл шаблона ['.$tName.'] не найден. ['.getResult('reason').']');
		$data['result']=getResult();
		if(tpl_find('e500',false)){
			$data['content']=getResult('reason');
			return(tpl_parse('e500',$data));
		}
		setResult(false,'Файл шаблона ['.$tName.'] не найден. ['.getResult('reason').']');
		css_add('swc_error','.swc_error{font-family:verdana;font-size:14px;color:#fff;}');
		return('<div class="swc_error">'.getResult('reason').'</div>');
	}else{
		setResult(false,'Файл шаблона не найден. ['.$tName.'] <br/>DBG:<pre>'.print_r($data,true).'</pre><pre>'.print_r($tpl,true).'</pre>');
		$data['content']=getResult('reason');
		if(tpl_find('e500',true)){
			$data['result']=getResult();
			return(tpl_parse('e500',$data));
		}
		css_add('swc_error','.swc_error{font-family:verdana;font-size:14px;color:#fff;}');
		return('<div class="swc_error">'.getResult('reason').'</div>');
	}}
	_msg('Обрабатывается шаблон: '.$tFile,'D','SWC.core-tpl_parse');
	ob_start();
	include(fname($tFile));
	$ret=ob_get_clean();
	if(!$ret){
		if(!getResult('result')){return(setResult(false,'Ошибка выполнения шаблона. ['.$tName.'('.$tFile.'):'.getResult('reason').']'));}
//Если шаблон вернул пустой результат, но getResult('result')==true - значит все в порядке, шаблон не должен быть ничего вернуть.
	}
	setResult(true);
	return($ret);
}
?>
