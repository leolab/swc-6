<?php
/**
@title: Интерфейс действия субмодуля управления модулями системы
@package: SWC
@subpackage: core.mdl
@version: 1.0.a <08/01/2011>
@author: Eugeny Leonov <eleonov@leolab.info>
*/
if(!defined('htaccess')){die('SWC: Direct access disabled');}
if(!isset($blk)||!is_array($blk)){return(setResult(false,'Отсутствуют параметры интерфейса'));}
if(!isset($blk['act'])){return(setResult(false,'Не указан метод интерфейса'));}

switch($blk['act']){

	default:
		return(setResult(false,'Неизвестный метод интрефейса: [SWC.mdl]:'.$blk['act']));
	break;
}
?>
