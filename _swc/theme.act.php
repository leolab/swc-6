<?php
/**
@title: Интерфейс действия поддержки тем
@package: SWC-6
@subpackage: core
@version: 1.0.rc <20/12/2010>

#Rev.1 <24/12/2010>
	[f]	При попытке установить тему в значение "_default" устанавливалась тема ядра по умолчанию.
*/
if(!defined('htaccess')){die('SWC: Direct access disabled.');}
if(!isset($blk)||!is_array($blk)){return(setResult(false,'Отсутствуют данные интерфейса.'));}
if(!isset($blk['act'])){return(setResult(false,'Не указан метод интерфейса.'));}

switch($blk['act']){
/**
@title: Установить указанную тему текущей
@version: 1.0.a.1 <29/02/2011>
@param: &theme[name]
@todo: Проверка прав текущего пользователя на устанавливаемую тему.
*/
	case 'select':
		if(!isset($blk['pars']['theme']['name'])||(trim($blk['pars']['theme']['name'])=='')){return(setResult(false,'Не указана тема.'));}
		return(theme_change($blk['pars']['theme']['name']));
	/*
		if(cfg_exists('@T/'.$blk['pars']['theme']['name'].'/theme')){
			$_SESSION['swc.theme']=cfg_load('@T/'.$blk['pars']['theme']['name'].'/theme');
			if(isset($_SESSION['swc.cfg']['theme'])){unset($_SESSION['swc.cfg']['theme']);}
			return(setResult(true));
		}elseif($blk['pars']['theme']['name']=='_default'){ //Установка темы по умолчанию.
			unset($_SESSION['swc.theme']);
//			$_SESSION['swc.theme']['name']='_default';
			if(isset($_SESSION['swc.cfg']['theme'])){unset($_SESSION['swc.cfg']['theme']);}
			_msg('Выбрана тема по умолчанию.','D','SWC.core');
			return(setResult(true));
		}elseif($blk['pars']['theme']['name']=='_blank'){ //Установка "пустой" темы
			$_SESSION['swc.theme']['name']='_blank';
			if(isset($_SESSION['swc.cfg'])){unset($_SESSION['swc.cfg']['theme']);}
			return(setResult(true));
		}else{
			return(setResult(false,'Запрошенная тема не найдена: '.$blk['pars']['theme']['name']));
		}
	*/
	break;
	default:
		return(setResult(false,'Неизвестный интерфейс метода: [swc]:theme.'.$blk['act']));
	break;
}
?>
