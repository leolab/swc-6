<?php
/**
@title: Интерфейс данных субмодуля работы с файлами и файловой системой
@package: SWC-6
@subpackage: core.file
@author: Eugeny Leonov <eleonov@leolab.info>
@version: 1.0.a <18/03/2011>
*/
if(!defined('htaccess')){die('SWC: Direct access disabled.');}

if(!isset($blk)||!is_array($blk)){return(setResult(false,'Отсутствуют параметры модуля.'));}
if(!isset($blk['get'])||(trim($blk['get']==''))){return(setResult(false,'Не указан интерфейс модуля.'));}
$data['blk']=$blk;

switch($blk['get']){

/**
@title: Получить список файлов в указанной папке
@version: 1.0.a <18/03/2011>
@param: &path string
*/
	case 'list':
		if(!isset($blk['pars']['path'])){$blk['pars']['path']='/';}
		$blk['pars']['path']=str_replace('//','/',$blk['pars']['path']);
		$data['cPath']=trim($blk['pars']['path'],'/');
		$data['path']=explode('/',$blk['pars']['path']);
/*
		if(strpos($blk['pars']['path'],'/')){
			$cp=trim($blk['pars']['path'],'/');
			while($cp!=''){
				$data['path'][]=$cp;
				$cp=trim(substr($cp,0,strrpos($cp,'/')),'/');
			}
		}
*/
		$data['list']=file_list('@D/'.$blk['pars']['path']);
		echo(tpl_parse('file:list',$data));
		return(getResult('result'));
	break;

	default:
		echo(tpl_parse('e500',array('blk'=>$blk,'content'=>'Метод ['.$blk['mdl'].']:'.$blk['get'].' не определен.')));
		return(setResult(false,'Метод: ['.$blk['mdl'].']:'.$blk['get'].' неопределен.'));
	break;
}
setResult(false,'Ошибка выхода интерфейса.');
_die('Метод [SWC:core.file]:'.$blk['get'].' не вернул управления.');
?>
