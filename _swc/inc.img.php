<?php
/**
@title: Функции работы с изображениями
@package: SWC-6
@subpackage: core
@version: 1.0.a <07/12/2010>
 */
if(!defined('htaccess')){die('SWC: Direct access disabled.');}

/**
@title: Изменить размер изображения с сохранением пропорций
@author: Eugeny Leonov <eleonov@leolab.info>
@version: 1.0.a <07/12/2010>
@param: gdimage-resource
@param: int
@param: int
@return: gdimage-resource|false
 */
function img_resize($src,$nw,$nh){
	if(!$src){return(setResult(false,'Неверный формат изображения'));}
	$x=imagesx($src);
	$y=imagesy($src);
	if(!$x||!$y){return(setResult(false,'Ошибка получения данных изображения.'));}
	$k1=$nw/$x;
	$k2=$nh/$y;
	$k=$k1>$k2?$k2:$k1;
	$w=intval($x*$k);
	$h=intval($y*$k);
	$ret=imagecreatetruecolor($w,$h);
	if(!imagecopyresampled($ret,$src,0,0,0,0,$w,$h,$x,$y)){return(setResult(false,'Ошибка преобразования изображения.'));}
	setResult(true);
	return($ret);
}

/**
@title: Загрузить изображение в зависимости от типа
@author: Eugeny Leonov <eleonov@leolab.info>
@version: 1.0.a <07/12/2010>
 */
function img_load($iFile){
	if(!file_exists(fname($iFile))){return(setResult(false,'Запрошенный файл не найден: '.$iFile));}
	$ret=imagecreatefromstring(file_get_contents(fname($iFile)));
	if($ret!==false){setResult(true);return($ret);
	}else{return(setResult(false,'Ошибка загрузки файла или неподдерживаемый тип. ('.$iFile.')'));}
}

?>
