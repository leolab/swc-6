<?php
/**
@title: Интерфейс данных подсистемы аутентификации и управления пользователями ядра системы.
@package: SWC-6
@subpackage: core.user
@version: 1.0.a <18/12/2010>
@author: Eugeny Leonov <eleonov@leolab.info>

#rev.2 <15/02/2011> Eugeny Leonov
	[-] Проверка старых обработчиков.

#rev.1 <19/01/2011> Eugeny Leonov
	[*] Контрольная ревизия, описание изменений см. в описаниях методов.
*/
if(!defined('htaccess')){die('SWC: Direct access disabled.');}
if(!isset($blk)||!is_array($blk)){return(setResult(false,'Отсутствуют данные интерфейса. [swc]:user'));}
if(!isset($blk['get'])){return(setResult(false,'Не указан метод интерфейса. [swc]:user'));}

$data['blk']=$blk;

switch($blk['get']){
//== Общие методы >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/**
@title: Форма регистрации пользователя
@version: 1.0.a <07/01/2011>
*/
	case 'registry':
		echo(tpl_parse('user:frm.registry',$data));
		return(getResult('result'));
	break;

/**
@title: Форма авторизации или информации о пользователе
@version: 1.0.rc <15/12/2010>

#rev.1 <14/02/2011> Eugeny Leonov
	[+] Обработка параметра [swc.user]allow_self_registry конфигурации ядра системы
*/
	case 'form':
		$data['user']=$_SESSION['swc.user'];
		if(isset($_SESSION['swc.cfg']['swc.user']['allow_self_registry'])){$data['pars']['allow_self_registry']=is_true($_SESSION['swc.cfg']['swc.user']['allow_self_registry']);}
		if(user_isGuest()){echo(tpl_parse('user:frm.logon',$data));}else{echo(tpl_parse('user:frm.info',$data));}
		return(getResult('result'));
	break;

/**
@title: Получить строку пользователя
@version: 1.0.a <06/01/2011>
@param: &logName string

#rev.3 <02/02/2011> Eugeny Leonov
	[!] Именование полей параметров приведено к текущему стандарту.
	[*] Если пользователь не найден - выводится строка @Error: User not found.
	[*] Строка показывается всем пользователям, обработчики сбрасываются для пользователей не входящих в группы менеджеров пользователей или администраторов системы.

#rev.2 <29/01/2011> Eugeny Leonov
	[*] Переработана проверка прав пользователей.
		Метод доступен пользователям входящим в группы управления пользователями или адиминистрирования ядра системы.

#rev.1 <19/01/2011> Eugeny Leonov
	[+] Проверка прав пользователя.

@todo: Обработчики полей управления пользователем должны передаваться в шаблон только если текущий пользователь имеет право управления пользователями.
*/
	case 'row':
		//= Проверка прав пользователя >
		if(isset($data['blk']['pars']['jsh']['onRemove'])){
			$a=false;
			if(isset($_SESSION['swc.cfg']['user.manage'])){$a=user_isMember($_SESSION['swc.cfg']['user.manage']);}
			if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
			if(!$a){unset($data['blk']['pars']['jsh']['onRemove']);}
			unset($a);
		}
		if(isset($data['blk']['pars']['jsh']['onEdit'])){
			$a=false;
			if(isset($_SESSION['swc.cfg']['user.manage'])){$a=user_isMember($_SESSION['swc.cfg']['user.manage']);}
			if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
			if(!$a){unset($data['blk']['pars']['jsh']['onEdit']);}
			unset($a);
		}
		//< Проверка прав пользователя =
/*
		$a=false;
		if(isset($_SESSION['swc.cfg']['user.manage'])&&is_array($_SESSION['swc.cfg']['user.manage'])){$a=user_isMember($_SESSION['swc.cfg']['user.manage']);}
		if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])&&is_array($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
		if(!$a){setResult(false,'Недостаточно прав.');return(_e403($data));}
*/
/*
		if(!isset($blk['pars']['logName'])||!is_string($blk['pars']['logName'])||(trim($blk['pars']['logName'])=='')){return(setResult(false,'Не указан логин пользователя.'));}
		$data['user']=user_load($blk['pars']['logName']);
*/
		if(!isset($data['blk']['pars']['user']['logName'])){setResult(false,'Не указан пользователь.');return(_e500($data));}
		$data['user']=user_load($data['blk']['pars']['user']['logName']);
		if(!$data['user']){echo('@Error: User not found.');}else{
			if(isset($data['user']['sys'])&&is_true($data['user']['sys'])){if(isset($data['blk']['pars']['jsh']['onRemove'])){unset($data['blk']['pars']['jsh']['onRemove']);}}
			echo(tpl_parse('user:row',$data));
		}
		return(getResult('result'));
	break;

/**
@title: Получить форму выбора пользователей
@version: 1.0.a <19/01/2011>

@todo: Реализовать проверку прав текущего пользователя. Пользователь не входящий в группу управления пользователями не должен иметь доступа к элементам управления
*/
	case 'select':
		$data['items']=user_list(true);
		echo(tpl_parse('user:select',$data));
		return(getResult('result'));
	break;

/**
@title: Информация о пользователе
@version: 1.0.a <16/12/2010>
@param: &user[logName]
*/
	case 'info':
		if(!isset($blk['pars']['user']['logName'])||(trim($blk['pars']['user']['logName'])=='')){return(setResult(false,'Не указан логин пользователя.'));}
		$data['user']=user_load($blk['pars']['user']['logName']);
		echo(tpl_parse('user:info',$data));
		return(getResult('result'));
	break;


/**
@title: Селектор (список выбора) групп пользователей определенных в системе
@version: 1.0.a <12/01/2011>

#rev.3 <01/02/2011> Eugeny Leonov
	[*] Изменена проверка права пользователя на создание новой группы.
		Новую группу может создавать пользователь входящий в группы управления пользователями или администраторов ядра системы

#rev.2 <29/01/2011> Eugeny Leonov
	[f] В селекторе групп отсутствуют имена групп
	[+] Для вывода имен групп в функцию выбора спика групп передается параметр включающий загрузку данных групп

#rev.1 <25/01/2011> Eugeny Leonov
	[+] Реализована проверка прав пользователей.
		Право создания новой группы имеют пользователи входящие в группы указанные в разделе [user.manage] конфигурации ядра системы.
*/
	case 'group.select':
		if(isset($data['blk']['pars']['acl']['create'])&&is_true($data['blk']['pars']['acl']['create'])){
			$a=false;
			if(isset($_SESSION['swc.cfg']['user.manage'])){$a=user_isMember($_SESSION['swc.cfg']['user.manage']);}
			if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
			if(!$a){unset($data['blk']['pars']['acl']['create']);}
		}
		$data['items']=user_group_list(true);
		echo(tpl_parse('user:group.select',$data));
		return(getResult('result'));
	break;

/**
@title: Строка списка групп
	[!] Метод не производит проверку прав пользователя
	Параметры метода зависят от шаблона.

@version: 1.0.a <12/01/2011>
@param: &group[name]
@param: &jsh[onDelete;onRemove;onEdit]

#rev.6 <02/02/2011> Eugeny Leonov
	[f] Неверные имена полей имен обработчиков
	[*] При запросе несуществующей группы возвращается строка: @Error: Group not found.

#rev.5 <01/02/2011> Eugeny Leonov
	[f] Ошибка при попытке выбора несуществующей группы.

#rev.4 <31/01/2011> Eugeny Leonov
	[-] Убрана заглушка для перехода на новый стандарт именования параметров.

#rev.3 <29/01/2011> Eugeny Leonov
	[+] Проверка прав текущего пользователя
		Проверяются права для установленных обработчиков jsh[onRemove] и jsh[onEdit]
		Обработчики доступны только пользователям входящим в группы управления пользователями (swc.cfg[user.manage]) и администраторам системы swc.cfg[swc.admin]
	[!] Параметр grpName переименован в group[name]
 
#rev.2 <25/01/2011> Eugeny Leonov
 	[*] Наименования обработчиков передаются в параметре [jsh][]
 
#rev.1 <22/01/2011> Eugeny Leonov
	[f] Если запрошенная группа не найдена (не существует) выводимая строка не содержит элементов управления.
	[*] Если запрошенная группа не найдена шаблону передается массив описания группы с единственным параметром [name] содержащим переданное имя группы.

@todo: Проверка прав пользователя.
@todo: Если пользователь не имеет права управления группами - в шаблон не должны передаваться обработчики элементов управления.
@todo: Возвращение пустого значения при ошибке и установленном флаге &silent={true}
*/
	case 'group.row':
		//= Проверка прав пользователя >
		if(isset($data['blk']['pars']['jsh']['onRemove'])){//Запрошено удаление группы
			$a=false;
			if(isset($_SESSION['swc.cfg']['user.manage'])&&is_array($_SESSION['swc.cfg']['user.manage'])){$a=user_isMember($_SESSION['swc.cfg']['user.manage']);}
			if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])&&is_array($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
			if(!$a){unset($data['blk']['pars']['jsh']['onRemove']);}unset($a);
		}
		if(isset($data['blk']['pars']['jsh']['onEdit'])){//Запрошено редактирование группы
			$a=false;
			if(isset($_SESSION['swc.cfg']['user.manage'])&&is_array($_SESSION['swc.cfg']['user.manage'])){$a=user_isMember($_SESSION['swc.cfg']['user.manage']);}
			if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])&&is_array($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
			if(!$a){unset($data['blk']['pars']['jsh']['onEdit']);}unset($a);
		}
		//< Проверка прав пользователя =
		if(!isset($blk['pars']['group']['name'])||!is_string($blk['pars']['group']['name'])||(trim($blk['pars']['group']['name'])=='')){return(setResult(false,'Не указана группа.'));}
		$data['group']=user_group_load($blk['pars']['group']['name']);
		if(!$data['group']){echo('@Error: Group not found.');return(getResult('result'));}
//		if(!$data['group']||!is_array($data['group'])){$data['group']=array('name'=>$blk['pars']['group']['name']);}
		//= Проверка обработчиков >
		//Системную группу удалять нельзя.
		if(isset($data['blk']['pars']['jsh']['onRemove'])){if(isset($data['group']['sys'])&&is_true($data['group']['sys'])){unset($data['blk']['pars']['jsh']['onRemove']);}}
		//< Проверка обработчиков =
		echo(tpl_parse('user:group.row',$data));
		return(getResult('result'));
	break;

/**
@title: Форма управления списком групп
@version: 1.0.a <07/02/2011>
*/
	case 'group.manageFrm':
		echo(tpl_parse('user:group.manageFrm',$data));
		return(getResult('result'));
	break;

//<< Общие методы ==============================================================
//== Методы управления пользователями и группами >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/**
@title: Управление пользователями
@version: 1.0.a <17/12/2010>

@todo: Параметры фильтрации и постраничного вывода списка пользователей.
 
#rev.3 <01/02/2011> Eugeny Leonov
 	[*] Откорректирована проверка прав пользователя.

#rev.2 <19/01/2011> Eugeny Leonov
	[f] Метод доступен только пользователям входящим в группы указанные в разделе [user.manage] конфигурации системы.
	[-] Убрано редактирование отдельного пользователя.

#rev.1 <14/01/2011> Eugeny Leonov
	[-] Режим редактирования пользователя.
	[+] Проверка прав пользователя.
		Метод доступен пользователям входящим в группы указанные в разделе [user.manage] конфигурации ядра системы.
	[!] Если раздел [user.manage] конфигурации ядра системы не указан (или не содержит групп) - метод доступен всем пользователям.
*/
	case 'manage':
		//= Проверка прав пользователя >
		$a=false;
		if(isset($_SESSION['swc.cfg']['user.manage'])){$a=user_isMember($_SESSION['swc.cfg']['user.manage']);}
		if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
		if(!$a){setResult(false,'Недостаточно прав.');return(_e403($data));}
		//< Проверка прав пользователя =
		$data['users']=user_list();
		echo(tpl_parse('user:manage',$data));
		return(getResult('result'));
	break;

/**
@title: Форма изменения пользователя
@version: 1.0.a <01/02/2011>
@param: &user[logName]

#rev.2 <01/02/2011> Eugeny Leonov
 	[*] Откорректирована проверка прав пользователя.
 	[!] Параметр - логин пользователя должен быть в массиве user

#rev.1 <19/01/2011> Eugeny Leonov
	[+] Проверка прав пользователя.
*/
	case 'edit':
		//= Проверка прав пользователя >
		$a=false;
		if(isset($_SESSION['swc.cfg']['user.manage'])){$a=user_isMember($_SESSION['swc.cfg']['user.manage']);}
		if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
		if(!$a){setResult(false,'Недостаточно прав.');return(_e403($data));}
		//< Проверка прав пользователя =
		if(!isset($data['blk']['pars']['user']['logName'])||!is_string($data['blk']['pars']['user']['logName'])||(trim($data['blk']['pars']['user']['logName'])=='')){setResult(false,'Не указан пользователь');return(_e500($data));}
		$data['user']=user_load($data['blk']['pars']['user']['logName']);
		echo(tpl_parse('user:edit',$data));
		return(getResult('result'));
	break;

/**
@title: Форма создания пользователя
@version: 1.0.a <01/02/2011>
*/
	case 'create':
		$a=false;
		if(isset($_SESSION['swc.cfg']['user.manage'])){$a=user_isMember($_SESSION['swc.cfg']['user.manage']);}
		if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
		if(!$a){if(isset($data['blk']['pars']['user']['logName'])&&($data['blk']['pars']['user']['logName']==$_SESSION['swc.user']['logName'])){$a=true;}}
		if(!$a){setResult(false,'Недостаточно прав.');return(_e403($data));}
		$data['groups']=user_group_list();
		echo(tpl_parse('user:create',$data));
		return(getResult('result'));
	break;

/**
@title: Метод управления группами
@version: 1.0.a <29/01/2011>
*/
	case 'group.manage':
		//= Проверка прав текущего пользователя >
		$a=false;
		if(isset($_SESSION['swc.cfg']['user.manage'])&&is_array($_SESSION['swc.cfg']['user.manage'])){$a=user_isMember($_SESSION['swc.cfg']['user.manage']);}
		if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])&&is_array($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
		if(!$a){setResult(false,'Недостаточно прав.');return(_e403($data));}
		//< Проверка прав текущего пользователя =
		$data['items']=user_group_list();
		echo(tpl_parse('user:group.manage',$data));
		return(getResult('result'));
	break;

/**
@title: Форма создания/редактирования группы
@version: 1.0.a <17/01/2011>
*/
	case 'group.edit':
		if(!isset($blk['pars']['group']['name'])||!is_string($blk['pars']['group']['name'])||(trim($blk['pars']['group']['name'])=='')){setResult(false,'Не указано имя группы.');return(_e500());}
	case 'group.create':
		//= Проверка прав пользователя >
		$a=false;
		if(isset($_SESSION['swc.cfg']['user.manage'])&&is_array($_SESSION['swc.cfg']['user.manage'])){$a=user_isMember($_SESSION['swc.cfg']['user.manage']);}
		if(!$a){if(isset($_SESSION['swc.cfg']['swc.admin'])&&is_array($_SESSION['swc.cfg']['swc.admin'])){$a=user_isMember($_SESSION['swc.cfg']['swc.admin']);}}
		if(!$a){setResult(false,'Недостаточно прав.');return(_e403($data));}
		//< Проверка прав пользователя =
		echo(tpl_parse('user:group.add',$data));
		return(getResult('result'));
	break;

	default:
		return(setResult(false,'Неизвестный метод интерфейса [swc.user]:'.$blk['get']));
	break;
}
_die('Метод интерфейса [SWC:core.user].'.$blk['get'].' не вернул результат.');
?>
