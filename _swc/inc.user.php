<?php
/**
@title: Файл функций ядра системы (функции аутентификации и управления пользователями)
@package: SWC-6
@subpackage: core.user
@author: Eugeny Leonov <eleonov@leolab.info>
@version: 1.0.b.3 <17/08/2015>

@todo: Реализовать функцию user_group_check()
	Функция должна проверять корректность параметров указанной группы.

#rev.3 <17/08/2015> Eugeny Leonov
	[f->user_create]

#rev.2 <28/01/2011> Eugeny Leonov
	[-] Из функций окончательно убраны попытки обработки методами класса user
	[r] Бета-релиз.

#rev.1 <23/01/2011> Eugeny Leonov
	[*] Общая ревизия субмодуля, подготовка к beta-версии.
	[!] Из функций убраны попытки обработки методами класса $user
*/
if(!defined('htaccess')){die('SWC: Direct access disabled.');}

//== Общие функции >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/**
* Текущий пользователь - гость?
* @version 1.0.b.2 <06/01/2011>
* @param bool - пытаться обработать запрос классом модуля или собственными средствами
* @return bool
*
* rev.2 <23/01/2011> Eugeny Leonov
*	[-] убрана попытка обработки методом user::isGuest()
* 
* #rev.1 <04/01/2011> Eugeny Leonov
* 	[*] Исправлена ошибка: При установленном пустом значении пользователя по умолчанию конфигурации ядра системы, пользователь по умолчанию не считался гостевой учетной записью.
*/
function user_isGuest($self=false){
//	if(!$self){$usr=mdl_load('user');if(is_object($usr)&&method_exists($usr,'isGuest')){return($usr->isGuest());}}
	if(isset($_SESSION['swc.user']['logName'])){
		if(isset($_SESSION['swc.cfg']['default']['user'])&&(trim($_SESSION['swc.cfg']['default']['user'])!='')){return($_SESSION['swc.cfg']['default']['user']==$_SESSION['swc.user']['logName']);}
		elseif($_SESSION['swc.user']['logName']=='_default'){return(true);}
	}else{_msg('Параметры текущего пользователя не загружены.','D','SWC.core');return(true);}
}

/**
* Входит ли текущий пользователь в группу
* 	Параметр $groups может быть непосредственно списком групп по которому производится проверка
* 	Если параметр $field содержит строковое значение, проверка производится по элементу массива $groups заданным этим параметром. Если элемент не найден - возвращается значение true
* @version 1.0.b.1 <08/01/2011>
* @param string|array - список групп в которых производится проверка. Может задаваться как массивом значений, так и списком групп разделенных запятыми.
* @param [string]=false
* @return bool
*
* #rev.2 <23/01/2011>
*	[-] Убрана попытка обработки методом user::isMember()
*
* @check: Проверить работу функции при задании списка строкой.
*
*/
function user_isMember($groups,$field=false){
//	if(!$self){$usr=mdl_load('user');if(is_object($usr)&&method_exists($usr,'isMember')){return($usr->isMember($grpName));}}
	if(is_string($field)){
		if(!isset($groups[$field])){return(setResult(false,'Не найден список групп для проверки ('.$field.')'));}
		$groups=$groups[$field];
	}
	if(is_string($groups)){if(strpos($groups,',')){$groups=explode(',',$groups);}else{$groups=array($groups);}}
	if(!is_array($groups)){return(setResult(false,'Неверный параметр.'));}
	if(!isset($_SESSION['swc.user']['groups'])||!is_array($_SESSION['swc.user']['groups'])){$_SESSION['swc.user']['groups']=array();}
	setResult(true);
	return(aval_inArray($groups,$_SESSION['swc.user']['groups']));
}

/**
* Проверить разрешение пользователя.
* 	Функция возвращает false в случае:
* 		присутствия одной из групп в которые входит пользователь в разделе [grp.deny] переданного параметра
* 		наличия раздела [grp.allow] но не содержащего ни одной группы в которые входит пользователь.
* 	Во всех остальных случаях функция возвращает значение true
* @param array
* @return bool
* 
* ! Использование функции рекомендуется с большой осторожностью !
*/
function user_isAllow($arr){
	if(!is_array($arr)){return(true);}
	if(isset($arr['grp.deny'])){if(user_isMember($arr['grp.deny'])){return(false);}}
	if(!isset($arr['grp.allow'])){return(true);}
	return(user_isMember($arr['grp.allow']));
}

/**
* Существует ли пользователь?
* @version 1.0.rc <06/01/2011>
* @param string
* @param bool
* @return bool
* 
* #rev.1 <28/01/2011> Eugeny Leonov
* 	[-] Убрана попытка обработки методом класса user
*/
function user_exists($logName,$self=false){
//	if(!$self){$usr=mdl_load('user');if(is_object($usr)&&method_exists($usr,'exists')){return($usr->exists($logName));}}
	if(cfg_exists('@U/'.$logName)){return(true);}
	elseif(cfg_exists(swc_base.'/_user/'.$logName)){return(true);}
	else{return(false);}
}

/**
* Авторизовать пользователя
* @version 1.0.rc <18/11/2010>
* @param string
* @param string
* @param [bool]=false
* @return bool
*
* #rev.2 <28/01/2011> Eugeny Leonov
* 	[-] Убрана попытка обработки методом класса user
* #rev.1 <11/01/2011> Eugeny Leonov
*	[-] Авто-создание пароля для пользователя у которого не указан пароль.
*	[*] Параметры нового текущего пользователя загружаются функцией user_load()
*/
function user_logOn($logName,$logWord,$self=false){
//	if(!$self){$usr=mdl_load('user');if(is_object($usr)&&method_exists($usr,'logOn')){return($usr->logOn($logName,$logWord));}}
	$usr=false;
	if(cfg_exists('@U/'.$logName)){$usr=cfg_load('@U/'.$logName);}
	elseif(cfg_exists(swc_base.'/_user/'.$logName)){$usr=cfg_load(swc_base.'/_user/'.$logName);}
	if(!$usr){return(setResult(false,'User not found.'));}
	if(isset($usr['logWord'])&&($usr['logWord']!=='')){
		if($usr['logWord']!==md5($logWord)){return(setResult(false,'Неверный пароль.'));}
	}else{
		return(setResult(false,'Невозможно авторизоваться системным пользователем.'));
	}
	$_SESSION['swc.user']=user_load($logName);
	if(isset($_SESSION['swc.user']['site'])){if(!site_change($_SESSION['swc.user']['site'])){_die('Ошибка изменения текущего сайта.');}}
	if(isset($_SESSION['swc.user']['theme'])){if(!theme_change($_SESSION['swc.user']['theme'])){_die('Ошибка изменения текущего сайта.');}}
	if(isset($_SESSION['swc.user']['ref'])){$GLOBALS['swc.req']['ref']=$_SESSION['swc.user']['ref'];} //Для возврата на страницу указанную в конфиге пользователя.
	return(setResult(true));
}

/**
* Завершить работу текущего пользователя
* @version 1.0.rc <06/01/2011>
* @param [bool]=false
* 
* #rev.1 <28/01/2011> Eugeny Leonov
* 	[-] Убрана попытка обработки методом класса user
*/
function user_logOff($self=false){
//	if(!$self){$usr=mdl_load('user');if(is_object($usr)&&method_exists($usr,'logOff')){return($usr->logOff());}}
//= Загружаем пользователя по умолчанию >
	if(!isset($_SESSION['swc.cfg']['default']['user'])){_die('Не указан пользователь по умолчанию.');}
	$_SESSION['swc.user']=user_load($_SESSION['swc.cfg']['default']['user']);
	if(!$_SESSION['swc.user']||!is_array($_SESSION['swc.user'])){_die('Ошибка завершения работы пользователя: '.getResult('reason'));}
//< Загружаем пользователя по умолчанию =
	$_SESSION['swc.cfg']['user']=$_SESSION['swc.user']['logName'];
//Так-же сбрасываем сайт и тему.
	if(isset($_SESSION['swc.user']['site'])){if(!site_change($_SESSION['swc.user']['site'])){_die('Ошибка изменения текущего сайта.');}}
	if(isset($_SESSION['swc.user']['theme'])){if(!theme_change()){_die('Ошибка изменения текущей темы.');}}
	return(setResult(true));
}

/**
* Загрузить параметры пользователя
* @version 1.0.rc <16/12/2010>
* @param string
* @return array|false
* 
* #rev.2 <02/02/2011> Eugeny Leonov
* 	[*] Небольшие корректировки
*
* #rev.1 <28/01/2011> Eugeny Leonov
* 	[-] Убрана попытка обработки методом класса user
*/
function user_load($logName,$self=false){
//	if(!$self){$usr=mdl_load('user');if(is_object($usr)&&method_exists($usr,'load')){return($usr->load($logName));}}
	$usr=false;
	if(cfg_exists('@U/'.$logName)){$usr=cfg_load('@U/'.$logName);}
	elseif(cfg_exists(swc_base.'/_user/'.$logName)){$usr=cfg_load(swc_base.'/_user/'.$logName);$usr['sys']=true;}
	if(!$usr){return(setResult(false,'User not found.'));}
	if(!isset($usr['groups'])||!is_array($usr['groups'])){$usr['groups']=array();}
	setResult(true);
	return($usr);
}

/**
* Получить список пользователей определенных в системе
* @version 1.0.rc <16/12/2010>
* @param [bool]=false
* @param [bool]
* @return array|false
* 
* #rev.2 <30/01/2011> Eugeny Leonov
* 	[+] Добавлен параметр включающий загрузку параметров пользователей.
* 
* #rev.1 <28/01/2011> Eugeny Leonov
* 	[-] Убрана попытка обработки методом класса user
* 	[!] Функция возвращает только список логинов пользователей, без загрузки их параметров.
*/
function user_list($f=false,$self=false){
//	if(!$self){$usr=mdl_load('user');if(is_object($usr)&&method_exists($usr,'list')){return($usr->list($f));}}
	//Получаем список "ядерных" пользователей
	$ret=cfg_list(swc_base.'/_user/');
//	if(is_array($ds)){foreach($ds as $k=>$v){$ret[$k]=cfg_load(swc_base.'/_user/'.$k);$ret[$k]['sys']=1;}}
	if(!is_array($ret)){$ret=array();}
	//Получаем общих пользователей. Общие пользователи могут перекрывать "ядерных".
	$ds=cfg_list('@U/');
	if(is_array($ds)){foreach($ds as $k=>$v){$ret[$k]=$v;}}
	if($f){foreach($ret as $k=>$v){$ret[$k]=user_load($k);}}
	setResult(true);
	return($ret);
}

//<< Общие функции =============================================================
//== Функции управления пользователями >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/**
Создать учетную запись пользователя
@name: user_create
@version: 1.0.rc.4 <17/08/2015>
@param: array
@param: [bool]=false
@return: bool
@note: После создания пользователя рекомендуется выполнять функцию user_check()

@todo: Проверка параметров по описанию параметров

#rev.4 <17/08/2015> Eugeny Leonov
	[f] Ошибочный вызов функции user_load. В параметр передавался массив данных нового пользователя вместо логина.

#rev.3 <01/02/2011> Eugeny Leonov
	[f] в список групп добавляется хеш вместо логина пользователя

#rev.2 <31/01/2011> Eugeny Leonov
	[f] Временно убрана проверка записи пользователя.
 
#rev.1 <28/01/2011> Eugeny Leonov
	[-] Удалена попытка обработки методом класса user
*/
function user_create($data,$self=false){
//	if(!$self){$usr=mdl_load('user');if(is_object($usr)&&method_exists($usr,'create')){return($usr->create($data));}}
	if(!isset($data['logName'])||(trim($data['logName'])=='')){return(setResult(false,'Не указан логин пользователя.'));}
	$usr=user_load($data['logName']);
	if(is_array($usr)){//Пользователь существует, обновляем
		return(setResult(false,'Пользователь уже существует.'));
	}
	// Пароль пользователя
	if(!isset($data['logWord'])||!is_string($data['logWord'])){$data['logWord']=='';}
	$data['logWord']=md5($data['logWord']);
	//= Проверяем (обновляем) группы в которые входит пользователь >
	if(isset($data['groups'])&&is_array($data['groups'])){foreach($data['groups']as$k=>$v){
		$g=user_group_load($k);
		if(!$g){unset($data['groups'][$k]);}
		elseif(!isset($g['users'][$data['logName']])){
			$g['users'][$data['logName']]=$data['logName'];
			user_group_save($g,false);
		}
	}}
	//< Проверяем (обновляем) группы в которые входит пользователь =
	if(!cfg_save('@U/'.$data['logName'],$data)){return(setResult(false,'Ошибка сохранения файла пользователя ('.$data['logName'].'): '.getResult('reason')));}
	return(setResult(true));
}

/**
* Обновить параметры пользователя
* @version 1.0.rc <22/11/2010>
* @param array
* @param [bool]=true
* @note После обновления параметров рекомендуется вызывать функцию user_check()
*
* @todo: Проверка параметров по описанию параметров.
*
* #rev.4 <01/02/2011> Eugeny Leonov
* 	[+] Проверка наличия группы и вхождения пользователя в указанные группы
* 	[+] Параметр check указывающий необходимость проверки параметров пользователя
*
* #rev.3 <28/01/2011> Eugeny Leonov
* 	[-] Удалена попытка обработки методом класса user
* 
* #rev.2 <24/01/2011> Eugeny Leonov
*	[f] При изменении пользователя исчезает пароль
*	[*] Перенесено удаление некорректно переданного поля [logWord]. Логическая ошибка исправлена.
*
* #rev.1 <11/01/2011> Eugeny Leonov
*	[+] Проверка наличия полей в новом конфиге определенных в текущем конфиге. При отсутствии - копируем.
*/
function user_update($data,$check=true){
//	if(!$self){$usr=mdl_load('user');if(is_object($usr)&&method_exists($usr,'update')){return($usr->update($data));}}
	if(!isset($data['logName'])||(trim($data['logName'])=='')){return(setResult(false,'Не указан логин пользователя.'));}
	$usr=user_load($data['logName']);
	if(!is_array($usr)){//Пользователя нет, создаем
		return(user_create($data));
	}
	//Пароль не меняем. Он должен быть скопирован из текущего файла.
	if(!isset($usr['logWord'])){if(isset($data['logWord'])){unset($data['logWord']);}}else{$data['logWord']=$usr['logWord'];}
	//Сбрасываем признак sys
	if(isset($data['sys'])){unset($data['sys']);}
	if($check){ //Проверка параметров пользователя.
		//= Проверяем наличие групп и вхождение в них пользователя >
		if(isset($data['groups'])&&is_array($data['groups'])){foreach($data['groups']as$k=>$v){
			$g=user_group_load($k);
			if(!$g){
				//Группы нет, забираем у зверька.
				unset($data['groups'][$k]);
			}elseif(!isset($g['users'][$data['logName']])){
				//Зверька в группе нет, добавляем.
				$g['users'][$data['logName']]=$data['logName'];
				user_group_save($g,false);
			}
		}}
		//< Проверяем наличие групп и вхождение в них пользователя =
	}
	if(!cfg_save('@U/'.$data['logName'],$data)){return(setResult(false,'Ошибка обновления файла пользователя ('.$data['logName'].'): '.getResult('reason')));}
	return(setResult(true));
}

/**
* Сохранить параметры пользователя
* @version 1.0.a <01/02/2011>
* @param array
* @param [bool]=true
*/
function user_save($user,$check=true){
	if(!is_array($user)){return(setResult(false,'Неверный формат параметра.'));}
	if(!isset($user['logName'])||!is_string($user['logName'])||(trim($user['logName'])=='')){return(setResult(false,'Не указан логин пользователя.'));}
	$usr=user_load($user['logName']);
	if(!$usr){$usr=array();if(isset($user['logWord'])){$user['logWord']=md5($user['logWord']);}}else{if(isset($usr['logWord'])){$user['logWord']=$usr['logWord'];}}
	
}

/**
* Удалить пользователя
* @version 1.0.rc <06/01/2011>
* @param string
* @param [bool]=false
* @return bool
*
* #rev.1 <28/01/2011> Eugeny Leonov
* 	[-] Убрана попытка обработки методом класса user
*/
function user_remove($logName,$self=false){
//	if(!$self){$usr=mdl_load('user');if(is_object($usr)&&method_exists($usr,'remove')){return($usr->remove($logName));}}
	if(!cfg_exists('@U/'.$logName)){return(setResult(false,'Файл пользователя не найден ('.$logName.'): '.getResult('reason')));}
	$usr=cfg_load('@U/'.$logName);
	if(is_array($usr)&&(isset($usr['groups'])&&is_array($usr['groups']))){
	//= Удаление пользователя из групп >
		foreach($usr['groups']as$k=>$v){
			$grp=user_group_load($k);
			if(is_array($grp)&&isset($grp['users'][$logName])){unset($grp['users'][$logName]);user_group_save($grp,false);}
		}
	//< Удаление пользователя из групп =
	}
	if(!cfg_remove('@U/'.$logName)){return(setResult(false,'Ошибка удаление файла пользователя ('.$logName.'): '.getResult('reason')));}
	return(setResult(true));
}

/**
* Установить новый пароль пользователя
* @version 1.0.rc <07/01/2011>
* @param string
* @param string
* @param [bool]=false
* @return bool
* 
* #rev.1 <28/01/2011> Eugeny Leonov
* 	[-] Убрана попытка обработки методом класса user
*/
function user_setPassword($logName,$logWord,$self=false){
//	if(!$self){$usr=mdl_load('user');if(is_object($usr)&&method_exists($usr,'setPassword')){return($usr->setPassword($logName,$logWord));}}
	$usr=user_load($logName);
	if(!$usr){return(setResult(false,'Указанного пользователя не существует ('.$logName.'): '.getResult('reason')));}
	$usr['logWord']=md5($logWord);
	if(!cfg_save('@U/'.$logName,$usr)){return(setResult(false,'Ошибка обновления файла пользователя ('.$logName.'): '.getResult('reason')));}
	return(setResult(true));
}

/**
* Обновить (проверить) параметры пользователя
* @version 1.0.a <06/01/2011>
* @param string
* @param [bool]=false
* @return bool
* 
* @todo: Оптимизировать функцию.
* 	[+] Функция user_group_list() должна возвращать только список имен групп, не загружая параметры групп.
* 	Проверять группы пользователя следует по указанным для пользователя группам.
* 
* #rev.2 <17/08/2015> Eugeny Leonov
*		[+] Параметр может быть массивом.
* 
* #rev.1 <28/01/2011> Eugeny Leonov
* 	[-] Убрана попытка обработки методом класса user
*/
function user_check($usr,$self=false){
//	if(!$self){$usr=mdl_load('user');if(is_object($usr)&&method_exists($usr,'grpCheck')){return($usr->grpCheck($logName,$groups));}}
	if(!is_array($usr)){$usr=user_load($usr);}
	if(!is_array($usr)){return(setResult(false,'Пользователь не найден.'));}
	if(!isset($usr['logName'])||!is_string($usr['logName'])||(trim($usr['logName'])=='')){return(setResult(false,'Не указан логин пользователя (ошибка файла конфигурации пользователя).'));}
	if(!isset($usr['groups'])||!is_array($usr['groups'])){$usr['groups']=array();}
	foreach($usr['groups']as$k=>$v){
		$grp=user_group_load($k);
		if(!is_array($grp)){//Группы не существует
			unset($usr['groups'][$k]);
		}elseif(!isset($grp['users'][$usr['logName']])){
			$grp['users'][$usr['logName']]=$usr['logName'];
			user_group_save($grp);
		}
	}
	if(!isset($usr['sites'])||!is_array($usr['sites'])){$usr['sites']=array();}
	foreach($usr['sites']as$k=>$v){
		$site=site_load($k);
		if(!is_array($site)){//Сайта не существует
			unset($usr['sites'][$k]);
		}
	}
	return(user_update($usr));
}

/**
* Добавить пользователя в группу
* @version 1.0.rc.1 <06/01/2011>
* @param string
* @param string
* @param bool=false
* @return bool
* 
* #rev.2 <01/02/2011> Eugeny Leonov
* 	[*] Пользователя нельзя добавить в несуществующую группу.
*
* #rev.1 <28/01/2011> Eugeny Leonov
* 	[-] Убрана попытка обработки методом класса user
* 	[*] Обновление группы производится функцией user_group_save()
* 	[*] Обновление пользователя производится функцией user_update()
*/
function user_grpAdd($logName,$grpName,$self=false){
//	if(!$self){$usr=mdl_load('user');if(is_object($usr)&&method_exists($usr,'grpAdd')){return($usr->grpAdd($logName,$grpName));}}
	$usr=user_load($logName);
	if(!$usr){return(setResult(false,'Пользователь не найден.'));}
	$usr['groups'][$grpName]=$grpName;
	if(!user_update($usr,false)){return(setResult(false,'Ошибка обновления пользователя.'));}
	$grp=user_group_load($grpName);
	if(!is_array($grp)){return(setResult(false,'Группа не найдена.'));}
	$grp['users'][$logName]=$logName;
	if(!user_group_save($grp,false)){return(setResult(false,'Ошибка обновления группы ('.$grpName.'): '.getResult('reason')));}
	return(setResult(true));
}

/**
* Удалить пользователя из группы
* @version 1.0.rc.1 <06/01/2011>
* @param string
* @param string
* @param [bool]=false
* @return bool
* 
* #rev.2 <01/02/2011> Eugeny Leonov
* 	[*] Обновление группы производится функцией user_group_save() с отключенной проверкой данных.
* 	[*] Обновление пользователя производится функцией user_update() с отключенной проверкой данных.
*
* #rev.1 <28/01/2011> Eugeny Leonov
* 	[-] Убрана попытка обработки методом класса user
* 	[*] Обновление группы производится функцией user_group_update()
*/
function user_grpDel($logName,$grpName,$self=false){
//	if(!$self){$usr=mdl_load('user');if(is_object($usr)&&method_exists($usr,'grpDel')){return($usr->grpDel($logName,$grpName));}}
	$usr=user_load($logName);
	if(isset($usr['groups'][$grpName])){
		unset($usr['groups'][$grpName]);
		user_update($usr,false);
	}
	$grp=user_group_load($grpName);
	if(is_array($grp)&&isset($grp['users'][$logName])){
		unset($grp['users'][$logName]);
		if(!user_group_save($grp,false)){return(setResult(false,'Ошибка обновления группы ('.$grpName.'): '.getResult('reason')));}
	}
	return(setResult(true));
}
//<< Функции управления пользователями =========================================
//== Функции управления группами >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
/**
* Загрузить параметры группы
* @version 1.0.rc <06/01/2011>
* @param string
* @param [bool]=false
* @return array|false
* 
* #rev.1 <28/01/2011> Eugeny Leonov
* 	[-] Убрана попытка обработки методом класса user
* 	[+] Если параметры группы загружены из ядра системы к ним добавляется параметр sys=1
*/
function user_group_load($grpName,$self=false){
//	if(!$self){$usr=mdl_load('user');if(is_object($usr)&&method_exists($usr,'group_load')){return($usr->group_load($grpName));}}
	$ret=cfg_load('@U/_groups/'.$grpName);
	if(!$ret){$ret=cfg_load(swc_base.'/_user/_groups/'.$grpName);if(is_array($ret)){$ret['sys']=1;}}
	if(!is_array($ret)){return(setResult(false,'Запрошенная группа не найдена ('.$grpName.'): '.getResult('reason')));}
	setResult(true);
	return($ret);
}

/**
* Сохранить параметры группы
* @version 1.0.a <28/01/2011>
* @param array
* @param [bool]=true - произвести проверку пользователей при сохранении файла группы.
* @return bool
*/
function user_group_save($group,$check=true){
	if(!is_array($group)){return(setResult(false,'Неверный формат параметра.'));}
	if(!isset($group['name'])||!is_string($group['name'])||(trim($group['name'])=='')){return(setResult(false,'Отсутствует имя группы.'));}
	if($check){//Проверка параметров
		//= Проверка пользователей входящих в группу >
		if(isset($group['users'])&&is_array($group['users'])){foreach($group['users']as$k=>$v){
			if(!user_exists($k)){ //Указан несуществующий пользователь.
				unset($group['users'][$k]);
			}
		}}
		//< Проверка пользователей входящих в группу =
	}
	//Сбрасываем параметр sys (группа больше не системная)
	if(isset($group['sys'])){unset($group['sys']);}
	if(!cfg_save('@U/_groups/'.$group['name'],$group)){return(setResult(false,'Ошибка сохранения данных группы ('.$group['name'].'): '.getResult('reason')));}
	return(setResult(true));
}

/**
* Получить список определенных групп
* @version 1.0.rc.1 <06/01/2011>
* @param [bool]=false $dat - загружать данные группы
* @param [bool]=false
* @return array|false
*
* #rev.4 <29/01/2011> Eugeny Leonov
*	[+] Добавлен параметр включающий загрузку данных групп.
*
* #rev.3 <28/01/2011> Eugeny Leonov
*	[-] Убрана попытка обработки методом класса user
*	[!] Сейчас функция возвращает только список имен групп, не производя загрузку их параметров.
*
* #rev.2 <12/01/2011> Eugeny Leonov
*	[*] Для групп определенных в ядре системы устанавливается флаг [sys]=1
*/
function user_group_list($dat=false,$self=false){
//	if(!$self){$usr=mdl_load('user');if(is_object($usr)&&method_exists($usr,'group_list')){return($usr->group_list($f));}}
	$ret=cfg_list(swc_base.'/_user/_groups/');
	if(!is_array($ret)){$ret=array();}
	if($dat){foreach($ret as $k=>$v){$ret[$k]=cfg_load(swc_base.'/_user/_groups/'.$k);}}
//	if(is_array($gl)){foreach($gl as $k=>$v){$ret[$k]=cfg_load(swc_base.'/_user/_groups/'.$k);$ret[$k]['sys']=1;}}
	$gl=cfg_list('@U/_groups/');
	if(is_array($gl)){foreach($gl as $k=>$v){
		if($dat){$ret[$k]=cfg_load('@U/_groups/'.$k);}else{$ret[$k]=$k;}
	}}
	setResult(true);
	return($ret);
}

/**
* Удалить группу
* @version 1.0.rc <04/01/2011>
* @param string
* @param bool=false
* @param bool=false
* @return bool
* 
* @fixme При удалении группы из нее не происходит удаление пользователей
* 
* #rev.2 <01/02/2011> Eugeny Leonov
* 	[*] Обновление пользователя производится функцией :user_update() с отключенной проверкой данных
*
* #rev.1 <28/01/2011> Eugeny Leonov
* 	[-] Убрана попытка обработки методом класса user
* 	[-] убрана обработка параметра $force
*/
function user_group_remove($grpName,$force=false,$self=false){
//	if(!$self){$usr=mdl_load('user');if(is_object($usr)&&method_exists($usr,'group_remove')){return($usr->group_remove($grpName,$force));}}
	$grp=cfg_load('@U/_groups/'.$grpName);
	if(!is_array($grp)){return(setResult(true));} //Файла группы нет, или группа системная (ядерная)
	if(is_array($grp)){
		if(isset($grp['users'])&&is_array($grp['users'])){
			//= Удаляем пользователей из группы >
			foreach($grp['users']as$k=>$v){
				$u=user_load($k);
				if(is_array($u)&&isset($u['groups'][$grpName])){
					unset($u['groups'][$grpName]);
					user_update($u,false);
				}
			}
			//< Удаляем пользователей из группы =
		}
		if(!cfg_remove('@U/_groups/'.$grpName)){return(setResult(false,'Ошибка удаления файла группы ('.$grpName.'): '.getResult('reason')));}
	}
	//= Проверка наличия системной группы >
	$grp=cfg_load(swc_base.'/_user/_groups/'.$grpName);
	if(is_array($grp)){
		//= Восстанавливаем членство пользователей указанных в системной группе >
		if(isset($grp['users'])&&is_array($grp['users'])){foreach($grp['users']as$k=>$v){
			$u=user_load($k);
			if(is_array($u)){
				$u['groups'][$k]=$v;
				user_update($u,false);
			}
		}}
		//< Восстанавливаем членство пользователей указанных в системной группе =
	}
	//< Проверка наличия системной группы =
	return(setResult(true));
}

/**
* Проверить корректность группы
* @version 1.0.a <01/02/2011>
* @param string
*/
function user_group_check($grpName){
	$grp=user_group_load($grpName);
	if(!$grp){return(false);}
	if(isset($grp['users'])&&is_array($grp['users'])){foreach($grp['users']as$k=>$v){
		$u=user_load($k);
		if(!$u){unset($grp['users'][$k]);}
		elseif(!isset($u['groups'][$grpName])){
			$u['groups'][$grpName]=$grpName;
			user_update($u);
		}
	}}
	return(user_group_save($grp,false));
}
//<< Функции управления группами ===============================================
?>
